//=====================================================
// Projekt: de.egladil.exceptions
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.jsf.exceptions;

/**
 * @author heike
 *
 */
public class HoneypotException extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @param message
	 */
	public HoneypotException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public HoneypotException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public HoneypotException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public HoneypotException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
