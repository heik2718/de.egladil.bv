package de.egladil.bv.jsf.utils;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.ResourceBundle.Control;

/**
 * Diese Klasse stellt Methoden zum Zugriff auf ausgelagerte Strings zur Anzeige in der Webapp zur Verfügung. Die
 * Module, die diese Klasse verwenden, müssen eine resource Namens <b>messages.proprties</b> definieren. Dort werden alle
 * GUI-Texte versammelt, so dass es für jedes Modul eine zentrale Stelle gibt, an der diese gepflegt werden. Sämtliche
 * Klassen und xhtml-Dateien, die GUI-Texte erzeugen, referenzieren die in texte.properties definierten Strings.<br>
 * <br>
 * <b>Beispiele:</b>
 * <ul>
 * <li>Klassen: TexteResource.getInstance().getPlainText("validation_altes_passwd")</li>
 * <li>xhtml: #{messages.mitarbeiterDetails_name}</li>
 * </ul>
 * Parametrisierung erfolgt mittels Platzhaltern: {0}, {1}, ... <br>
 * <br>
 * Die Texte müssen in der <b>faces-config.xml</b> deklariert werden: <br>
 * <br>
 * <code>&lt;resource-bundle&gt;<br>
		&lt;base-name&gt;messages&lt;/base-name&gt;<br>
		&lt;var&gt;msg&lt;/var&gt;<br>
	&lt;/resource-bundle&gt;</code> <br>
 * <br>
 * <b>Encoding:</b> Die anzuzeigenden Texte in messages.properties müssen zu UTF-8 konvertiert werden: z.B. ä=\u00e4.
 *
 */
public class MessagesResource {

	private static final MessagesResource INSTANCE = new MessagesResource();

	private final ResourceBundle resourceBundle;

	private MessagesResource() {
		resourceBundle = ResourceBundle.getBundle("messages", ResourceBundle.Control.getControl(Control.FORMAT_PROPERTIES));
	}

	public static MessagesResource getInstance() {
		return INSTANCE;
	}

	/**
	 * Gibt den Text aus texte.properties zurück.
	 *
	 * @param key
	 * @param args
	 * @return String.
	 */
	public String getText(String key, Object... args) throws IllegalArgumentException {
		return getMessageText(resourceBundle, key, args);
	}

	/**
	 * Kleine Helfermethode für fehlendes Pflichtfeld.
	 *
	 * @param feldName
	 * @return String
	 */
	public String getMessageInputRequired(String feldName) {
		String message = getText("validation_pflichtfeld", new Object[] { feldName });
		return message;
	}

	/**
	 *
	 * Kleine Helfermethode für falsche Eingabe in einem Feld.<br>
	 * <br>
	 * <b>Achtung: </b> Kann nur verwendet werden, wenn texte.properties eine property mit key
	 * 'validation_invalid_input' enthält!
	 *
	 * @param feldName
	 * @return
	 */
	public String getInvalidInput(String feldName) {
		return getInvalidInput(feldName, "");
	}

	/**
	 *
	 * Kleine Helfermethode für falsche Eingabe in einem Feld.<br>
	 * <br>
	 * <b>Achtung: </b> Kann nur verwendet werden, wenn texte.properties eine property mit key
	 * 'validation_invalid_input' enthält!
	 *
	 * @param feldName
	 * @param customMessage
	 * @return
	 */
	public String getInvalidInput(String feldName, String customMessage) {
		String message = getText("validation_invalid_input", new Object[] { feldName, customMessage });
		return message;
	}

	/**
	 * Gibt den mit args parametrisierten Text aus dem gegebenen ResourceBundle zurück.
	 *
	 * @param bundle das ResourceBundle.
	 * @param key
	 * @param args
	 * @return
	 */
	private String getMessageText(final ResourceBundle bundle, String key, Object... args) throws IllegalArgumentException {
		if (bundle == null) {
			throw new IllegalArgumentException("Parameter bundle darf nicht null sein!");
		}
		String text = "";
		try {
			text = bundle.getString(key);
		} catch (MissingResourceException e) {
			return "???" + key + "???";
		}
		if (args != null) {
			text = MessageFormat.format(text, args);
		}
		return text;
	}
}
