//=====================================================
// Projekt: de.egladil.bv.jsf
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.jsf.rest;

import java.io.Serializable;
import java.net.URI;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

/**
 * RESTClientBean Kommuniziert mit diversen REST-Services.
 */
@ApplicationScoped
public class RESTClientBean implements Serializable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	private static URI getBaseURI() {
		return UriBuilder.fromUri("http://localhost:9520/mkvadmin").build();
	}

	public void loadBenutzer() {
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(getBaseURI());

		// @formatter:off
		String response = target.path("session")
								.path("kontext")
								.request()
								.accept(MediaType.APPLICATION_JSON)
								.get(Response.class)
								.toString();
		// @formatter:on
		System.out.println(response);

	}

}
