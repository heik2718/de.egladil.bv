/**
 *
 */
package de.egladil.bv.jsf.beans;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.StringUtils;
import org.omnifaces.util.Faces;
import org.omnifaces.util.Messages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.bv.jsf.exceptions.BVAuthenticationException;
import de.egladil.bv.jsf.exceptions.HoneypotException;
import de.egladil.bv.jsf.utils.FacesUtils;
import de.egladil.bv.jsf.utils.MessagesResource;
import de.egladil.bv.shiro_aas.domain.EgladilPrincipalCollection;
import de.egladil.bv.shiro_aas.entities.Benutzerkonto;
import de.egladil.bv.shiro_aas.impl.BenutzerServiceImpl;
import de.egladil.common.persistence.EgladilStorageException;

/**
 * @author heike
 *
 */
@Named
@SessionScoped
public class Login implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final Logger LOG = LoggerFactory.getLogger(Login.class);

	public static final String BV_URL = "app/bvIndex.xhtml";

	private String username;

	private String password;

	private String kleber = "";

	@Inject
	private BenutzerServiceImpl benutzerService;

	/**
	 *
	 */

	public void submit() {
		try {
			if (StringUtils.hasText(kleber)) {
				throw new HoneypotException("EgladilHoneypotException: " + toHoneypotDetails());
			}
			// Hier wird jetzt über die shiro.ini der AuthenticationService bemüht
			UsernamePasswordToken token = new UsernamePasswordToken(username, password, false);
			SecurityUtils.getSubject().login(token);
			token.clear();

			Subject subject = SecurityUtils.getSubject();
			EgladilPrincipalCollection principals = (EgladilPrincipalCollection) subject.getPrincipals();

			String uuid = (String) principals.getPrimaryPrincipal();
			Benutzerkonto admin = benutzerService.loadBenutzerkontoByUuid(uuid);

			LOG.info("Admin {} hat sich eingeloggt", admin.getEmail());

			FacesUtils.setBenutzerkonto(admin);
			// Wenn man hier ist, gibt es keinen savedRequest, auf den umgeleitet werden könnte, da man direkt
			// mit der Login-Seite startet. Daher kann man hier direkt weiter in die BV
			Faces.redirect(BV_URL);
		} catch (EgladilStorageException | BVAuthenticationException e) {
			LOG.error(e.getMessage());
			String resourceText = MessagesResource.getInstance().getText("server.error");
			Messages.addGlobalError(resourceText);
		} catch (HoneypotException e) {
			LOG.warn(e.getMessage());
			String resourceText = MessagesResource.getInstance().getText("auth.login.honey");
			Messages.addGlobalError(resourceText);
		} catch (UnknownAccountException e) {
			LOG.debug(e.getMessage(), e);
			String resourceText = MessagesResource.getInstance().getText("auth.login.credentials");
			Messages.addGlobalError(resourceText);
		} catch (ExcessiveAttemptsException e) {
			LOG.warn(e.getMessage());
			String resourceText = MessagesResource.getInstance().getText("auth.login.bot");
			Messages.addGlobalError(resourceText);
		} catch (AuthenticationException e) {
			String resourceText = MessagesResource.getInstance().getText("auth.login.general");
			Messages.addGlobalError(resourceText);
			LOG.debug(e.getMessage(), e);
		} catch (Exception e) {
			LOG.error("Unerwartete Exception: " + e.getMessage(), e);
			String resourceText = MessagesResource.getInstance().getText("server.error");
			Messages.addGlobalError(resourceText);
		}
	}

	private String toHoneypotDetails() {
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}
		return "Login [username=" + username + ", kleber=" + kleber + ", ipAddress=" + ipAddress + "]";
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the kleber
	 */
	public String getKleber() {
		return kleber;
	}

	/**
	 * @param kleber the kleber to set
	 */
	public void setKleber(String kleber) {
		this.kleber = kleber;
	}

	/**
	 * Rückgabe des Benutzernamens zur Anzeige im fisHeader.
	 *
	 * @return the logged in user name
	 */
	public String getLoggedInUserInfo() {
		Benutzerkonto admin = FacesUtils.getBenutzerkonto();
		if (admin != null) {
			return MessagesResource.getInstance().getText("header.userinfo.admin", admin.getEmail());
		}
		return MessagesResource.getInstance().getText("header.userinfo.anon");
	}
}
