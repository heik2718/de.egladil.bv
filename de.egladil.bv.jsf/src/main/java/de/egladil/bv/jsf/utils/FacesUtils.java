package de.egladil.bv.jsf.utils;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIData;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

import de.egladil.bv.shiro_aas.entities.Benutzerkonto;

public class FacesUtils {

	public enum CallBackStatus {
		SUCCESS,
		FAILURE,
		USERACTION;
	}

	public static void setCallbackParam(String key, String value) {
		RequestContext rCtx = RequestContext.getCurrentInstance();
		rCtx.addCallbackParam(key, value);
	}

	public static void setCallbackSuccess(CallBackStatus status) {
		setCallbackParam("processingStatus", status.name());
	}

	public static void keepMessagesOverRequest() {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	}

	public static String addInfoMessageId(String messageId, Object... params) {
		return addMessageId(messageId, FacesMessage.SEVERITY_INFO, params);
	}

	public static String addWarnMessageId(String messageId, Object... params) {
		return addMessageId(messageId, FacesMessage.SEVERITY_WARN, params);
	}

	public static String addErrorMessageId(String messageId, Object... params) {
		return addMessageId(messageId, FacesMessage.SEVERITY_ERROR, params);
	}

	public static String addInfoMessage(String message, Object... params) {
		return addMessage(message, FacesMessage.SEVERITY_INFO, params);
	}

	public static String addWarnMessage(String message, Object... params) {
		return addMessage(message, FacesMessage.SEVERITY_WARN, params);
	}

	public static String addErrorMessage(String message, Object... params) {
		return addMessage(message, FacesMessage.SEVERITY_ERROR, params);
	}

	public static String addMessageId(String messageId, FacesMessage.Severity severity, Object... params) {
		String msg = MessagesResource.getInstance().getText(messageId, params);
		FacesMessage facesMessage = new FacesMessage(severity, msg, msg);
		FacesContext.getCurrentInstance().addMessage(null, facesMessage);
		return msg;
	}

	public static String addMessage(String message, FacesMessage.Severity severity, Object... params) {
		String msg = String.format(message, params);
		FacesMessage facesMessage = new FacesMessage(severity, msg, msg);
		FacesContext.getCurrentInstance().addMessage(null, facesMessage);
		return msg;
	}

	/**
	 * Resets UIInput/UIData components value to un-initialized state. The given component and all childs are affected
	 * recursively.
	 *
	 * @param componentId is an id of root component to reset value of and all childs recursively, can be null
	 */
	public static void resetUIComponentValueRecursively(String componentId) {
		if (componentId != null) {
			UIComponent root = FacesContext.getCurrentInstance().getViewRoot().findComponent(componentId);
			resetUIComponentValueRecursively(root);
		}
	}

	/**
	 * Resets UIInput/UIData components value to un-initialized state. The given component and all childs are affected
	 * recursively.
	 *
	 * @param component is a root component to reset value of and all childs recursively, can be null
	 */
	public static void resetUIComponentValueRecursively(UIComponent component) {
		if (component != null) {
			resetUIComponentValue(component);
			for (UIComponent child : component.getChildren())
				resetUIComponentValueRecursively(child);
		}
	}

	/**
	 * Resets UIInput/UIData component value to un-initialized state.
	 *
	 * @param component to reset value of, can be null
	 */
	private static void resetUIComponentValue(UIComponent component) {
		if (component instanceof UIInput) {
			((UIInput) component).resetValue();
		} else if (component instanceof UIData) {
			((UIData) component).setValue(null);
		}
	}

	/**
	 * Holt den angemeldeten Autor aus dem SessionContext.
	 *
	 * @return Autor oder null
	 */
	public static Benutzerkonto getBenutzerkonto() {
		Object obj = FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("benutzerkonto");
		if (obj != null) {
			return (Benutzerkonto) obj;
		}
		return null;
	}

	/**
	 * Setzt den Autor in den SessionContext
	 *
	 * @param benutzerkonto
	 */
	public static void setBenutzerkonto(Benutzerkonto benutzerkonto) {
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("benutzerkonto", benutzerkonto);
	}
}
