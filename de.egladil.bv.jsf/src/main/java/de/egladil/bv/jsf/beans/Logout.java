/**
 *
 */
package de.egladil.bv.jsf.beans;

import java.io.IOException;
import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import org.apache.shiro.SecurityUtils;
import org.omnifaces.util.Faces;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.bv.jsf.utils.FacesUtils;
import de.egladil.bv.shiro_aas.entities.Benutzerkonto;

/**
 * @author heike
 *
 */
@Named
@RequestScoped
public class Logout implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -7526371246088242806L;

	private static final Logger LOG = LoggerFactory.getLogger(Logout.class);

	public static final String HOME_URL = "index.xhtml";

	public void submit() throws IOException {
		Benutzerkonto admin = FacesUtils.getBenutzerkonto();
		String email = admin == null ? "null" : admin.getEmail();
		SecurityUtils.getSubject().logout();
		Faces.invalidateSession();
		LOG.info("Admin {} hat sich ausgeloggt", email);
		Faces.redirect(HOME_URL);
	}
}
