//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.shiro_aas.storage.impl;

import java.io.Serializable;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.common.config.IEgladilConfiguration;
import de.egladil.common.persistence.IEntityManagerProducer;

/**
 * @author heike
 *
 */
public class BVEntityManagerProducer implements Serializable, IEntityManagerProducer {

	private static final String PU_NAME = "de.egladil.bv.aas";

	private static final long serialVersionUID = 1L;

	private static final Logger LOG = LoggerFactory.getLogger(BVEntityManagerProducer.class);

	private EntityManagerFactory emf;

	private final IEgladilConfiguration persistenceConfiguration;

	/**
	 * Erzeugt eine Instanz von EntityManagerProducer für den Fall, dass es Probleme mit CDI gibt.
	 */
	public BVEntityManagerProducer() {
		this.persistenceConfiguration = new EgladilBVConfiguration();
	}

	/**
	 *
	 * @see de.egladil.common.persistence.IEntityManagerProducer#getEntityManager()
	 */
	@Override
	public EntityManager getEntityManager() {
		if (emf == null) {
			this.emf = Persistence.createEntityManagerFactory(PU_NAME, persistenceConfiguration.getConfigurationMap());
			LOG.debug("emf created");
		}
		return emf.createEntityManager();
	}

	/**
	 *
	 * @see de.egladil.common.persistence.IEntityManagerProducer#dispose(javax.persistence.EntityManager)
	 */
	@Override
	public void dispose(EntityManager em) {
		if (em != null && em.isOpen()) {
			em.close();
			LOG.debug("em closed");
		}
		LOG.debug("disposed");
	}

	/**
	 *
	 * @see de.egladil.common.persistence.IEntityManagerProducer#shutDown()
	 */
	@Override
	public void shutDown() {
		if (emf != null && emf.isOpen()) {
			emf.close();
			LOG.debug("emf closed");
		}
		LOG.debug("destroyed");
	}
}
