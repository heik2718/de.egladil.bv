//=====================================================
// Projekt: de.egladil.bv.api
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.shiro_aas.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.shiro.subject.MutablePrincipalCollection;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.CollectionUtils;
import org.apache.shiro.util.StringUtils;

/**
 * @author heike
 *
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public class EgladilPrincipalCollection implements MutablePrincipalCollection {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public static final String REALM_NAME = "egladilBVRealm";

	public static final String KEY_UUID = "UUID";

	public static final String KEY_EMAIL = "Email";

	public static final String KEY_LOGINNAME = "login";

	public static final String KEY_ROLLEN = "rollen";

	private Object primaryPrincipal;

	private Map<String, Object> principalMap = new HashMap<>();

	private transient String cachedToString; // cached toString() result, as this can be printed many times in logging

	/*
	 * (non-Javadoc)
	 *
	 * @see org.apache.shiro.subject.PrincipalCollection#getPrimaryPrincipal()
	 */
	@Override
	public Object getPrimaryPrincipal() {
		return this.primaryPrincipal;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.apache.shiro.subject.PrincipalCollection#oneByType(java.lang.Class)
	 */
	@Override
	public <T> T oneByType(Class<T> type) {
		if (isEmpty()) {
			return null;
		}
		for (Object o : this.principalMap.values()) {
			if (type.isAssignableFrom(o.getClass())) {
				return (T) o;
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.apache.shiro.subject.PrincipalCollection#byType(java.lang.Class)
	 */
	@Override
	public <T> Collection<T> byType(Class<T> type) {
		if (isEmpty()) {
			return Collections.EMPTY_SET;
		}
		Set<T> typed = new LinkedHashSet<>();
		for (Object o : this.principalMap.values()) {
			if (type.isAssignableFrom(o.getClass())) {
				typed.add((T) o);
			}
		}
		if (typed.isEmpty()) {
			return Collections.EMPTY_SET;
		}
		return Collections.unmodifiableSet(typed);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.apache.shiro.subject.PrincipalCollection#asList()
	 */
	@Override
	public List asList() {
		if (isEmpty()) {
			return Collections.EMPTY_LIST;
		}
		return Collections.unmodifiableList(new ArrayList(principalMap.values()));
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.apache.shiro.subject.PrincipalCollection#asSet()
	 */
	@Override
	public Set asSet() {
		return Collections.emptySet();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.apache.shiro.subject.PrincipalCollection#fromRealm(java.lang.String)
	 */
	@Override
	public Collection fromRealm(String realmName) {
		if (isEmpty()) {
			return Collections.EMPTY_SET;
		}
		return Collections.unmodifiableCollection(principalMap.values());
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.apache.shiro.subject.PrincipalCollection#getRealmNames()
	 */
	@Override
	public Set<String> getRealmNames() {
		Set<String> result = new HashSet<>();
		result.add(REALM_NAME);
		return Collections.unmodifiableSet(result);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.apache.shiro.subject.PrincipalCollection#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return principalMap == null || principalMap.isEmpty();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator iterator() {
		return asSet().iterator();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.apache.shiro.subject.MutablePrincipalCollection#add(java.lang.Object, java.lang.String)
	 */
	@Override
	public void add(Object principal, String key) {
		// if (!REALM_NAME.equals(key)) {
		// throw new IllegalArgumentException("only [" + REALM_NAME + "] allowed");
		// }
		if (principal == null) {
			throw new IllegalArgumentException("principal argument cannot be null.");
		}
		this.cachedToString = null;
		if (principalMap == null) {
			principalMap = new HashMap<>();
		}
		if (!principalMap.containsKey(key)) {
			principalMap.put(key, principal);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.apache.shiro.subject.MutablePrincipalCollection#addAll(java.util.Collection, java.lang.String)
	 */
	@Override
	public void addAll(Collection principals, String key) {
		// if (!REALM_NAME.equals(key)) {
		// throw new IllegalArgumentException("only [" + REALM_NAME + "] allowed");
		// }
		// if (principals == null) {
		// throw new IllegalArgumentException("principals argument cannot be null.");
		// }
		// if (principals.isEmpty()) {
		// throw new IllegalArgumentException("principals argument cannot be an empty collection.");
		// }
		// this.cachedToString = null;
		// if (principalMap == null) {
		// principalMap = new HashMap<>();
		// }
		// for (Principal p : principals)
		// if (!principalMap.containsKey(key)){
		// principalMap.put(key, principal);
		// }
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.apache.shiro.subject.MutablePrincipalCollection#addAll(org.apache.shiro.subject.PrincipalCollection)
	 */
	@Override
	public void addAll(PrincipalCollection principals) {
		// if (principals.getRealmNames() != null) {
		// this.cachedToString = null;
		// for (String realmName : principals.getRealmNames()) {
		// for (Object principal : principals.fromRealm(realmName)) {
		// if (REALM_NAME.equals(realmName)) {
		// add(principal, realmName);
		// }
		// }
		// }
		// }
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.apache.shiro.subject.MutablePrincipalCollection#clear()
	 */
	@Override
	public void clear() {
		this.cachedToString = null;
		if (this.principalMap != null) {
			this.principalMap.clear();
			this.principalMap = null;
		}

	}

	/**
	 * @param primaryPrincipal the primaryPrincipal to set
	 */
	public void setPrimaryPrincipal(Object primaryPrincipal) {
		this.primaryPrincipal = primaryPrincipal;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (o instanceof EgladilPrincipalCollection) {
			EgladilPrincipalCollection other = (EgladilPrincipalCollection) o;
			return this.principalMap != null ? this.principalMap.equals(other.principalMap) : other.principalMap == null;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		if (this.principalMap != null && !principalMap.isEmpty()) {
			return principalMap.hashCode();
		}
		return super.hashCode();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		if (this.cachedToString == null) {
			Set<Object> principals = asSet();
			if (!CollectionUtils.isEmpty(principals)) {
				this.cachedToString = StringUtils.toString(principals.toArray());
			} else {
				this.cachedToString = "empty";
			}
		}
		return this.cachedToString;
	}

	/**
	 *
	 * TODO
	 *
	 * @param key
	 * @return
	 */
	public Object getPrincipalByKey(String key) {
		if (isEmpty()) {
			return null;
		}
		return principalMap.get(key);
	}
}
