/**
 *
 */
package de.egladil.bv.shiro_aas.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.shiro.authc.AuthenticationInfo;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import de.egladil.bv.shiro_aas.domain.EgladilAuthenticationInfo;
import de.egladil.bv.shiro_aas.domain.TransientLoginSecrets;
import de.egladil.common.validation.annotations.UuidString;

/**
 * Ein Objekt dieses Typs stellt lediglich die Attribute zur Verfügung, die zur Authentisierung und Autorisierung
 * erforderlich sind.
 *
 * @author heike
 *
 */
@Entity
@Table(name = "benutzer")
public class Benutzerkonto implements Serializable, IDomainObject {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	@NotBlank
	@Column(name = "LOGINNAME", length = 255)
	private String loginName;

	@NotBlank
	@Email
	@Column(name = "EMAIL", length = 255)
	private String email;

	@Column(name = "AKTIVIERT")
	@Convert(converter = BooleanToIntegerConverter.class)
	private boolean aktiviert;

	@Column(name = "GESPERRT")
	@Convert(converter = BooleanToIntegerConverter.class)
	private boolean gesperrt;

	@ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.MERGE, CascadeType.REMOVE })
	@JoinTable(name = "benutzerrollen", joinColumns = {
		@JoinColumn(name = "BENUTZER_ID", referencedColumnName = "ID") }, inverseJoinColumns = {
			@JoinColumn(name = "ROLLE_ID", referencedColumnName = "ID") })
	private List<Rolle> rollen = new ArrayList<>();

	@NotNull
	@Column(name = "ANWENDUNG")
	@Enumerated(EnumType.STRING)
	private Anwendung anwendung;

	@Version
	@Column(name = "VERSION")
	private int version;

	@OneToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.EAGER)
	@JoinColumn(name = "PID", referencedColumnName = "ID")
	private LoginSecrets loginSecrets;

	@UuidString
	@NotNull
	@Size(min = 1, max = 40)
	@Column(name = "UUID")
	private String uuid;

	// @Transient
	// private PrincipalMap principalMap = new PrincipalMap();

	/**
	 * Collection of all string-based permissions associated with the account.
	 */
	@Transient
	private Set<String> stringPermissions = new HashSet<>();

	/**
	 *
	 */
	public Benutzerkonto() {
	}

	public void addRolle(final Rolle rolle) {
		if (!this.rollen.contains(rolle)) {
			this.rollen.add(rolle);
		}
	}

	/**
	 *
	 * @see de.egladil.bv.shiro_aas.entities.aas.domain.IDomainObject#toLog()
	 */
	@Override
	public String toLog() {
		return "Benutzerkonto [loginName=" + loginName + ", email=" + email + ", anwendung=" + anwendung + "]";
	}

	/**
	 * @return
	 */
	public Collection<String> getRoles() {
		final List<String> result = new ArrayList<>();
		for (final Rolle r : rollen) {
			result.add(r.getRole().toString());
		}
		return result;
	}

	public Collection<String> getStringPermissions() {
		return this.stringPermissions;
	}

	public void addStringPermission(final String permission) {
		if (this.stringPermissions == null) {
			this.stringPermissions = new HashSet<>();
		}
		this.stringPermissions.add(permission);
	}

	/**
	 * Gibt die AuthenticationInfo als EgladilAuthenticationInfo zurück.
	 *
	 * @return
	 */
	public AuthenticationInfo getAuthenticationInfo() {
		final TransientLoginSecrets ls = new TransientLoginSecrets(loginSecrets.getPasswordhash(), loginSecrets.getSalz().getWert(),
			loginSecrets.getSalz().getAlgorithmName(), loginSecrets.getSalz().getIterations());
		final EgladilAuthenticationInfo authenticationInfo = new EgladilAuthenticationInfo(uuid, ls);
		return authenticationInfo;
	}

	/**
	 *
	 * @see de.egladil.bv.shiro_aas.entities.aas.domain.IDomainObject#getId()
	 */
	@Override
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(final Long id) {
		this.id = id;
	}

	/**
	 * @return the loginName
	 */
	public String getLoginName() {
		return loginName;
	}

	/**
	 * @param loginName the loginName to set
	 */
	public void setLoginName(final String loginName) {
		this.loginName = loginName;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(final String email) {
		this.email = email;
	}

	/**
	 * @param aktiv the aktiv to set
	 */
	public void setAktiviert(final boolean aktiv) {
		this.aktiviert = aktiv;
	}

	/**
	 * @return the anwendung
	 */
	public Anwendung getAnwendung() {
		return anwendung;
	}

	/**
	 * @param anwendung the anwendung to set
	 */
	public void setAnwendung(final Anwendung anwendung) {
		this.anwendung = anwendung;
	}

	/**
	 * @return the loginSecrets
	 */
	public LoginSecrets getLoginSecrets() {
		return loginSecrets;
	}

	/**
	 * @param loginSecrets the loginSecrets to set
	 */
	public void setLoginSecrets(final LoginSecrets loginSecrets) {
		this.loginSecrets = loginSecrets;
	}

	/**
	 * @param gesperrt the gesperrt to set
	 */
	public void setGesperrt(final boolean gesperrt) {
		this.gesperrt = gesperrt;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(final int version) {
		this.version = version;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(final String uuid) {
		this.uuid = uuid;
	}

	public boolean isAktiviert() {
		return aktiviert;
	}

	public boolean isGesperrt() {
		return gesperrt;
	}

	/**
	 * Liefert die Membervariable rollen
	 *
	 * @return die Membervariable rollen
	 */
	public List<Rolle> getRollen() {
		return rollen;
	}

	public Role[] rollenAsRoleArray() {
		final Role[] result = new Role[this.rollen.size()];
		int index = 0;
		for (final Rolle r : this.rollen) {
			result[index++] = r.getRole();
		}
		return result;
	}
}
