//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.shiro_aas.storage;

import java.util.List;
import java.util.Optional;

import de.egladil.bv.shiro_aas.entities.IDomainObject;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.EgladilStorageException;

/**
 * IBaseDao
 */
public interface IBaseDao<T extends IDomainObject> {

	/**
	 *
	 * @return
	 */
	List<T> findAll(Class<T> clazz);

	/**
	 *
	 * @param id
	 * @return
	 */
	Optional<T> findById(Class<T> clazz, long id);

	/**
	 *
	 * @param entity
	 * @return T
	 */
	T persist(T entity) throws EgladilStorageException, EgladilDuplicateEntryException;

}
