//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.shiro_aas.storage.impl;

import javax.persistence.TypedQuery;

import com.google.inject.Inject;

import de.egladil.bv.shiro_aas.entities.Role;
import de.egladil.bv.shiro_aas.entities.Rolle;
import de.egladil.bv.shiro_aas.exception.EgladilShiroException;
import de.egladil.bv.shiro_aas.storage.IRollenDao;
import de.egladil.common.persistence.EgladilStorageException;

/**
 * RollenDaoImpl
 */
public class RollenDaoImpl extends BaseDaoImpl<Rolle> implements IRollenDao {

	/**
	 * Erzeugt eine Instanz von RollenDaoImpl
	 */
	@Inject
	public RollenDaoImpl() {
		super();
	}

	/**
	 * @see de.egladil.bv.aas.storage.IRollenDao#findByRole(de.egladil.bv.aas.domain.Role)
	 */
	@Override
	public Rolle findByRole(Role role) throws EgladilShiroException, EgladilStorageException {
		TypedQuery<Rolle> query = getEntityManager().createQuery("from EnsureRolle r where r.role = :role", Rolle.class);
		query.setParameter("role", role);
		try {
			Rolle rolle = query.getSingleResult();
			return rolle;
		} catch (Exception e) {
			String msg = "Konnte EnsureRolle mit Role=" + role + " nicht finden. " + e.getMessage();
			throw new EgladilStorageException(msg, e);
		}
	}
}
