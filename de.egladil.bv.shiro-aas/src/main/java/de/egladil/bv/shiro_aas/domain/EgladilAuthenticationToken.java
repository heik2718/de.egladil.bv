//=====================================================
// Projekt: de.egladil.bv.shiro-aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.shiro_aas.domain;

import org.apache.shiro.authc.AuthenticationToken;

/**
 * Verpackt loginName und Passwort zusammen mit einer Anwendung.
 * 
 * @author heikew
 *
 */
public class EgladilAuthenticationToken implements AuthenticationToken {

	private final ProvidedCredentials credentials;

	/**
	 * @param credentials
	 */
	public EgladilAuthenticationToken(ProvidedCredentials credentials) {
		super();
		this.credentials = credentials;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.shiro.authc.AuthenticationToken#getPrincipal()
	 */
	@Override
	public Object getPrincipal() {
		return credentials.getLoginName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.shiro.authc.AuthenticationToken#getCredentials()
	 */
	@Override
	public Object getCredentials() {
		return credentials;
	}
}
