//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.shiro_aas.storage.impl;

import de.egladil.common.config.AbstractEgladilConfiguration;
import de.egladil.common.config.IEgladilConfiguration;

/**
* EgladilBVConfiguration
*/
public class EgladilBVConfiguration extends AbstractEgladilConfiguration implements IEgladilConfiguration {

	/* serialVersionUID		*/
	private static final long serialVersionUID = 1L;

	/**
	* @see de.egladil.config.AbstractEgladilConfiguration#getConfigFileName()
	*/
	@Override
	protected String getConfigFileName() {
		return "bv_persistence.properties";
	}

}
