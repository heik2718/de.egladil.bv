//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.shiro_aas.storage;

import de.egladil.bv.shiro_aas.entities.Anwendung;
import de.egladil.bv.shiro_aas.entities.Benutzerkonto;
import de.egladil.common.persistence.EgladilStorageException;

public interface IBenutzerDao extends IBaseDao<Benutzerkonto> {

	/**
	 * Sucht den Benutzer mit diesem Loginnamen.
	 *
	 * @param loginName
	 * @param anwendung
	 * @return Benutzerkonto oder null, wenn es nicht gefunden wurde.
	 */
	Benutzerkonto findBenutzerByLoginName(String loginName, Anwendung anwendung) throws EgladilStorageException;

	/**
	 * Sucht den Benutzer mit dieser Mailadresse.
	 *
	 * @param email
	 * @param anwendung
	 * @return Benutzerkonto oder null, wenn es nicht gefunden wurde.
	 */
	Benutzerkonto findBenutzerByEmail(String email, Anwendung anwendung) throws EgladilStorageException;

	/**
	 * Sucht den Benutzer mit diesem Loginnamen.
	 *
	 * @param uuid
	 * @return Benutzerkonto oder null, wenn es nicht gefunden wurde.
	 */
	Benutzerkonto findBenutzerByUUID(String uuid) throws EgladilStorageException;
}