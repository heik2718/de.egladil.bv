/**
 *
 */
package de.egladil.bv.shiro_aas.impl;

import java.io.Serializable;
import java.util.Date;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.Authenticator;
import org.apache.shiro.authc.DisabledAccountException;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.bv.shiro_aas.entities.Anwendung;
import de.egladil.bv.shiro_aas.entities.Benutzerkonto;
import de.egladil.bv.shiro_aas.entities.LoginSecrets;
import de.egladil.bv.shiro_aas.entities.Salz;
import de.egladil.bv.shiro_aas.exception.EgladilShiroException;
import de.egladil.bv.shiro_aas.storage.IBenutzerDao;
import de.egladil.common.config.EgladilConfigurationException;
import de.egladil.common.config.IEgladilConfiguration;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.crypto.provider.CryptoConfigurationKeys;
import de.egladil.crypto.provider.impl.EgladilCryptoConfiguration;
import de.egladil.crypto.provider.impl.EgladilCryptoUtilsImpl;

/**
 * @author root
 *
 */
@Stateless
public class AuthenticationServiceImpl implements Authenticator, Serializable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	private static final Logger LOG = LoggerFactory.getLogger(AuthenticationServiceImpl.class);

	private Anwendung anwendung;

	private int delaySeconds = 2;

	private IBenutzerDao benutzerDao;

	private EgladilCryptoUtilsImpl cryptoUtils;

	private IEgladilConfiguration cryptoConfig;

	/**
	 *
	 * Erzeugt eine Instanz von AuthenticationServiceImpl
	 */
	@Inject
	public AuthenticationServiceImpl(IBenutzerDao benutzerDao) {
		this.benutzerDao = benutzerDao;
		init();
	}

	private void init() throws EgladilShiroException {
		String configRoot = getDevConfigRoot();
		if (cryptoConfig == null) {
			// FIXME: auslagern
			cryptoConfig = new EgladilCryptoConfiguration(configRoot);
		}
		delaySeconds = cryptoConfig.getIntegerProperty(CryptoConfigurationKeys.LOGIN_BOT_DELAY);
		if (cryptoUtils == null) {
			// FIXME: ist hart codiert, wird aber
			cryptoUtils = new EgladilCryptoUtilsImpl(configRoot);
		}
	}

	private String getDevConfigRoot() {
		String osName = System.getProperty("os.name");

		if (osName.contains("Windows")) {
			return "C:/Users/Winkelv/.esapi/mkvapi/config";
		}
		return "/home/heike/.esapi";
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.apache.shiro.authc.Authenticator#authenticate(org.apache.shiro.authc.AuthenticationToken)
	 */
	@Override
	public AuthenticationInfo authenticate(AuthenticationToken authenticationToken)
		throws IllegalArgumentException, UnknownAccountException, DisabledAccountException, ExcessiveAttemptsException,
		AuthenticationException, EgladilStorageException {
		// Das Teil wird vom Shiro-SecurityManager aufgerufen
		// Leider verwendet Shiro in der Standardversion einen anderen CDI container. Da ich keine Lust habe, das
		// reinzufriggeln
		// initialisiere ich erstmal per Hand, falls erforderlich.
		if (!(authenticationToken instanceof UsernamePasswordToken)) {
			throw new IllegalArgumentException("Funktioniert nur fuer UsernamePasswordToken");
		}
		UsernamePasswordToken usernamePasswordToken = (UsernamePasswordToken) authenticationToken;
		Benutzerkonto benutzerkonto = benutzerDao.findBenutzerByLoginName(usernamePasswordToken.getUsername(), anwendung);
		if (benutzerkonto == null) {
			benutzerkonto = benutzerDao.findBenutzerByEmail(usernamePasswordToken.getUsername(), anwendung);

			if (benutzerkonto == null) {
				throw new UnknownAccountException("Kein Benutzerkonto mit [loginname oder email="
					+ usernamePasswordToken.getUsername() + ", Anwendung=" + anwendung + "] bekannt");
			}
		}
		if (!benutzerkonto.isAktiviert() || benutzerkonto.isGesperrt()) {
			throw new DisabledAccountException("Benutzerkonto  mit [loginname oder email=" + usernamePasswordToken.getUsername()
				+ ", Anwendung=" + anwendung + "] noch noch nicht oder nicht mehr aktiviert");
		}
		benutzerkonto = checkFrequency(benutzerkonto);
		this.verifyPasswort(usernamePasswordToken.getPassword(), benutzerkonto);

		usernamePasswordToken.clear();
		return benutzerkonto.getAuthenticationInfo();
	}

	/**
	 * Methode prüft, ob es sich um einen Bot handeln könnte.
	 *
	 * @param benutzerkonto
	 * @return Benutzerkonto das gemerge-te Benutzerkonto, dessen lastLoginAttempt aktualisiert wurde.
	 * @throws ExcessiveAttemptsException
	 * @trows EgladilStorageException
	 */
	private synchronized Benutzerkonto checkFrequency(Benutzerkonto benutzerkonto)
		throws ExcessiveAttemptsException, EgladilStorageException {
		LoginSecrets secrets = benutzerkonto.getLoginSecrets();
		Date lastLoginAttempt = secrets.getLastLoginAttempt();
		Benutzerkonto gespeichert = benutzerDao.persist(benutzerkonto);
		if (lastLoginAttempt != null) {
			DateTime now = new DateTime();
			DateTime lastLoginAttemptDateTime = new DateTime(lastLoginAttempt.getTime());
			Period diff = new Period(now, lastLoginAttemptDateTime);
			secrets.setLastLoginAttempt(now.toDate());
			if (diff.getSeconds() < delaySeconds) {
				throw new ExcessiveAttemptsException("Benutzerkonto  mit [loginname=" + benutzerkonto.getLoginName()
					+ ", Anwendung=" + anwendung + "] scheint attackiert zu werden");
			}
		}
		return gespeichert;
	}

	public void verifyPasswort(char[] password, Benutzerkonto benutzerkonto) throws IncorrectCredentialsException {
		LoginSecrets loginSecrets = benutzerkonto.getLoginSecrets();
		Salz salz = benutzerkonto.getLoginSecrets().getSalz();

		boolean korrekt = cryptoUtils.isPasswordCorrect(password, loginSecrets.getPasswordhash(), salz.getWert(),
			salz.getAlgorithmName(), salz.getIterations());
		if (!korrekt) {
			throw new IncorrectCredentialsException("loginname stimmt, passwort nicht");
		}
	}

	/**
	 * @param anwendung the anwendung to set
	 */
	public void setApplicationName(String applicationName) throws EgladilConfigurationException {
		try {
			this.anwendung = Anwendung.valueOf(applicationName);
			LOG.debug("Authenticator konfiguriert fuer " + this.anwendung.toString());
		} catch (Exception e) {
			throw new EgladilConfigurationException(
				"Fehler in shiro.ini: egladilAuthenticator.applicationName muss gesetzt sein auf einen der Werte "
					+ Anwendung.getAll());
		}
	}

	// public void changePasswort(String uuid, char[] pwdAlt, char[] pwdNeu)
	// throws UnknownAccountException, IncorrectCredentialsException, EgladilServerException {
	// try {
	// Benutzerkonto benutzerkonto = benutzerDao.findBenutzerByUUID(uuid);
	// if (benutzerkonto == null) {
	// throw new UnknownAccountException(
	// "Kein Benutzerkonto mit [uuid=" + uuid + ", Anwendung=" + anwendung + "] bekannt");
	// }
	// this.verifyPasswort(pwdAlt, benutzerkonto);
	//
	// Hash hash = cryptoUtils.hashPassword(pwdNeu);
	//
	// LoginSecrets loginSecrets = new LoginSecrets();
	// loginSecrets.setPasswordhash(Base64.getEncoder().encodeToString(hash.getBytes()));
	//
	// Salz salz = new Salz();
	// salz.setAlgorithmName(hash.getAlgorithmName());
	// salz.setIterations(hash.getIterations());
	// salz.setWert(hash.getSalt().toBase64());
	// loginSecrets.setSalz(salz);
	//
	// benutzerkonto.setLoginSecrets(loginSecrets);
	// Benutzerkonto result = this.benutzerDao.persist(benutzerkonto);
	// LOG.debug("Benutzer {} geaendert", result.toString());
	// } catch (Exception e) {
	// if (e instanceof UnknownAccountException) {
	// throw e;
	// }
	// throw new EgladilServerException("Fehler beim Ändern des Passworts", e);
	// }
	// }
}
