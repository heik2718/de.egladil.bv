//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.shiro_aas.storage.impl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;

import com.google.inject.Inject;

import de.egladil.bv.shiro_aas.entities.IDomainObject;
import de.egladil.bv.shiro_aas.storage.IBaseDao;
import de.egladil.common.persistence.PersistenceExceptionMapper;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.common.persistence.IEntityManagerProducer;

/**
 * BaseDaoImpl
 */
public class BaseDaoImpl<T extends IDomainObject> implements IBaseDao<T> {

	private final IEntityManagerProducer emp;

	/**
	 * Erzeugt eine Instanz von BaseDaoImpl
	 */
	@Inject
	public BaseDaoImpl() {
		emp = new BVEntityManagerProducer();
	}

	/**
	 * @see de.egladil.bv.aas.storage.IBaseDao#findAll(java.lang.Class)
	 */
	@Override
	public List<T> findAll(Class<T> clazz) {
		String statement = "SELECT e FROM " + clazz.getSimpleName() + " e";
		List<T> resultList = emp.getEntityManager().createQuery(statement.toString(), clazz).getResultList();
		return resultList;
	}

	/**
	 * @see de.egladil.bv.aas.storage.IBaseDao#findById(java.lang.Class, long)
	 */
	@Override
	public Optional<T> findById(Class<T> clazz, long id) {
		final T entity = emp.getEntityManager().find(clazz, id);
		return entity == null ? Optional.empty() : Optional.of(entity);
	}

	/**
	 * @see de.egladil.bv.aas.storage.IBaseDao#persist(de.egladil.bv.aas.domain.IDomainObject)
	 */
	@Override
	public T persist(T entity) {
		EntityTransaction trx = null;
		T result = null;
		EntityManager em = emp.getEntityManager();
		try {
			trx = em.getTransaction();
			trx.begin();
			if (entity.getId() != null) {
				result = em.merge(entity);
			} else {
				em.persist(entity);
				result = entity;
			}
			trx.commit();
			return result;
		} catch (PersistenceException e) {
			if (trx != null && trx.isActive()) {
				trx.rollback();
			}
			Optional<EgladilDuplicateEntryException> integrityException = PersistenceExceptionMapper
				.toEgladilDuplicateEntryException(e);
			if (integrityException.isPresent()) {
				throw integrityException.get();
			}
			throw new EgladilStorageException("Fehler in einer Datenbanktransaktion", e);
		}
	}

	/**
	 * Liefert die Membervariable emp
	 *
	 * @return die Membervariable emp
	 */
	protected EntityManager getEntityManager() {
		return emp.getEntityManager();
	}

}
