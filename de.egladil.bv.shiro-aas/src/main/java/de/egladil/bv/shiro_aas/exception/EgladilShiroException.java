//=====================================================
// Projekt: de.egladil.bv.shiro-aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.shiro_aas.exception;

/**
 * @author heikew
 *
 */
public class EgladilShiroException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @param message
	 */
	public EgladilShiroException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public EgladilShiroException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public EgladilShiroException(String message, Throwable cause) {
		super(message, cause);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public EgladilShiroException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
