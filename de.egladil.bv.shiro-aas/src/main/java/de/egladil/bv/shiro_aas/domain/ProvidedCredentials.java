//=====================================================
// Projekt: de.egladil.bv.shiro-aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.shiro_aas.domain;

import de.egladil.bv.shiro_aas.entities.Anwendung;

/**
 * @author heikew
 *
 */
public class ProvidedCredentials {

	private final Anwendung anwendung;

	private final String loginName;

	private final char[] password;

	/**
	 * @param anwendung
	 * @param loginName
	 * @param password
	 */
	public ProvidedCredentials(Anwendung anwendung, String loginName, char[] password) {
		super();
		this.anwendung = anwendung;
		this.loginName = loginName;
		this.password = password;
	}

	/**
	 * @return the anwendung
	 */
	public Anwendung getAnwendung() {
		return anwendung;
	}

	/**
	 * @return the loginName
	 */
	public String getLoginName() {
		return loginName;
	}

	/**
	 * @return the password
	 */
	public char[] getPassword() {
		return password;
	}
}
