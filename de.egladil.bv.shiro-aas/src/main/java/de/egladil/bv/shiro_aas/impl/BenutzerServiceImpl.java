//=====================================================
// Projekt: de.egladil.bv.api
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.shiro_aas.impl;

import java.io.Serializable;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.bv.shiro_aas.entities.Anwendung;
import de.egladil.bv.shiro_aas.entities.Benutzerkonto;
import de.egladil.bv.shiro_aas.entities.Role;
import de.egladil.bv.shiro_aas.entities.Rolle;
import de.egladil.bv.shiro_aas.storage.IBenutzerDao;


/**
 * BenutzerServiceImpl
 */
@Stateless
public class BenutzerServiceImpl implements Serializable {

	/* serialVersionUID */
	private static final long serialVersionUID = 2L;

	private static final Logger LOG = LoggerFactory.getLogger(BenutzerServiceImpl.class);

	@Inject
	private IBenutzerDao benutzerDao;

	/**
	 * Erzeugt eine Instanz von BenutzerServiceImpl
	 */
	public BenutzerServiceImpl() {
	}

	/**
	 * Sperrt oder entsperrt das zum subject gehörende Benutzerkonto.
	 *
	 * @param subject
	 */
	public void toggleSperre(Subject subject) {
		// TODO Generierter Code

	}

	/**
	 * Läd die Daten des Benutzerkontos anhand des primary principals
	 *
	 * @param uuid
	 * @return
	 */
	public Benutzerkonto loadBenutzerkontoByUuid(String uuid) {
		// Das ist nur eine Dummy-Implementierung
		Benutzerkonto benutzerkonto = new Benutzerkonto();
		benutzerkonto.setId(24l);
		benutzerkonto.setEmail("heike@egladil.de");
		benutzerkonto.setAktiviert(true);
		benutzerkonto.setAnwendung(Anwendung.BV);
		benutzerkonto.setUuid(uuid);
		Rolle rolle = new Rolle(Role.BV_ADMIN);
		benutzerkonto.addRolle(rolle);
		return benutzerkonto;
	}


}
