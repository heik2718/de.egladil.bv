/**
 *
 */
package de.egladil.bv.shiro_aas.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * @author heike
 *
 */
@Entity(name = "LoginSecrets")
@Table(name = "pw")
public class LoginSecrets implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 2L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	@Column(name = "PWHASH", length = 2000)
	private String passwordhash;

	@Column(name = "LAST_LOGIN_ATTEMPT")
	private Date lastLoginAttempt;

	@Version
	@Column(name = "VERSION")
	private int version;

	@OneToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.EAGER)
	@JoinColumn(name = "SLZ")
	private Salz salz;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the passwordhash
	 */
	public String getPasswordhash() {
		return passwordhash;
	}

	/**
	 * @param passwordhash the passwordhash to set
	 */
	public void setPasswordhash(String passwordhash) {
		this.passwordhash = passwordhash;
	}

	/**
	 * @return the version
	 */
	public int getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(int version) {
		this.version = version;
	}

	/**
	 * @return the salz
	 */
	public Salz getSalz() {
		return salz;
	}

	/**
	 * @param salz the salz to set
	 */
	public void setSalz(Salz salz) {
		this.salz = salz;
	}

	/**
	 * @return the lastLoginAttempt
	 */
	public Date getLastLoginAttempt() {
		return lastLoginAttempt;
	}

	/**
	 * @param lastLoginAttempt the lastLoginAttempt to set
	 */
	public void setLastLoginAttempt(Date lastLoginAttempt) {
		this.lastLoginAttempt = lastLoginAttempt;
	}
}
