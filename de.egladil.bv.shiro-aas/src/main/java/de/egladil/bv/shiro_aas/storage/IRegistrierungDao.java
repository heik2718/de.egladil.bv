//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.shiro_aas.storage;

import de.egladil.bv.shiro_aas.entities.Registrierungsbestaetigung;
import de.egladil.bv.shiro_aas.exception.EgladilShiroException;
import de.egladil.common.persistence.EgladilStorageException;

/**
 * IRegistrirungDao
 */
public interface IRegistrierungDao extends IBaseDao<Registrierungsbestaetigung> {

	/**
	 * Tja, was wohl.
	 * 
	 * @param confirmationCode
	 * @return
	 * @throws EgladilServerException
	 * @throws EgladilStorageException
	 */
	Registrierungsbestaetigung findByConfirmationCode(String confirmationCode)
		throws EgladilShiroException, EgladilStorageException;

	/**
	 * Löscht die Registrierungsbestätigung.
	 * 
	 * @param entity
	 * @throws EgladilServerException
	 * @throws EgladilStorageException
	 */
	void delete(Registrierungsbestaetigung entity) throws EgladilShiroException, EgladilStorageException;
}
