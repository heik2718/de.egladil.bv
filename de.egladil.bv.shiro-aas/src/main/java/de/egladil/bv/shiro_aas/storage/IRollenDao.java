//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.shiro_aas.storage;

import de.egladil.bv.shiro_aas.entities.Role;
import de.egladil.bv.shiro_aas.entities.Rolle;
import de.egladil.bv.shiro_aas.exception.EgladilShiroException;
import de.egladil.common.persistence.EgladilStorageException;

/**
 * IRollenDao
 */
public interface IRollenDao extends IBaseDao<Rolle> {

	/**
	 * Tja, was wohl.
	 * 
	 * @param role
	 * @return EnsureRolle
	 * @throws EgladilServerException
	 * @throws EgladilStorageException
	 */
	Rolle findByRole(Role role) throws EgladilShiroException, EgladilStorageException;
}
