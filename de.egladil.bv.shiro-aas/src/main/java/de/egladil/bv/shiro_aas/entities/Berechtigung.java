/**
 *
 */
package de.egladil.bv.shiro_aas.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Eine Berechtigung wird später zu einer Permission umgewandelt.
 *
 * @author heike
 *
 */
@Entity
@Table(name = "kat_rechte")
public class Berechtigung implements Serializable, IDomainObject {

	private static final long serialVersionUID = 2L;

	@Id
	@Column(name = "ID")
	private Long id;

	@Column(name = "PERMISSION")
	@NotNull
	private String permission;

	@ManyToOne
	@JoinColumn(name = "ROLLE")
	private Rolle rolle;

	/**
	 *
	 */
	public Berechtigung() {
		// TODO Auto-generated constructor stub
	}

	/**
	 *
	 * @see de.egladil.bv.shiro_aas.entities.aas.domain.IDomainObject#toLog()
	 */
	@Override
	public String toLog() {
		return "Berechtigung [permission=" + permission + ", rolle=" + (rolle == null ? "null" : rolle.toLog()) + "]";
	}

	/**
	 * @return the berechtigung
	 */
	public String getPermission() {
		return permission;
	}

	/**
	 * @param berechtigung the berechtigung to set
	 */
	public void setPermission(String berechtigung) {
		this.permission = berechtigung;
	}

	/**
	 *
	 * @see de.egladil.bv.shiro_aas.entities.aas.domain.IDomainObject#getId()
	 */
	@Override
	public Long getId() {
		return id;
	}

}
