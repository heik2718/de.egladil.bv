//=====================================================
// Projekt: de.egladil.bv.shiro-aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.shiro_aas.domain;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;

import de.egladil.bv.shiro_aas.EgladilBVRealm;

/**
 * Implementierung der AuthenticationInfo.
 * 
 * @author heikew
 *
 */
public class EgladilAuthenticationInfo implements AuthenticationInfo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final String uuid;

	private final TransientLoginSecrets loginSecrets;

	/**
	 * @param uuid
	 * @param loginSecrets
	 */
	public EgladilAuthenticationInfo(String uuid, TransientLoginSecrets loginSecrets) {
		super();
		this.uuid = uuid;
		this.loginSecrets = loginSecrets;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.shiro.authc.AuthenticationInfo#getPrincipals()
	 */
	@Override
	public PrincipalCollection getPrincipals() {
		SimplePrincipalCollection pc = new SimplePrincipalCollection();
		pc.add(uuid, EgladilBVRealm.EGLADIL_BV_REALM);
		return pc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.shiro.authc.AuthenticationInfo#getCredentials()
	 */
	@Override
	public Object getCredentials() {
		return loginSecrets;
	}
}
