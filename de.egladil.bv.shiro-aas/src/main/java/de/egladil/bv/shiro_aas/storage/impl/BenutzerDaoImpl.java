//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.shiro_aas.storage.impl;

import java.io.Serializable;
import java.util.List;

import javax.persistence.TypedQuery;

import com.google.inject.Inject;

import de.egladil.bv.shiro_aas.entities.Anwendung;
import de.egladil.bv.shiro_aas.entities.Benutzerkonto;
import de.egladil.bv.shiro_aas.storage.IBenutzerDao;
import de.egladil.common.persistence.EgladilStorageException;

/**
 * @author heike
 *
 */
public class BenutzerDaoImpl extends BaseDaoImpl<Benutzerkonto> implements IBenutzerDao, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 3L;

	/**
	 * Erzeugt eine Instanz von BenutzerDaoImpl
	 */
	@Inject
	public BenutzerDaoImpl() {
		super();
	}

	/**
	 * @see de.egladil.bv.aas.storage.IBenutzerDao#findBenutzerByLoginName(java.lang.String,
	 * de.egladil.bv.aas.domain.Anwendung)
	 */
	@Override
	public Benutzerkonto findBenutzerByLoginName(String loginName, Anwendung anwendung) throws EgladilStorageException {
		try {
			TypedQuery<Benutzerkonto> query = getEntityManager().createQuery(
				"from Benutzerkonto b where b.loginName = :loginName and b.anwendung = :anwendung",
				Benutzerkonto.class);
			query.setParameter("loginName", loginName);
			query.setParameter("anwendung", anwendung);

			List<Benutzerkonto> result = query.getResultList();
			if (result.isEmpty()) {
				return null;
			}
			if (result.size() > 1) {
				throw new EgladilStorageException("Benutzer mit Loginname [" + loginName + "] und Mandant [" + anwendung
					+ "] existiert mehr als einmal - kann eigentlich nicht sein");
			}
			return result.get(0);
		} catch (Exception e) {
			throw new EgladilStorageException("Unerwartete Exception beim Suchen eines Benutzerkontos", e);
		}
	}

	/**
	 * @see de.egladil.bv.aas.storage.IBenutzerDao#findBenutzerByEmail(java.lang.String,
	 * de.egladil.bv.aas.domain.Anwendung)
	 */
	@Override
	public Benutzerkonto findBenutzerByEmail(String email, Anwendung anwendung) throws EgladilStorageException {
		try {
			TypedQuery<Benutzerkonto> query = getEntityManager().createQuery(
				"from Benutzerkonto b where b.email = :email and b.anwendung = :anwendung", Benutzerkonto.class);
			query.setParameter("email", email);
			query.setParameter("anwendung", anwendung);

			List<Benutzerkonto> result = query.getResultList();
			if (result.isEmpty()) {
				return null;
			}
			if (result.size() > 1) {
				throw new EgladilStorageException("Benutzer mit Email [" + email + "] und Mandant [" + anwendung
					+ "] existiert mehr als einmal - kann eigentlich nicht sein");
			}
			return result.get(0);
		} catch (Exception e) {
			throw new EgladilStorageException("Unerwartete Exception beim Suchen eines Benutzerkontos", e);
		}
	}

	/**
	 * @see de.egladil.bv.aas.storage.IBenutzerDao#findBenutzerByUUID(java.lang.String)
	 */
	@Override
	public Benutzerkonto findBenutzerByUUID(String uuid) throws EgladilStorageException {
		try {
			TypedQuery<Benutzerkonto> query = getEntityManager().createQuery(
				"from Benutzerkonto b where b.uuid = :uuid and b.anwendung = :anwendung", Benutzerkonto.class);
			query.setParameter("uuid", uuid);

			List<Benutzerkonto> result = query.getResultList();
			if (result.isEmpty()) {
				return null;
			}
			if (result.size() > 1) {
				throw new EgladilStorageException(
					"Benutzer mit UUID [" + uuid + "] existiert mehr als einmal - kann eigentlich nicht sein");
			}
			return result.get(0);
		} catch (Exception e) {
			throw new EgladilStorageException("Unerwartete Exception beim Suchen eines Benutzerkontos", e);
		}
	}
}
