//=====================================================
// Projekt: de.egladil.bv.shiro-aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.shiro_aas.domain;

/**
 * Value-Object, das die Daten aus LoginSecrets enthält und nach der Verwendung sauber weggeräumt wreden kann, da es
 * nicht mit irgendeinem EntityManager verküpft ist.
 * 
 * @author heikew
 *
 */
public class TransientLoginSecrets {

	private final String passwordhash;

	private final String saltValue;

	private final String algorithmName;

	private final int numberIterations;

	/**
	 * @param passwordhash
	 * @param saltValue
	 * @param algorithmName
	 * @param numberIterations
	 */
	public TransientLoginSecrets(String passwordhash, String saltValue, String algorithmName, int numberIterations) {
		super();
		this.passwordhash = passwordhash;
		this.saltValue = saltValue;
		this.algorithmName = algorithmName;
		this.numberIterations = numberIterations;
	}

	/**
	 * @return the passwordhash
	 */
	public String getPasswordhash() {
		return passwordhash;
	}

	/**
	 * @return the saltValue
	 */
	public String getSaltValue() {
		return saltValue;
	}

	/**
	 * @return the algorithmName
	 */
	public String getAlgorithmName() {
		return algorithmName;
	}

	/**
	 * @return the numberIterations
	 */
	public int getNumberIterations() {
		return numberIterations;
	}

}
