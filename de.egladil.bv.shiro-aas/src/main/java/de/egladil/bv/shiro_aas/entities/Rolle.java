/**
 *
 */
package de.egladil.bv.shiro_aas.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author heike
 *
 */
@Entity
@Table(name = "kat_rollen")
public class Rolle implements Serializable, IDomainObject {

	/**
	 *
	 */
	private static final long serialVersionUID = 2L;

	@Id
	private Long id;

	@Enumerated(EnumType.STRING)
	@Column(name = "NAME")
	private Role role;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "rolle")
	private List<Berechtigung> berechtigungen;

	/**
	 *
	 */
	public Rolle() {
		super();
	}

	/**
	 * @param role
	 */
	public Rolle(Role role) {
		super();
		this.role = role;
	}

	/**
	 *
	 * @see de.egladil.bv.shiro_aas.entities.aas.domain.IDomainObject#toLog()
	 */
	@Override
	public String toLog() {
		return "EnsureRolle [role=" + role + "]";
	}

	/**
	 * @return the role
	 */
	public Role getRole() {
		return role;
	}

	/**
	 * @return the berechtigungen
	 */
	public List<Berechtigung> getBerechtigungen() {
		return berechtigungen;
	}

	/**
	 *
	 * @see de.egladil.bv.shiro_aas.entities.aas.domain.IDomainObject#getId()
	 */
	@Override
	public Long getId() {
		return id;
	}

	public static Rolle findByRole(List<Rolle> rollen, Role role) {
		for (Rolle r : rollen) {
			if (r.getRole().equals(role)) {
				return r;
			}
		}
		return null;
	}

}
