/**
 *
 */
package de.egladil.bv.shiro_aas.entities;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * Kapselt die Attribute, die für die Aktivierung eines Benutzerkontos erforderlich sind.
 *
 * @author heike
 *
 */
@Entity
@Table(name = "aktivierungsdaten")
public class Registrierungsbestaetigung implements Serializable, IDomainObject {

	/**
	 *
	 */
	private static final long serialVersionUID = 2L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	@Column(name = "CONFIRM_CODE", length = 200)
	private String confirmationCode;

	@Column(name = "CONFIRM_EXPIRETIME")
	private Date expirationTime;

	@OneToOne(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE  }, optional = false)
	@JoinColumn(name = "BENUTZER", referencedColumnName = "ID", nullable = false)
	private Benutzerkonto benutzerkonto;

	@Version
	@Column(name = "VERSION")
	private int version;

	/**
	 *
	 */
	public Registrierungsbestaetigung() {
	}

	/**
	 *
	 * @see de.egladil.bv.shiro_aas.entities.aas.domain.IDomainObject#toLog()
	 */
	@Override
	public String toLog() {
		String expires = expirationTime == null ? "null"
			: new SimpleDateFormat("dd.MM.yyyy hh:mm:ss").format(expirationTime);
		return "Registrierungsbestaetigung [confirmationCode=" + confirmationCode + ", expirationTime=" + expires
			+ ", benutzerkonto=" + (benutzerkonto == null ? "null" : benutzerkonto.toLog()) + "]";
	}

	/**
	 *
	 * @see de.egladil.bv.shiro_aas.entities.aas.domain.IDomainObject#getId()
	 */
	@Override
	public Long getId() {
		return this.id;
	}

	public String getConfirmationCode() {
		return this.confirmationCode;
	}

	public void setConfirmationCode(String code) {
		this.confirmationCode = code;
	}

	public Date getExpirationTime() {
		return this.expirationTime;
	}

	public void setExpirationTime(Date date) {
		this.expirationTime = date;
	}

	/**
	 * @return the benutzerkonto
	 */
	public Benutzerkonto getBenutzerkonto() {
		return benutzerkonto;
	}

	/**
	 * @param benutzerkonto the benutzerkonto to set
	 */
	public void setBenutzerkonto(Benutzerkonto benutzerkonto) {
		this.benutzerkonto = benutzerkonto;
	}

}
