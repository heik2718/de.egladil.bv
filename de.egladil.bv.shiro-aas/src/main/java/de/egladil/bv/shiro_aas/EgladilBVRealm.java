/**
 *
 */
package de.egladil.bv.shiro_aas;

import java.util.HashSet;
import java.util.Set;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.realm.AuthenticatingRealm;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author heike
 *
 */
public class EgladilBVRealm extends AuthenticatingRealm {

	public static final String EGLADIL_BV_REALM = "egladilBVRealm";

	private static final Logger LOG = LoggerFactory.getLogger(EgladilBVRealm.class);

	/**
	 *
	 */
	public EgladilBVRealm() {
	}

	/**
	 * @param cacheManager
	 */
	public EgladilBVRealm(CacheManager cacheManager) {
		super(cacheManager);
	}

	/**
	 * @param matcher
	 */
	public EgladilBVRealm(CredentialsMatcher matcher) {
		super(matcher);

	}

	/**
	 * @param cacheManager
	 * @param matcher
	 */
	public EgladilBVRealm(CacheManager cacheManager, CredentialsMatcher matcher) {
		super(cacheManager, matcher);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.apache.shiro.realm.AuthenticatingRealm#doGetAuthenticationInfo(org.apache.shiro.authc.AuthenticationToken)
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		SimpleAuthenticationInfo authn = new SimpleAuthenticationInfo(token.getPrincipal(), token.getCredentials(), getName());
		return authn;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.apache.shiro.realm.CachingRealm#getName()
	 */
	@Override
	public String getName() {
		return EGLADIL_BV_REALM;
	}
}
