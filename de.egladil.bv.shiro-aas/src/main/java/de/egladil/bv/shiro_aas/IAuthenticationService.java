//=====================================================
// Projekt: de.egladil.bv.shiro-aas
// (c) Heike Winkelvoß
//=====================================================
	
package de.egladil.bv.shiro_aas;

import org.apache.shiro.authc.Authenticator;

/**
 * @author heikew
 *
 */
public interface IAuthenticationService extends Authenticator {

}
