//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.shiro_aas.storage.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import de.egladil.bv.shiro_aas.entities.Registrierungsbestaetigung;
import de.egladil.bv.shiro_aas.exception.EgladilShiroException;
import de.egladil.bv.shiro_aas.storage.IRegistrierungDao;
import de.egladil.common.persistence.EgladilStorageException;

/**
 * RegistrierungDaoImpl
 */
public class RegistrierungDaoImpl extends BaseDaoImpl<Registrierungsbestaetigung> implements IRegistrierungDao {

	private static final Logger LOG = LoggerFactory.getLogger(RegistrierungDaoImpl.class);

	/**
	 * Erzeugt eine Instanz von RegistrierungDaoImpl
	 */
	@Inject
	public RegistrierungDaoImpl() {
		super();
	}

	/**
	 * @see de.egladil.bv.aas.storage.IRegistrierungDao#findByConfirmationCode(java.lang.String)
	 */
	@Override
	public Registrierungsbestaetigung findByConfirmationCode(String confirmationCode)
		throws EgladilShiroException, EgladilStorageException {
		// TODO Generierter Code
		return null;
	}

	// /**
	// * @see de.egladil.bv.aas.storage.impl.BaseDaoImpl#persist(de.egladil.bv.aas.domain.IDomainObject)
	// */
	// @Override
	// public Registrierungsbestaetigung persist(Registrierungsbestaetigung entity) {
	// EntityTransaction trx = null;
	// Registrierungsbestaetigung result = null;
	// EntityManager em = getEmp().getEntityManager();
	// try {
	// // damit die EnsureRolle nicht neu angelegt wird.
	// em.merge(entity.getBenutzerkonto().getRollen().get(0));
	// trx = em.getTransaction();
	// trx.begin();
	// if (entity.getId() != null) {
	// result = em.merge(entity);
	// } else {
	// em.persist(entity);
	// result = entity;
	// }
	// trx.commit();
	// return result;
	// } catch (Exception e) {
	// if (trx != null && trx.isActive()) {
	// trx.rollback();
	// }
	// Optional<EgladilDuplicateEntryException> integrityException = EgladilConstraintViolationExceptionMapper
	// .toEgladilDuplicateEntryException(e);
	// if (integrityException.isPresent()) {
	// throw integrityException.get();
	// }
	// LOG.error("Fehler in einer Datenbanktransaktion aufgetreten: {}, entity={}", e.getMessage(), entity.toLog(),
	// e);
	// throw new EgladilServerException("Es ist ein Serverfehler aufgetreten", e);
	// }
	// }

	/**
	 * @see de.egladil.bv.aas.storage.IRegistrierungDao#delete(de.egladil.bv.aas.domain.Registrierungsbestaetigung)
	 */
	@Override
	public void delete(Registrierungsbestaetigung entity) throws EgladilShiroException, EgladilStorageException {

	}

}
