//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================
	
package de.egladil.bv.shiro_aas.entities;

/**
* IDomainObject
*/
public interface IDomainObject {
	
	Long getId();

	String toLog();
}
	