//=====================================================
// Projekt: de.egladil.crypto.provider
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.shiro_aas;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.CredentialsMatcher;

import com.google.inject.Inject;

import de.egladil.bv.shiro_aas.domain.EgladilAuthenticationInfo;
import de.egladil.bv.shiro_aas.domain.TransientLoginSecrets;
import de.egladil.bv.shiro_aas.exception.EgladilShiroException;
import de.egladil.crypto.provider.IEgladilCryptoUtils;

/**
 * Prüft, ob das Passwort aus einem UserPasswordToken zu den gespeicherten LoginCredentials aus der AuthenticationInfo
 * passen.
 * 
 * @author heike
 *
 */

public class EgladilCredentialsMatcher implements CredentialsMatcher {

	private final IEgladilCryptoUtils cryptoUtils;

	/**
	 *
	 */
	@Inject
	public EgladilCredentialsMatcher(IEgladilCryptoUtils cryptoUtils) {
		this.cryptoUtils = cryptoUtils;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.apache.shiro.authc.credential.CredentialsMatcher#doCredentialsMatch(org.apache.shiro.authc.
	 * AuthenticationToken, org.apache.shiro.authc.AuthenticationInfo)
	 */
	@Override
	public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
		if (!(token instanceof UsernamePasswordToken)) {
			throw new EgladilShiroException("nur UsernamePasswordToken werden unterstuetzt");
		}
		if (!(info instanceof EgladilAuthenticationInfo)) {

		}
		UsernamePasswordToken usernamePasswordToken = (UsernamePasswordToken) token;
		char[] password = usernamePasswordToken.getPassword();

		EgladilAuthenticationInfo authInfo = (EgladilAuthenticationInfo) info;
		TransientLoginSecrets loginSecrets = (TransientLoginSecrets) authInfo.getCredentials();

		boolean match = cryptoUtils.isPasswordCorrect(password, loginSecrets.getPasswordhash(), loginSecrets.getSaltValue(),
			loginSecrets.getAlgorithmName(), loginSecrets.getNumberIterations());
		return match;
	}
}
