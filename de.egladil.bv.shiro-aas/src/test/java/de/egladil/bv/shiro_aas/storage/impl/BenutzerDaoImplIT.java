//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.shiro_aas.storage.impl;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.bv.shiro_aas.entities.Anwendung;
import de.egladil.bv.shiro_aas.entities.Benutzerkonto;
import de.egladil.bv.shiro_aas.entities.Role;
import de.egladil.bv.shiro_aas.storage.IBenutzerDao;

/**
 * BenutzerDaoTest
 */
public class BenutzerDaoImplIT {

	private static final Logger LOG = LoggerFactory.getLogger(BenutzerDaoImplIT.class);

	private IBenutzerDao benutzerDao;

	@Before
	public void setUp() throws InterruptedException {
		benutzerDao = new BenutzerDaoImpl();
	}

	@Test
	public void should_load_benutzer_by_loginname_succeed() {
		// Arrange
		String loginName = "heike1";

		// Act
		Benutzerkonto benutzerkonto = benutzerDao.findBenutzerByLoginName(loginName, Anwendung.BQ);

		// Assert
		assertNotNull(benutzerkonto);
		assertNotNull(benutzerkonto.rollenAsRoleArray());

		Role[] roles = benutzerkonto.rollenAsRoleArray();
		for (Role r : roles) {
			LOG.debug(r.toString());
		}
		
		assertNotNull(benutzerkonto.getAuthenticationInfo());
	}
}
