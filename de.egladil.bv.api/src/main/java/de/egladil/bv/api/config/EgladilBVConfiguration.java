//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.api.config;

import de.egladil.common.config.AbstractEgladilConfiguration;
import de.egladil.common.config.IEgladilConfiguration;

/**
 * EgladilBVConfiguration
 */
@Deprecated
public class EgladilBVConfiguration extends AbstractEgladilConfiguration implements IEgladilConfiguration {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	/**
	 * Erzeugt eine Instanz von EgladilBVConfiguration
	 */
	public EgladilBVConfiguration() {
		super();
	}

//	/**
//	 * Erzeugt eine Instanz von EgladilBVConfiguration
//	 */
//	public EgladilBVConfiguration(String pathConfigRoot) {
//		// TODO Generierter Code
//		super(pathConfigRoot);
//	}

	/**
	 * @see de.egladil.config.AbstractEgladilConfiguration#getConfigFileName()
	 */
	@Override
	protected String getConfigFileName() {
		return "bv_persistence.properties";
	}

}
