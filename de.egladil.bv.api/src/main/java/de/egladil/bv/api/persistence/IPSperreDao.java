//=====================================================
// Projekt: de.egladil.bv.api
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.api.persistence;

import javax.persistence.EntityManager;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;

import de.egladil.bv.api.domain.IPSperre;

/**
 * IPSperreDao
 */
public class IPSperreDao {

	
	private Provider<EntityManager> emp;

	/**
	 * Erzeugt eine Instanz von IPSperreDao
	 */
	@Inject
	public IPSperreDao(Provider<EntityManager> emf) {
		this.emp = emf;
	}

	@Transactional
	public IPSperre persist(IPSperre sperre) {
		if (sperre.getId() == null) {
			emp.get().persist(sperre);
			return sperre;

		} else {
			IPSperre persistent = emp.get().merge(sperre);
			return persistent;
		}
	}
}
