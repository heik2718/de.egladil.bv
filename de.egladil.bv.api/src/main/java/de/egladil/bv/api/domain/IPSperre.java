//=====================================================
// Projekt: de.egladil.bv.api
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.api.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * IPSperre
 */
@Entity
@Table(name = "ipsperren")
public class IPSperre {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	@Column(name = "IP")
	private String ip;

	@Column(name = "LOGINFAILS")
	private int loginFails;

	@Column(name = "PENALTYEND")
	private Date penaltyEnd;

	/**
	 * Erzeugt eine Instanz von IPSperre
	 */
	public IPSperre() {
		// TODO Generierter Code

	}

	/**
	* Liefert die Membervariable id
	* @return die Membervariable  id
	*/
	public Long getId() {
		return id;
	}

	/**
	* Setzt die Membervariable
	* @param id  neuer Wert der Membervariablen id
	*/
	public void setId(Long id) {
		this.id = id;
	}

	/**
	* Liefert die Membervariable ip
	* @return die Membervariable  ip
	*/
	public String getIp() {
		return ip;
	}

	/**
	* Setzt die Membervariable
	* @param ip  neuer Wert der Membervariablen ip
	*/
	public void setIp(String ip) {
		this.ip = ip;
	}

	/**
	* Liefert die Membervariable loginFails
	* @return die Membervariable  loginFails
	*/
	public int getLoginFails() {
		return loginFails;
	}

	/**
	* Setzt die Membervariable
	* @param loginFails  neuer Wert der Membervariablen loginFails
	*/
	public void setLoginFails(int loginFails) {
		this.loginFails = loginFails;
	}

	/**
	* Liefert die Membervariable penaltyEnd
	* @return die Membervariable  penaltyEnd
	*/
	public Date getPenaltyEnd() {
		return penaltyEnd;
	}

	/**
	* Setzt die Membervariable
	* @param penaltyEnd  neuer Wert der Membervariablen penaltyEnd
	*/
	public void setPenaltyEnd(Date penaltyEnd) {
		this.penaltyEnd = penaltyEnd;
	}

}
