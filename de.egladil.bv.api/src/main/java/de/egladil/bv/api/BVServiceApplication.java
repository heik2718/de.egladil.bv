package de.egladil.bv.api;

import java.util.EnumSet;

import javax.servlet.DispatcherType;

import com.google.inject.Guice;
import com.google.inject.persist.PersistFilter;
import com.google.inject.persist.jpa.JpaPersistModule;
import com.hubspot.dropwizard.guice.GuiceBundle;

import de.egladil.bv.api.config.BVGuiceModule;
import de.egladil.bv.api.config.BVServiceConfiguration;
import de.egladil.bv.api.config.EgladilBVConfiguration;
import de.egladil.common.config.IEgladilConfiguration;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

/**
 * Hello world!
 *
 */
public class BVServiceApplication extends Application<BVServiceConfiguration> {

	private static final String SERVICE_NAME = "BV-REST-API";

	private GuiceBundle<BVServiceConfiguration> guiceBundle;

	private BVServiceConfiguration dropwizardConfiguration;

	public static void main(String[] args) {
		BVServiceApplication application = new BVServiceApplication();
		try {
			application.run(args);
		} catch (Exception e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
			System.exit(1);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see io.dropwizard.Application#getName()
	 */
	@Override
	public String getName() {
		return SERVICE_NAME;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see io.dropwizard.Application#initialize(io.dropwizard.setup.Bootstrap)
	 */
	@Override
	public void initialize(Bootstrap<BVServiceConfiguration> bootstrap) {
		super.initialize(bootstrap);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see io.dropwizard.Application#run(io.dropwizard.Configuration, io.dropwizard.setup.Environment)
	 */
	@Override
	public void run(BVServiceConfiguration configuration, Environment environment) throws Exception {
		Guice.createInjector(new BVGuiceModule(), createJpaPersistModule(configuration));
		environment.servlets().addFilter("persistFilter", guiceBundle.getInjector().getInstance(PersistFilter.class))
			.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), true, "/*");

	}

	private JpaPersistModule createJpaPersistModule(BVServiceConfiguration configuration) {
		IEgladilConfiguration persistenceConfig = new EgladilBVConfiguration();
		persistenceConfig.init(configuration.getConfigRoot());
		JpaPersistModule jpaModule = new JpaPersistModule("bvAPI");
		jpaModule.properties(persistenceConfig.getConfigurationMap());
		return jpaModule;
	}
}
