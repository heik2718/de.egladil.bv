//=====================================================
// Projekt: de.egladil.bv.api
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.api.config;

import io.dropwizard.Configuration;

/**
 * @author heikew
 *
 */
public class BVServiceConfiguration extends Configuration {

	public String configRoot;

	/**
	 *
	 */
	public BVServiceConfiguration() {
	}

	/**
	* Liefert die Membervariable configRoot
	* @return die Membervariable  configRoot
	*/
	public String getConfigRoot() {
		return configRoot;
	}

}
