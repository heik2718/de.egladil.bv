//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.api;

import com.google.inject.AbstractModule;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.PersistService;
import com.google.inject.persist.jpa.JpaPersistModule;

import de.egladil.common.config.AbstractEgladilConfiguration;
import de.egladil.common.config.IEgladilConfiguration;

/**
* DatabaseModule
*/
public class DatabaseModule extends AbstractModule {

	/**
	* @see com.google.inject.AbstractModule#configure()
	*/
	@Override
	protected void configure() {
		install(createPersistModule());
		bind(JPAInitializer.class).asEagerSingleton();
	}

	@Singleton
	public static class JPAInitializer {

		@Inject
		public JPAInitializer(final PersistService service) {
			service.start();
		}

	}

	private JpaPersistModule createPersistModule() {
		IEgladilConfiguration persistenceConfig = new AbstractEgladilConfiguration() {

			/* serialVersionUID */
			private static final long serialVersionUID = 1L;

			@Override
			protected String getConfigFileName() {
				return "bv_persistence.properties";
			}
		};
		JpaPersistModule jpaModule = new JpaPersistModule("bvAPI");
		jpaModule.properties(persistenceConfig.getConfigurationMap());
		return jpaModule;
	}

}
