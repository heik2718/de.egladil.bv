//=====================================================
// Projekt: de.egladil.bv.api
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.api;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;

import de.egladil.bv.api.domain.IPSperre;
import de.egladil.bv.api.persistence.IPSperreDao;

/**
 * @author heikew
 *
 */
public class IPSperreDaoIT {

	@Inject
	private IPSperreDao dao;

	@Before
	public void setUp() {
		AbstractModule module = new AbstractModule() {

			@Override
			protected void configure() {
				bind(IPSperreDao.class);
				bind(IPSperreDaoIT.class);
			}
		};
		Injector injector = Guice.createInjector(module, new DatabaseModule());
		injector.injectMembers(this);
	}

	/**
	 * 
	 */
	public IPSperreDaoIT() {
		System.out.println("create instance of TestClass");
	}
	
	
	@Test
	public void persist_commits(){
		IPSperre ipSperre = new IPSperre();
		ipSperre.setIp("10.12.200.43");
		ipSperre.setLoginFails(0);
		
		IPSperre result = dao.persist(ipSperre);
		
		assertNotNull(result.getId());
	}

}
