//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.domain;

/**
 * @author root
 *
 */
public enum Role {

	BQ_ADMIN,
	BQ_AUTOR,
	BQ_SPIELER,
	BQ_TRANSLATOR,
	MKV_ADMIN,
	MKV_LEHRER,
	MKV_PRIVAT,
	MKM_ADMIN,
	WB_ADMIN,
	WB_AUTOR,
	BV_ADMIN;
}
