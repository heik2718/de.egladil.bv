//=====================================================
// Projekt: de.egladil.persistence.tools
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import de.egladil.bv.aas.domain.Anwendung;

/**
 * ValidAnwendungValidator
 */
public class ClientIdValidator implements ConstraintValidator<ClientId, String> {

	/**
	 * @see javax.validation.ConstraintValidator#initialize(java.lang.annotation.Annotation)
	 */
	@Override
	public void initialize(ClientId constraintAnnotation) {
		// nichts zu tun
	}

	/**
	 * @see javax.validation.ConstraintValidator#isValid(java.lang.Object, javax.validation.ConstraintValidatorContext)
	 */
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (value == null) {
			return true;
		}
		try {
			Anwendung.valueOf(value.trim());
		} catch (IllegalArgumentException e) {
			return false;
		}
		return true;
	}

}
