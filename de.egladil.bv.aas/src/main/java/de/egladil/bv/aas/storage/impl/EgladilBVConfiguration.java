//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.storage.impl;



import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import de.egladil.common.config.AbstractEgladilConfiguration;
import de.egladil.common.config.IEgladilConfiguration;

/**
 * EgladilBVConfiguration
 */
@Singleton
public class EgladilBVConfiguration extends AbstractEgladilConfiguration implements IEgladilConfiguration {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	/**
	 * Erzeugt eine Instanz von EgladilBVConfiguration
	 */
	@Inject
	public EgladilBVConfiguration(@Named("configRoot") final String pathConfigRoot) {
		super(pathConfigRoot);
	}

	/**
	 * @see de.egladil.config.AbstractEgladilConfiguration#getConfigFileName()
	 */
	@Override
	protected String getConfigFileName() {
		return "bv_persistence.properties";
	}

}
