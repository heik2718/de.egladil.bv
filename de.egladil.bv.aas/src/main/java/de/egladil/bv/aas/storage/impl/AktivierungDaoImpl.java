//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.storage.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.bv.aas.storage.BVPersistenceUnit;
import de.egladil.bv.aas.storage.IAktivierungDao;
import de.egladil.common.persistence.BaseDaoImpl;
import de.egladil.common.persistence.EgladilStorageException;

/**
 * RegistrierungDaoImpl
 */
@Singleton
@BVPersistenceUnit
public class AktivierungDaoImpl extends BaseDaoImpl<Aktivierungsdaten> implements IAktivierungDao {

	private static final Logger LOG = LoggerFactory.getLogger(AktivierungDaoImpl.class);

	/**
	 * AktivierungDaoImpl
	 */
	@Inject
	public AktivierungDaoImpl(final Provider<EntityManager> emp) {
		super(emp);
	}

	@Override
	public Aktivierungsdaten findByConfirmationCode(final String confirmationCode) throws EgladilStorageException {
		final String stmt = "SELECT a from Aktivierungsdaten a where a.confirmationCode = :confirmationCode";
		final TypedQuery<Aktivierungsdaten> query = getEntityManager().createQuery(stmt, Aktivierungsdaten.class);
		query.setParameter("confirmationCode", confirmationCode);

		final List<Aktivierungsdaten> trefferliste = query.getResultList();
		LOG.debug("Anzahl Treffer: " + trefferliste.size());
		if (trefferliste.size() > 1) {
			throw new EgladilStorageException("Mehr als ein Eintrag in aktivierungsdaten mit [confirmationCode=" + confirmationCode
				+ "] in der DB: Du erhältst 1 Mio Euro!");
		}
		return trefferliste.isEmpty() ? null : trefferliste.get(0);
	}
}
