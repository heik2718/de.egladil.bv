//=====================================================
// Projekt: de.egladil.email.storage
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.storage.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

import de.egladil.bv.aas.domain.MailqueueItem;
import de.egladil.bv.aas.domain.MailqueueItemStatus;
import de.egladil.bv.aas.storage.BVPersistenceUnit;
import de.egladil.bv.aas.storage.IMailqueueDao;
import de.egladil.common.persistence.BaseDaoImpl;
import de.egladil.common.persistence.EgladilStorageException;

/**
 * MailsDaoImpl
 */
@Singleton
@BVPersistenceUnit
public class MailqueueDaoImpl extends BaseDaoImpl<MailqueueItem> implements IMailqueueDao, Serializable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	private static final Logger LOG = LoggerFactory.getLogger(MailqueueDaoImpl.class);

	/**
	 * Erzeugt eine Instanz von MailsDaoImpl
	 */
	@Inject
	public MailqueueDaoImpl(final Provider<EntityManager> emp) {
		super(emp);
	}

	/**
	 * @see de.egladil.bv.aas.storage.IMailqueueDao#findNextMails()
	 */
	@Override
	public List<MailqueueItem> findNextMails() throws EgladilStorageException {
		final String stmt = "from MailqueueItem m where m.status = :status ";
		final TypedQuery<MailqueueItem> query = getEntityManager().createQuery(stmt, MailqueueItem.class);
		query.setParameter("status", MailqueueItemStatus.WAITING);
		final List<MailqueueItem> mails = query.getResultList();
		LOG.debug("Anzahl Mails = {}", mails == null ? 0 : mails.size());
		return mails == null ? new ArrayList<>() : mails;
	}
}
