//=====================================================
// Projekt: de.egladil.email.storage
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.storage;

import java.util.List;

import de.egladil.bv.aas.domain.MailqueueItem;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.common.persistence.IBaseDao;

/**
 * MailsDao
 */
public interface IMailqueueDao extends IBaseDao<MailqueueItem> {

	/**
	 *
	 * Holt die noch nicht versendeten Mails aus der Tabelle.
	 *
	 * @return
	 */
	List<MailqueueItem> findNextMails() throws EgladilStorageException;
}
