//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.payload;

import javax.validation.constraints.NotNull;

import de.egladil.bv.aas.validation.ValidPasswortAendernAnfrage;
import de.egladil.common.validation.annotations.Passwort;

/**
 * PasswortAendernAnfrage
 */
@ValidPasswortAendernAnfrage
public class PasswortAendernPayload extends AbstractNeuesPasswortPayload {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@NotNull
	@Passwort
	private String passwort;

	/**
	 * @see de.egladil.bv.aas.payload.AbstractNeuesPasswortPayload#clear()
	 */
	@Override
	public void clear() {
		if (this.passwort != null) {
			final char[] charr = passwort.toCharArray();
			for (int i = 0; i < charr.length; i++) {
				charr[i] = 0x00;
			}
			this.passwort = charr.toString();
			this.passwort = null;
		}
		super.clear();
	}

	@Override
	public String toString() {
		return "PasswortAendernPayload [passwort=XXX, passwortNeu=XXX, passwortNeuWdh=XXX, kleber=" + super.getKleber() + "]";
	}

	/**
	 *
	 * @see de.egladil.common.webapp.ILoggable#toBotLog()
	 */
	@Override
	public String toBotLog() {
		return "PasswortAendernPayload [passwort=" + passwort + ", passwortNeu=" + super.getPasswortNeu() + ", passwortNeuWdh="
			+ super.getPasswortNeuWdh() + ", kleber=" + super.getKleber() + "]";
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + superHashCode();
		result = prime * result + ((passwort == null) ? 0 : passwort.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		final PasswortAendernPayload other = (PasswortAendernPayload) obj;
		if (!other.isEqual(obj)) {
			return false;
		}
		if (passwort == null) {
			if (other.passwort != null)
				return false;
		} else if (!passwort.equals(other.passwort))
			return false;
		return true;
	}

	/**
	 * Liefert die Membervariable passwort
	 *
	 * @return die Membervariable passwort
	 */
	@Override
	public String getPasswort() {
		return passwort;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param passwort neuer Wert der Membervariablen passwort
	 */
	@Override
	public void setPasswort(final String passwort) {
		this.passwort = passwort;
	}
}
