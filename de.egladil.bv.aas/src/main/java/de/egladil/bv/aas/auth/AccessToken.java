package de.egladil.bv.aas.auth;

import javax.validation.constraints.NotNull;

import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Das AccessToken enthält die für die Authentisierung und Autorisierung relevanten Informationen.<br>
 * <ul>
 * <li><b>accessTokenId:</b> das eigentliche Token</li>
 * <li><b>primaryPrincipal:</b> Identifier für den angemeldeten Benutzer. Hier die UUID des Benutzerkontos, die mit den
 * anwendungsspezifischen Usern verknüpft ist.</li>
 * <li><b>lastAccessUTC:</b> die letzte Zugriffszeit. Wird bei jedem autorisierten Serverroundtrip aktualisiert.</li>
 * <li><b>csrfToken:</b> das CSRF-Token</li>
 * </ul>
 */
public class AccessToken {

	@JsonProperty("access_token_id")
	@NotNull
	private String accessTokenId;

	@JsonProperty("primary_principal")
	@NotNull
	private String primaryPrincipal;

	@JsonProperty("last_access_utc")
	@NotNull
	private DateTime lastAccessUTC;

	@JsonIgnore
	private String csrfToken;

	/**
	 * Erzeugt eine Instanz von AccessToken
	 */
	public AccessToken(final String accessTokenId) {
		this.accessTokenId = accessTokenId;
	}

	/**
	 * Erzeugt eine Instanz von AccessToken
	 */
	public AccessToken(final String accessTokenId, final String primaryPrincipal) {
		this.accessTokenId = accessTokenId;
		this.primaryPrincipal = primaryPrincipal;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("AccessToken [accessTokenId=");
		builder.append(accessTokenId);
		builder.append(", primaryPrincipal=");
		builder.append(primaryPrincipal);
		builder.append("]");
		return builder.toString();
	}

	public String getAccessTokenId() {
		return accessTokenId;
	}

	public String getPrimaryPrincipal() {
		return primaryPrincipal;
	}

	public void setPrimaryPrincipal(final String primaryPrincipal) {
		this.primaryPrincipal = primaryPrincipal;
	}

	public DateTime getLastAccessUTC() {
		return lastAccessUTC;
	}

	public void setLastAccessUTC(final DateTime lastAccessUTC) {
		this.lastAccessUTC = lastAccessUTC;
	}

	public String getCsrfToken() {
		return csrfToken;
	}

	public void setCsrfToken(final String xsrfToken) {
		this.csrfToken = xsrfToken;
	}
}
