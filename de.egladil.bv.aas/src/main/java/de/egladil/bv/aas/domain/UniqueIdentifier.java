//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import de.egladil.common.validation.annotations.UuidString;

/**
 * Objekt, das eine uuid wrapped.
 */
public class UniqueIdentifier {

	@NotNull
	@UuidString
	@Size(min = 1, max = 40)
	private final String uuid;

	/**
	 * Erzeugt eine Instanz von BenutzerIdentifier
	 */
	public UniqueIdentifier(String uuid) {
		super();
		this.uuid = uuid;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UniqueIdentifier other = (UniqueIdentifier) obj;
		if (uuid == null) {
			if (other.uuid != null)
				return false;
		} else if (!uuid.equals(other.uuid))
			return false;
		return true;
	}

	/**
	 * Liefert die Membervariable uuid
	 *
	 * @return die Membervariable uuid
	 */
	public String getUuid() {
		return uuid;
	}
}
