//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.payload;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import de.egladil.common.persistence.ILoggable;
import de.egladil.common.validation.annotations.Honeypot;
import de.egladil.common.validation.annotations.Passwort;

/**
 * AbstractNeuesPasswortAnfrage
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({ @Type(value = PasswortAendernPayload.class, name = "changePwd"),
	@Type(value = TempPwdAendernPayload.class, name = "changeTempPwd") })
public abstract class AbstractNeuesPasswortPayload implements ILoggable, Serializable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@NotNull
	@Email
	@Pattern(regexp = ".+@.+\\..+", message = "{org.hibernate.validator.constraints.Email.message}")
	private String email;

	@NotNull
	@Passwort
	private String passwortNeu;

	@NotNull
	@Passwort
	private String passwortNeuWdh;

	@Honeypot
	private String kleber;

	/**
	 * Flag, mit dem geprüft werden kann, ob die clear-Methode gelaufen ist wegen HeapDumps.
	 */
	@JsonIgnore
	private boolean cleared;

	/**
	 * @see de.egladil.common.webapp.ILoggable#toBotLog()
	 */
	@Override
	public String toBotLog() {
		return "NeuesPasswortAnfrage [passwortNeu=" + passwortNeu + ", passwortNeuWdh=" + passwortNeuWdh + ", kleber=" + kleber
			+ "]";
	}

	@Override
	public String toString() {
		return "NeuesPasswortAnfrage [passwortNeu=XXX, passwortNeuWdh=XXX, kleber=" + kleber + "]";
	}

	protected int superHashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((kleber == null) ? 0 : kleber.hashCode());
		result = prime * result + ((passwortNeu == null) ? 0 : passwortNeu.hashCode());
		result = prime * result + ((passwortNeuWdh == null) ? 0 : passwortNeuWdh.hashCode());
		return result;
	}

	protected boolean isEqual(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final AbstractNeuesPasswortPayload other = (AbstractNeuesPasswortPayload) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (kleber == null) {
			if (other.kleber != null)
				return false;
		} else if (!kleber.equals(other.kleber))
			return false;
		if (passwortNeu == null) {
			if (other.passwortNeu != null)
				return false;
		} else if (!passwortNeu.equals(other.passwortNeu))
			return false;
		if (passwortNeuWdh == null) {
			if (other.passwortNeuWdh != null)
				return false;
		} else if (!passwortNeuWdh.equals(other.passwortNeuWdh))
			return false;
		return true;
	}

	/**
	 * Liefert die Membervariable email
	 *
	 * @return die Membervariable email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param email neuer Wert der Membervariablen email
	 */
	public void setEmail(final String email) {
		this.email = email;
	}

	/**
	 * Liefert die Membervariable passwortNeu
	 *
	 * @return die Membervariable passwortNeu
	 */
	public String getPasswortNeu() {
		return passwortNeu;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param passwortNeu neuer Wert der Membervariablen passwortNeu
	 */
	public void setPasswortNeu(final String passwortNeu) {
		this.passwortNeu = passwortNeu;
	}

	/**
	 * Liefert die Membervariable passwortNeuWdh
	 *
	 * @return die Membervariable passwortNeuWdh
	 */
	public String getPasswortNeuWdh() {
		return passwortNeuWdh;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param passwortNeuWdh neuer Wert der Membervariablen passwortNeuWdh
	 */
	public void setPasswortNeuWdh(final String passwortNeuWdh) {
		this.passwortNeuWdh = passwortNeuWdh;
	}

	/**
	 * Liefert die Membervariable kleber
	 *
	 * @return die Membervariable kleber
	 */
	public String getKleber() {
		return kleber;
	}

	public void setKleber(final String kleber) {
		this.kleber = kleber;
	}

	public abstract String getPasswort();

	public abstract void setPasswort(String passwort);

	/**
	 * Überschreibt passwortNeu und passwortNeuWdh mit 0 bevor sie null gesetzt werden.
	 */
	public void clear() {
		if (this.passwortNeu != null) {
			final char[] charr = passwortNeu.toCharArray();
			for (int i = 0; i < charr.length; i++) {
				charr[i] = 0x00;
			}
			this.passwortNeu = charr.toString();
			this.passwortNeu = null;
		}
		if (this.passwortNeuWdh != null) {
			final char[] charr = passwortNeuWdh.toCharArray();
			for (int i = 0; i < charr.length; i++) {
				charr[i] = 0x00;
			}
			this.passwortNeuWdh = charr.toString();
			this.passwortNeuWdh = null;
		}
		if (this.getPasswort() != null) {
			final char[] charr = getPasswort().toCharArray();
			for (int i = 0; i < charr.length; i++) {
				charr[i] = 0x00;
			}
			setPasswort(charr.toString());
			setPasswort(null);
		}
		cleared = true;
	}

	/**
	 * In Tests kann damit geprüft werden, ob die clear-Methode gelaufen ist wegen HeapDumps.
	 *
	 * @return die Membervariable cleared
	 */
	public boolean isCleared() {
		return cleared;
	}

}
