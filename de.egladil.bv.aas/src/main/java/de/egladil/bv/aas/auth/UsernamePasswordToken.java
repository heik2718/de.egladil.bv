//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.auth;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Objekt, das für die Authentisierung verwender wird. Es enthält von den {@link Credentials} nur die zur
 * Authentisierung relevanten Daten.
 */
public class UsernamePasswordToken implements Serializable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	/**
	 * The username
	 */
	private String username;

	/**
	 * The password, in char[] format
	 */
	private char[] password;

	/**
	 * Host, von dem aus sich jemand einloggt oder <code>null</code>, falls unbekannt.
	 */
	private String host;

	/**
	 * Flag, mit dem geprüft werden kann, ob die clear-Methode gelaufen ist wegen HeapDumps.
	 */
	private boolean cleared;

	/**
	 * Erzeugt eine Instanz von UsernamePasswordToken
	 */
	public UsernamePasswordToken(String username, char[] password) {
		this.username = username;
		this.password = password;
	}

	/**
	 * Erzeugt eine Instanz von UsernamePasswordToken
	 */
	public UsernamePasswordToken(String username, char[] password, String host) {
		this.username = username;
		this.password = password;
		this.host = host;
	}

	/**
	 * Erzeugt eine Instanz von UsernamePasswordToken
	 */
	public UsernamePasswordToken() {
	}

	/**
	 * Setzt alle attribute auf null oder den default zurück. die password bytes werden zunächst auf <tt>0x00</tt>
	 * gestzet, bevor sie genullt werden, damit im heap dump kein Klartextpasswort mehr auftauscht.
	 * 
	 */
	public void clear() {
		this.username = null;
		if (this.password != null) {
			for (int i = 0; i < password.length; i++) {
				this.password[i] = 0x00;
			}
			this.password = null;
		}
		cleared = true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(password);
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UsernamePasswordToken other = (UsernamePasswordToken) obj;
		if (!Arrays.equals(password, other.password))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

	/**
	 * Liefert die Membervariable username
	 *
	 * @return die Membervariable username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param username neuer Wert der Membervariablen username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Liefert die Membervariable password
	 *
	 * @return die Membervariable password
	 */
	public char[] getPassword() {
		return password;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param password neuer Wert der Membervariablen password
	 */
	public void setPassword(char[] password) {
		this.password = password;
	}

	/**
	 * Liefert die Membervariable host
	 *
	 * @return die Membervariable host
	 */
	public String getHost() {
		return host;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param host neuer Wert der Membervariablen host
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	* Liefert die Membervariable cleared
	* @return die Membervariable  cleared
	*/
	public boolean isCleared() {
		return cleared;
	}

}
