//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.auth.impl;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import de.egladil.common.config.IEgladilConfiguration;
import de.egladil.crypto.provider.CryptoConfigurationKeys;
import de.egladil.crypto.provider.impl.EgladilCryptoConfiguration;

/**
 * CsrfTokenCache
 */
@Singleton
public class CsrfTokenCache {

	private LoadingCache<String, String> cache;

	/**
	 * CsrfTokenCache
	 */
	public CsrfTokenCache() {
	}

	/**
	 * Erzeugt eine Instanz von CsrfTokenCache
	 */
	@Inject
	public CsrfTokenCache(@Named("configRoot") final String pathConfigRoot) {
		init(pathConfigRoot);
	}

	private void init(final String pathConfigRoot) {
		final IEgladilConfiguration cryptoConfig = new EgladilCryptoConfiguration(pathConfigRoot);
		cryptoConfig.init(pathConfigRoot);
		final int minutesExpiration = cryptoConfig.getIntegerProperty(CryptoConfigurationKeys.LOGIN_ACCESS_TOKEN_EXPIRE_TIME);
		final long maxSize = cryptoConfig.getLongProperty(CryptoConfigurationKeys.LOGIN_ACCESS_TOKEN_CACHE_SIZE);
		cache = CacheBuilder.newBuilder().maximumSize(maxSize).expireAfterWrite(minutesExpiration, TimeUnit.MINUTES)
			.build(new CacheLoader<String, String>() {

				@Override
				public String load(final String key) throws Exception {
					return UUID.randomUUID().toString();
				}
			});
	}

	/**
	 * @param tokenKey String darf nicht null sein.
	 * @return String oder null.
	 * @throws NullPointerException weitergereicht von com.goolgle.common.cache - falls tokenKey null.
	 */
	public String findCsrfTokenIfPresent(final String tokenKey) throws NullPointerException {
		return cache.getIfPresent(tokenKey);
	}

	/**
	 * Packt den tokenKey als key und value in den Cache.
	 *
	 * @param tokenKey String darf nicht null sein
	 * @throws NullPointerException weitergereicht von com.goolgle.common.cache - falls tokenKey null.
	 */
	public void register(final String tokenKey) throws NullPointerException {
		cache.put(tokenKey, tokenKey);
	}

	/**
	 * Das AccessToken wird rausgekantet.
	 *
	 * @param accessTokenId String darf nicht null sein.
	 * @throws NullPointerException weitergereicht von com.goolgle.common.cache - falls accessTokenId null.
	 */
	public void invalidate(final String accessTokenId) throws NullPointerException {
		cache.invalidate(accessTokenId);
	}

	/**
	 * Aktualisiert das Token.
	 *
	 * @param csrfToken String darf nicht null sein.
	 * @throws NullPointerException - weitergereicht von com.goolgle.common.cache - falls csrfToken null.
	 */
	public void refresh(final String csrfToken) throws NullPointerException {
		cache.put(csrfToken, csrfToken);
	}
}
