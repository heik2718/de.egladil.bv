//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.egladil.common.persistence.utils.PrettyStringUtils;


/**
 * @author root
 *
 */
public enum Anwendung {

	BQ("Blitzquiz") {

		@Override
		public List<Role> getAssignedRollen() {
			return Arrays.asList(new Role[] { Role.BQ_ADMIN, Role.BQ_AUTOR, Role.BQ_TRANSLATOR, Role.BQ_SPIELER });
		}

	},
	BV("Benutzerverwaltung") {

		@Override
		public List<Role> getAssignedRollen() {
			return Arrays.asList(new Role[] { Role.BQ_ADMIN, Role.MKV_ADMIN, Role.WB_ADMIN });
		}

	},
	MKV("Minikänguru") {

		@Override
		public List<Role> getAssignedRollen() {
			return Arrays.asList(new Role[] { Role.MKV_ADMIN, Role.MKV_LEHRER, Role.MKV_PRIVAT });
		}

	},
	MKM("Minikänguru-Manufaktur") {

		@Override
		public List<Role> getAssignedRollen() {
			return Arrays.asList(new Role[] { Role.MKM_ADMIN });
		}

	},
	WB("Winkels Blog") {

		@Override
		public List<Role> getAssignedRollen() {
			return Arrays.asList(new Role[] { Role.WB_ADMIN, Role.WB_AUTOR });
		}

	};

	private final String name;

	/**
	 * Erzeugt eine Instanz von Mandant
	 */
	private Anwendung(String name) {
		this.name = name;
	}

	/**
	 *
	 * Erzeugt eine Liste mit den Ergebnissen von toString().
	 *
	 * @return List
	 */
	public static List<String> asStringList() {
		List<String> result = new ArrayList<>();
		for (Anwendung m : Anwendung.values()) {
			result.add(m.toString());
		}
		return result;
	}

	/**
	 * Liefert die Membervariable name
	 *
	 * @return die Membervariable name
	 */
	public String getName() {
		return name;
	}

	public abstract List<Role> getAssignedRollen();

	public static String getAll() {
		return PrettyStringUtils.collectionToDefaultString(Anwendung.asStringList());
	}

}
