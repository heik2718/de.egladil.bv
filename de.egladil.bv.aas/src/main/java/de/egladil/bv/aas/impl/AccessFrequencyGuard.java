//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.impl;

import java.util.Date;
import java.util.Optional;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.PersistenceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.domain.LoginSecrets;
import de.egladil.bv.aas.storage.IBenutzerDao;
import de.egladil.common.config.GlobalConstants;
import de.egladil.common.exception.ExcessiveAttemptsException;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.common.persistence.PersistenceExceptionMapper;

/**
 * AccessFrequencyGuard überwacht die Häufigkeit eines Zugriffs auf ein Benutzerkonto.
 */
@Singleton
public class AccessFrequencyGuard {

	private static final Logger LOG = LoggerFactory.getLogger(AccessFrequencyGuard.class);

	private final PeriodChecker periodChecker = new PeriodChecker();

	private IBenutzerDao benutzerDao;

	private int anzCheckFrequencyCalled = 0;

	/**
	* AccessFrequencyGuard
	*/
	public AccessFrequencyGuard() {
	}

	/**
	 * Erzeugt eine Instanz von AccessFrequencyDelegate
	 */
	@Inject
	public AccessFrequencyGuard(final IBenutzerDao benutzerDao) {
		this.benutzerDao = benutzerDao;
	}

	/**
	 * Methode prüft, ob es sich um einen Bot handeln könnte. PersistenceExceptions werden nur geloggt.
	 *
	 * @param benutzerkonto
	 * @return Benutzerkonto das gemergete Benutzerkonto, dessen lastLoginAttempt aktualisiert wurde oder im Fall einer
	 * EgladilConcurrentModificationException neu ausgelesen.
	 */
	public synchronized Benutzerkonto checkFrequency(final Benutzerkonto benutzerkonto, final long delayMilliseconds)
		throws IllegalArgumentException, ExcessiveAttemptsException, EgladilConcurrentModificationException,
		EgladilStorageException {
		anzCheckFrequencyCalled++;
		final Date now = new Date();
		final LoginSecrets secrets = benutzerkonto.getLoginSecrets();
		final Date lastLoginAttempt = secrets.getLastLoginAttempt();
		secrets.setLastLoginAttempt(now);
		Benutzerkonto gespeichert = null;
		try {
			gespeichert = benutzerDao.persist(benutzerkonto);
			if (lastLoginAttempt != null) {
				if (periodChecker.isPeriodLessEqualExpectedPeriod(lastLoginAttempt, now, delayMilliseconds)) {
					final String message = "Benutzerkonto  mit [loginname=" + benutzerkonto.getLoginName() + ", Anwendung="
						+ benutzerkonto.getAnwendung() + "] scheint attackiert zu werden";
					LOG.warn(GlobalConstants.LOG_PREFIX_BOT + message);
					throw new ExcessiveAttemptsException(message);
				}
			}
		} catch (final PersistenceException e) {
			final Optional<EgladilConcurrentModificationException> optEx = PersistenceExceptionMapper
				.toEgladilConcurrentModificationException(e);
			final String concUpdateMessage = "Konkurrierendes Update beim Speichern des letzten Loginversuchs:"
				+ benutzerkonto.toString();
			if (optEx.isPresent()) {
				LOG.warn(concUpdateMessage);
				gespeichert = benutzerDao.findBenutzerByUUID(benutzerkonto.getUuid());
			} else {
				final String msg = "Exception beim Speichern des letzten Loginversuchs durch Benutzer " + benutzerkonto.toString()
					+ ": " + e.getMessage();
				throw new EgladilStorageException(msg, e);
			}
		}
		return gespeichert;
	}

	int getAnzCheckFrequencyCalled() {
		return anzCheckFrequencyCalled;
	}

	void setAnzCheckFrequencyCalled(final int checkFrequencyCalled) {
		this.anzCheckFrequencyCalled = checkFrequencyCalled;
	}
}
