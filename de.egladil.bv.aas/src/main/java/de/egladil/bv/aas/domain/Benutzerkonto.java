//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import de.egladil.bv.aas.auth.AuthenticationInfo;
import de.egladil.bv.aas.auth.EgladilPrincipal;
import de.egladil.bv.aas.auth.PrincipalMap;
import de.egladil.common.persistence.IDomainObject;
import de.egladil.common.validation.annotations.StringLatin;
import de.egladil.common.validation.annotations.UuidString;

/**
 * Ein Objekt dieses Typs stellt lediglich die Attribute zur Verfügung, die zur Authentisierung und Autorisierung
 * erforderlich sind.
 *
 * @author heike
 *
 */
@Entity
@Table(name = "benutzer")
public class Benutzerkonto implements AuthenticationInfo, Serializable, IDomainObject, IAktivierbar, ISperrbar {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	@NotBlank
	@StringLatin
	@Column(name = "LOGINNAME", length = 255)
	private String loginName;

	@NotBlank
	@Email
	@Column(name = "EMAIL", length = 255)
	private String email;

	@Column(name = "AKTIVIERT")
	@Convert(converter = BooleanToIntegerConverter.class)
	private boolean aktiviert;

	@Column(name = "ANONYM")
	@Convert(converter = BooleanToIntegerConverter.class)
	private boolean anonym;

	@Column(name = "GESPERRT")
	@Convert(converter = BooleanToIntegerConverter.class)
	private boolean gesperrt;

	@OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL }, mappedBy = "benutzerkonto", orphanRemoval = true)
	private List<Aktivierungsdaten> aktivierungsdaten = new ArrayList<>();

	@OneToMany(fetch = FetchType.EAGER, cascade = { CascadeType.MERGE, CascadeType.REMOVE })
	@JoinTable(name = "benutzerrollen", joinColumns = {
		@JoinColumn(name = "BENUTZER_ID", referencedColumnName = "ID") }, inverseJoinColumns = {
			@JoinColumn(name = "ROLLE_ID", referencedColumnName = "ID") })
	private List<Rolle> rollen = new ArrayList<>();

	@NotNull
	@Column(name = "ANWENDUNG")
	@Enumerated(EnumType.STRING)
	private Anwendung anwendung;

	@Version
	@Column(name = "VERSION")
	private int version;

	@OneToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.EAGER)
	@JoinColumn(name = "PID", referencedColumnName = "ID")
	private LoginSecrets loginSecrets;

	@UuidString
	@NotNull
	@Size(min = 1, max = 40)
	@Column(name = "UUID")
	private String uuid;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DATE_MODIFIED")
	private Date datumGeaendert;

	@Transient
	private PrincipalMap principalMap = new PrincipalMap();

	/**
	 * Collection of all string-based permissions associated with the account.
	 */
	@Transient
	private Set<String> stringPermissions = new HashSet<>();

	/**
	 *
	 */
	public Benutzerkonto() {
	}

	public void addAktivierungsdaten(final Aktivierungsdaten daten) {
		if (this.aktivierungsdaten == null) {
			this.aktivierungsdaten = new ArrayList<>();
		}
		if (!this.aktivierungsdaten.contains(daten)) {
			this.aktivierungsdaten.add(daten);
			daten.setBenutzerkonto(this);
		}
	}

	public void removeAktivierungsdaten(final Aktivierungsdaten daten) {
		if (this.aktivierungsdaten == null || !this.aktivierungsdaten.contains(daten)) {
			return;
		}
		this.aktivierungsdaten.remove(daten);
		daten.setBenutzerkonto(null);
	}

	public void clearAktivierungsdaten() {
		if (this.aktivierungsdaten != null) {
			this.aktivierungsdaten.clear();
		}
	}

	/**
	 * Sucht die Aktivierungsdaten mit dem gegeben confirmationCode.
	 *
	 * @param confirmCode
	 * @return
	 */
	public Optional<Aktivierungsdaten> findAktivierungsdatenByConfirmCode(final String confirmCode) {
		if (confirmCode == null) {
			throw new IllegalArgumentException("confirmCode null");
		}
		if (aktivierungsdaten == null) {
			return Optional.empty();
		}
		for (final Aktivierungsdaten d : aktivierungsdaten) {
			if (d.getConfirmationCode().equals(confirmCode)) {
				return Optional.of(d);
			}
		}
		return Optional.empty();
	}

	public void addRolle(final Rolle rolle) {
		if (!this.rollen.contains(rolle)) {
			this.rollen.add(rolle);
		}
	}

	@Override
	public String toString() {
		return "Benutzerkonto [loginName=" + loginName + ", email=" + email + ", anwendung=" + anwendung + ", aktiviert="
			+ aktiviert + ", gesperrt=" + gesperrt + ", uuid=" + uuid + "]";
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Benutzerkonto other = (Benutzerkonto) obj;
		if (uuid == null) {
			if (other.uuid != null)
				return false;
		} else if (!uuid.equals(other.uuid))
			return false;
		return true;
	}

	/**
	 * @return
	 */
	public Collection<String> getRoles() {
		final List<String> result = new ArrayList<>();
		for (final Rolle r : rollen) {
			result.add(r.getRole().toString());
		}
		return result;
	}

	public Collection<String> getStringPermissions() {
		return this.stringPermissions;
	}

	public void addStringPermission(final String permission) {
		if (this.stringPermissions == null) {
			this.stringPermissions = new HashSet<>();
		}
		this.stringPermissions.add(permission);
	}

	/**
	 * @see de.egladil.bv.aas.auth.AuthenticationInfo#getPrincipalMap()
	 */
	@Override
	public PrincipalMap getPrincipalMap() {
		if (this.principalMap.isEmpty()) {
			this.principalMap.add(PrincipalMap.TECH_ID, new EgladilPrincipal(this.id.toString()));
			this.principalMap.add(PrincipalMap.KEY_EMAIL, new EgladilPrincipal(this.email));
			this.principalMap.add(PrincipalMap.KEY_LOGINNAME, new EgladilPrincipal(this.loginName));
			this.principalMap.add(PrincipalMap.KEY_UUID, new EgladilPrincipal(this.uuid));
		}
		return this.principalMap;
	}

	/**
	 *
	 * @see de.egladil.common.persistence.IDomainObject#getId()
	 */
	@Override
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(final Long id) {
		this.id = id;
	}

	/**
	 * @return the loginName
	 */
	public String getLoginName() {
		return loginName;
	}

	/**
	 * @param loginName the loginName to set
	 */
	public void setLoginName(final String loginName) {
		this.loginName = loginName;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(final String email) {
		this.email = email;
	}

	/**
	 * @param aktiv the aktiv to set
	 */
	public void setAktiviert(final boolean aktiv) {
		this.aktiviert = aktiv;
	}

	/**
	 * @return the anwendung
	 */
	public Anwendung getAnwendung() {
		return anwendung;
	}

	/**
	 * @param anwendung the anwendung to set
	 */
	public void setAnwendung(final Anwendung anwendung) {
		this.anwendung = anwendung;
	}

	/**
	 * @return the loginSecrets
	 */
	public LoginSecrets getLoginSecrets() {
		return loginSecrets;
	}

	/**
	 * @param loginSecrets the loginSecrets to set
	 */
	public void setLoginSecrets(final LoginSecrets loginSecrets) {
		this.loginSecrets = loginSecrets;
	}

	/**
	 * @param gesperrt the gesperrt to set
	 */
	public void setGesperrt(final boolean gesperrt) {
		this.gesperrt = gesperrt;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(final int version) {
		this.version = version;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(final String uuid) {
		this.uuid = uuid;
	}

	/**
	 *
	 * @see de.egladil.bv.aas.domain.IAktivierbar#isAktiviert()
	 */
	@Override
	public boolean isAktiviert() {
		return aktiviert;
	}

	/**
	 *
	 * @see de.egladil.bv.aas.domain.ISperrbar#isGesperrt()
	 */
	@Override
	public boolean isGesperrt() {
		return gesperrt;
	}

	/**
	 * Liefert die Membervariable rollen
	 *
	 * @return die Membervariable rollen
	 */
	public List<Rolle> getRollen() {
		return rollen;
	}

	/**
	 * @param role
	 * @return
	 */
	public boolean hasRole(final Role role) {
		return getRoles().contains(role.toString());
	}

	public Role[] rollenAsRoleArray() {
		final Role[] result = new Role[this.rollen.size()];
		int index = 0;
		for (final Rolle r : this.rollen) {
			result[index++] = r.getRole();
		}
		return result;
	}

	/**
	 * unmodifiable List!!!
	 *
	 * @return die Membervariable registrierungsbestaetigungen
	 */
	public List<Aktivierungsdaten> getAktivierungsdaten() {
		return Collections.unmodifiableList(aktivierungsdaten);
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param aktivierungsdaten neuer Wert der Membervariablen aktivierungsdaten
	 */
	void setAktivierungsdaten(final List<Aktivierungsdaten> aktivierungsdaten) {
		this.aktivierungsdaten = aktivierungsdaten;
	}

	/**
	 * Liefert die Membervariable anonym
	 *
	 * @return die Membervariable anonym
	 */
	public boolean isAnonym() {
		return anonym;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param anonym neuer Wert der Membervariablen anonym
	 */
	public void setAnonym(final boolean anonym) {
		this.anonym = anonym;
	}

	public Date getDatumGeaendert() {
		return datumGeaendert;
	}
}
