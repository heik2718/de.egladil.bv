//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.auth.impl;

import java.security.Principal;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentMap;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.joda.time.DateTime;

import de.egladil.bv.aas.auth.AccessToken;
import de.egladil.bv.aas.auth.AuthenticationInfo;
import de.egladil.bv.aas.auth.IAccessTokenDAO;

/**
 * CachingAccessTokenDao. Nicht mehr verwenden, weil das Cachen verhindert, dass die Session gelöscht wird (bug im
 * google-cache).
 */
@Singleton
public class CachingAccessTokenDao implements IAccessTokenDAO {

	private AccessTokenCache accessTokenCache;

	private CsrfTokenCache csrfTokenCache;

	/**
	 * CachingAccessTokenDao
	 */
	public CachingAccessTokenDao() {
	}

	/**
	 * Erzeugt eine Instanz von CachingAccessTokenDao
	 */
	@Inject
	public CachingAccessTokenDao(final AccessTokenCache accessTokenCache, final CsrfTokenCache csrfTokenCache) {
		this.accessTokenCache = accessTokenCache;
		this.csrfTokenCache = csrfTokenCache;
	}

	/**
	 * @see de.egladil.bv.aas.auth.IAccessTokenDAO#generateNewAccessToken(de.egladil.bv.aas.auth.AuthenticationInfo)
	 */
	@Override
	public AccessToken generateNewAccessToken(final AuthenticationInfo benutzerkonto) {
		final Optional<Principal> primaryPrincipal = benutzerkonto.getPrincipalMap().getPrimaryPrincipal();

		// Hier könnte man sich einen SecureRandom-Generator denken, aber UUID ist erstmal sicher und ausreichend, wenn
		// auch recht lang.
		final AccessToken accessToken = new AccessToken(UUID.randomUUID().toString(), primaryPrincipal.get().getName());
		accessToken.setLastAccessUTC(new DateTime());
		accessToken.setCsrfToken(UUID.randomUUID().toString());
		accessTokenCache.refresh(accessToken);
		return accessToken;
	}

	@Override
	public String refresh(final AccessToken accessToken) {
		accessTokenCache.refresh(accessToken);
		return "";
	}

	@Override
	public Optional<AccessToken> findAccessTokenById(final String accessTokenId) throws NullPointerException {
		final AccessToken accessToken = accessTokenCache.findAccessTokenIfPresent(accessTokenId);
		return accessToken == null ? Optional.empty() : Optional.of(accessToken);
	}

	@Override
	public Optional<AccessToken> findAccessTokenByPrimaryPrincipal(final String benutzerUuid) throws NullPointerException {

		if (benutzerUuid == null) {
			throw new NullPointerException("benutzerUuid");
		}
		final ConcurrentMap<String, AccessToken> allEntries = accessTokenCache.getAllEntries();
		for (final AccessToken accessToken : allEntries.values()) {
			if (benutzerUuid.equals(accessToken.getPrimaryPrincipal())) {
				return Optional.of(accessToken);
			}
		}
		return Optional.empty();
	}

	@Override
	public boolean isTemporaryCsrfToken(final String csrfToken) {
		final String token = csrfTokenCache.findCsrfTokenIfPresent(csrfToken);
		return token != null;
	}

	@Override
	public String invalidateAccessToken(final String accessTokenId) {
		// invalidate tut nicht, was man erwartet. Daher primary principal austauschen fürs LOG
		// accessTokenCache.invalidate(accessTokenId);
		final Optional<AccessToken> optToken = this.findAccessTokenById(accessTokenId);
		if (optToken.isPresent()) {
			final AccessToken token = optToken.get();
			token.setPrimaryPrincipal(null);
			accessTokenCache.refresh(token);
		}

		return "";

	}

	@Override
	public String registerTemporaryCsrfToken(final String csrfToken) {
		csrfTokenCache.register(csrfToken);
		return "";
	}

	@Override
	public String refreshCsrfToken(final String csrfToken) {
		csrfTokenCache.refresh(csrfToken);
		return csrfToken;
	}

	@Override
	public String invalidateTemporaryCsrfToken(final String csrfToken) {
		csrfTokenCache.invalidate(csrfToken);
		return "";
	}
}
