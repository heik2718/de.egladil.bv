//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * ValidRegistrierungsanfrage
 */
@Target({ ElementType.TYPE, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = RegistrierungsanfrageValidator.class)
@Documented
public @interface ValidRegistrierungsanfrage {

	String message() default "{de.egladil.constraints.anwendung_role}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
