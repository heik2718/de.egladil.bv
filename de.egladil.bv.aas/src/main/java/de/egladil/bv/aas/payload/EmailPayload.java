//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.payload;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;

import de.egladil.common.persistence.ILoggable;
import de.egladil.common.validation.annotations.Honeypot;

/**
 *
 * Payload das nur eine Mailadresse und den Kleber hat.
 */
public class EmailPayload implements ILoggable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@NotNull
	@Email
	@Pattern(regexp = ".+@.+\\..+", message = "{org.hibernate.validator.constraints.Email.message}")
	private String email;

	@Honeypot
	private String kleber;

	@Override
	public String toString() {
		return "EmailPayload [email=" + email + ", kleber=" + kleber + "]";
	}

	/**
	 * @see de.egladil.common.persistence.ILoggable#toBotLog()
	 */
	@Override
	public String toBotLog() {
		return "EmailPayload [email=" + email + ", kleber=" + kleber + "]";
	}

	/**
	 * Liefert die Membervariable email
	 *
	 * @return die Membervariable email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param email neuer Wert der Membervariablen email
	 */
	public void setEmail(final String email) {
		this.email = email;
	}

	/**
	 * Liefert die Membervariable kleber
	 *
	 * @return die Membervariable kleber
	 */
	public String getKleber() {
		return kleber;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param kleber neuer Wert der Membervariablen kleber
	 */
	public void setKleber(final String kleber) {
		this.kleber = kleber;
	}
}
