//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import de.egladil.common.persistence.IDomainObject;

/**
 * @author heike
 *
 */
@Entity
@Table(name = "kat_rollen")
public class Rolle implements Serializable, IDomainObject {

	/**
	 *
	 */
	private static final long serialVersionUID = 2L;

	@Id
	private Long id;

	@Enumerated(EnumType.STRING)
	@Column(name = "NAME")
	private Role role;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "rolle")
	private List<Berechtigung> berechtigungen;

	/**
	 *
	 */
	public Rolle() {
		super();
	}

	/**
	 * @param role
	 */
	public Rolle(final Role role) {
		super();
		this.role = role;
	}

	@Override
	public String toString() {
		return "EnsureRole [role=" + role + "]";
	}

	/**
	 * @return the role
	 */
	public Role getRole() {
		return role;
	}

	/**
	 * @return the berechtigungen
	 */
	public List<Berechtigung> getBerechtigungen() {
		return berechtigungen;
	}

	/**
	 *
	 * @see de.egladil.common.persistence.IDomainObject#getId()
	 */
	@Override
	public Long getId() {
		return id;
	}

	public static Rolle findByRole(final List<Rolle> rollen, final Role role) {
		for (final Rolle r : rollen) {
			if (r.getRole().equals(role)) {
				return r;
			}
		}
		return null;
	}

}
