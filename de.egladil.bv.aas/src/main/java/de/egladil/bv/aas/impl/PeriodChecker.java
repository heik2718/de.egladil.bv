//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.impl;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * PeriodChecker
 */
public class PeriodChecker {

	private static final Logger LOG = LoggerFactory.getLogger(PeriodChecker.class);

	/**
	 * Vergleicht die Länge des Zeitintervalls zwischen startDate und endDate mit der erwarteten Zeitspanne.
	 *
	 * @param startDate Date
	 * @param endDate Date
	 * @param expectedPeriodMillis long Anzahl Millisekunden, die zwischen startDate und endDate liegen sollen.
	 * @return boolean true, wenn kürzer oder gleich lang wie expectedLengthPeriod, false sonst.
	 */
	public boolean isPeriodLessEqualExpectedPeriod(Date startDate, Date endDate, long expectedPeriodMillis) {
		long diff = endDate.getTime() - startDate.getTime();
		LOG.debug("Interval: {}, Ende: {}, Start: {}, Differenz: {}", expectedPeriodMillis, endDate.getTime(), startDate.getTime(),
			diff);
		return diff <= expectedPeriodMillis;
	}
}
