//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.impl;

import java.io.Serializable;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.PersistenceException;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.domain.Role;
import de.egladil.bv.aas.storage.IBenutzerDao;
import de.egladil.common.config.TimeUtils;
import de.egladil.common.exception.DisabledAccountException;
import de.egladil.common.exception.EgladilAuthenticationException;
import de.egladil.common.exception.EgladilBVException;
import de.egladil.common.exception.UnknownAccountException;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.common.persistence.PersistenceExceptionMapper;
import de.egladil.crypto.provider.CryptoConfigurationKeys;
import de.egladil.crypto.provider.IEgladilCryptoUtils;

/**
 * BenutzerServiceImpl
 */
@Singleton
public class BenutzerServiceImpl implements Serializable, IBenutzerService {

	/* serialVersionUID */
	private static final long serialVersionUID = 2L;

	private static final Logger LOG = LoggerFactory.getLogger(BenutzerServiceImpl.class);

	private IBenutzerDao benutzerDao;

	private IEgladilCryptoUtils cryptoUtils;

	private AccessFrequencyGuard accessFrequencyGuard;

	private Integer delayMilliseconds;

	private String tempPasswordForTest;

	/**
	 * BenutzerServiceImpl
	 */
	public BenutzerServiceImpl() {
	}

	/**
	 * Erzeugt eine Instanz von BenutzerServiceImpl
	 */
	@Inject
	public BenutzerServiceImpl(final IBenutzerDao benutzerDao, final AccessFrequencyGuard accessFrequencyGuard,
		final IEgladilCryptoUtils cryptoUtils) {
		this.benutzerDao = benutzerDao;
		this.accessFrequencyGuard = accessFrequencyGuard;
		this.cryptoUtils = cryptoUtils;
		init();
	}

	@PostConstruct
	public void init() {
		delayMilliseconds = cryptoUtils.getConfig().getIntegerProperty(CryptoConfigurationKeys.LOGIN_BOT_DELAY);
	}

	/**
	 * Sperrt oder entsperrt das zum subject gehörende Benutzerkonto.
	 *
	 * @param subject
	 */
	public void toggleSperre(final Subject subject) {
	}

	@Override
	public Benutzerkonto findBenutzerkontoByLoginName(final String loginName, final Anwendung anwendung)
		throws EgladilStorageException, EgladilBVException {
		return benutzerDao.findBenutzerByLoginName(loginName, anwendung);
	}

	@Override
	public Benutzerkonto findBenutzerkontoByUUID(final String principalName) {
		if (principalName == null) {
			throw new IllegalArgumentException("principalName null");
		}
		return benutzerDao.findBenutzerByUUID(principalName);
	}

	@Override
	public Optional<Benutzerkonto> findBenutzerkontoByFirstCharsOfUUID(final String firstChars)
		throws IllegalArgumentException, EgladilStorageException {
		if (StringUtils.isBlank(firstChars)) {
			throw new IllegalArgumentException("firstChars darf nicht blank sein");
		}
		return benutzerDao.findBenutzerByMostSignificantCharsOfUUID(firstChars);
	}

	@Override
	public List<Benutzerkonto> findByEmailLike(final String email, final Anwendung anwendung)
		throws PersistenceException, IllegalArgumentException {

		if (StringUtils.isBlank(email)) {
			throw new IllegalArgumentException("email blank");
		}
		if (anwendung == null) {
			throw new IllegalArgumentException("anwendung null");
		}
		return benutzerDao.findByEmailLike(email, anwendung);
	}

	@Override
	public Aktivierungsdaten generateResetPasswordCode(final String email, final Anwendung anwendung, final int expiresInMinutes)
		throws EgladilAuthenticationException, DisabledAccountException, EgladilStorageException, IllegalArgumentException,
		EgladilStorageException {
		if (email == null) {
			throw new IllegalArgumentException("email null");
		}
		if (anwendung == null) {
			throw new IllegalArgumentException("anwendung null");
		}
		Benutzerkonto benutzerkonto = benutzerDao.findBenutzerByEmail(email, anwendung);
		if (benutzerkonto == null) {
			LOG.warn("Reset Passwort: Benutzerkonto mit email [{}] und Anwendung [{}] nicht gefunden", email, anwendung);
			throw new UnknownAccountException("Benutzerkonto nicht gefunden");
		}
		if (!benutzerkonto.isAktiviert() || benutzerkonto.isGesperrt()) {
			throw new DisabledAccountException("Benutzerkonto  mit [loginname oder email=" + email + ", Anwendung=" + anwendung
				+ "] noch nicht oder nicht mehr aktiviert");
		}
		benutzerkonto = accessFrequencyGuard.checkFrequency(benutzerkonto, delayMilliseconds);
		final Aktivierungsdaten aktivierungsdaten = createAktivierungsdaten(expiresInMinutes);
		benutzerkonto.addAktivierungsdaten(aktivierungsdaten);
		try {
			final Benutzerkonto persistentBenutzer = benutzerDao.persist(benutzerkonto);
			final Optional<Aktivierungsdaten> result = persistentBenutzer
				.findAktivierungsdatenByConfirmCode(aktivierungsdaten.getConfirmationCode());
			return result.get();
		} catch (final PersistenceException e) {
			final Optional<EgladilDuplicateEntryException> integrityException = PersistenceExceptionMapper
				.toEgladilDuplicateEntryException(e);
			if (integrityException.isPresent()) {
				throw integrityException.get();
			}
			final Optional<EgladilConcurrentModificationException> optEx = PersistenceExceptionMapper
				.toEgladilConcurrentModificationException(e);
			if (optEx.isPresent()) {
				final String msg = "Jemand anders hat das entsprechende Benutzerkonto inzwischen geändert";
				throw new EgladilConcurrentModificationException(msg);
			}
			throw new EgladilStorageException("Fehler in einer Datenbanktransaktion", e);
		} catch (final Exception e) {
			throw new EgladilBVException("Fehler beim Erzeugen eines temporären Passworts", e);
		}
	}

	private Aktivierungsdaten createAktivierungsdaten(final int expireMinutes) {
		final Aktivierungsdaten aktivierungsdaten = new Aktivierungsdaten();
		final String confirmationCode = tempPasswordForTest != null ? tempPasswordForTest : cryptoUtils.generateShortUuid();
		aktivierungsdaten.setConfirmationCode(confirmationCode);

		final Date jetzt = new Date();
		final Date expireTime = TimeUtils.calculateExpireTime(jetzt, expireMinutes, ChronoUnit.MINUTES);
		aktivierungsdaten.setExpirationTime(expireTime);
		return aktivierungsdaten;
	}

	@Override
	public Benutzerkonto persistBenutzerkonto(final Benutzerkonto benutzer) throws EgladilStorageException {
		if (benutzer == null) {
			throw new IllegalArgumentException("Parameter benutzer darf nicht null sein!");
		}
		try {
			return benutzerDao.persist(benutzer);
		} catch (final PersistenceException e) {
			final Optional<EgladilDuplicateEntryException> integrityException = PersistenceExceptionMapper
				.toEgladilDuplicateEntryException(e);
			if (integrityException.isPresent()) {
				throw integrityException.get();
			}
			final Optional<EgladilConcurrentModificationException> optEx = PersistenceExceptionMapper
				.toEgladilConcurrentModificationException(e);
			if (optEx.isPresent()) {
				final String msg = "Jemand anders hat die Benutzerdaten inzwischen geändert";
				throw new EgladilConcurrentModificationException(msg);
			}
			throw new EgladilStorageException("Fehler in einer Datenbanktransaktion", e);
		}
	}

	@Override
	public boolean isBenutzerInRole(final Role role, final String principalName)
		throws IllegalArgumentException, EgladilStorageException {
		if (role == null) {
			throw new IllegalArgumentException("role null");
		}
		final Benutzerkonto benutzer = findBenutzerkontoByUUID(principalName);
		if (benutzer == null) {
			LOG.error("Benutzer mit [UUID=" + principalName + "] nicht vorhanden");
			return false;
		}
		return benutzer.hasRole(role);
	}

	@Override
	public void setTempPasswordForTest(final String tempPwd) {
		tempPasswordForTest = tempPwd;
	}
}
