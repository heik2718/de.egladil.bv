package de.egladil.bv.aas.config;

import de.egladil.bv.aas.storage.BVPersistenceUnit;
import de.egladil.bv.aas.storage.IAktivierungDao;
import de.egladil.bv.aas.storage.IBenutzerDao;
import de.egladil.bv.aas.storage.IMailqueueDao;
import de.egladil.bv.aas.storage.IRollenDao;
import de.egladil.bv.aas.storage.impl.AktivierungDaoImpl;
import de.egladil.bv.aas.storage.impl.BenutzerDaoImpl;
import de.egladil.bv.aas.storage.impl.MailqueueDaoImpl;
import de.egladil.bv.aas.storage.impl.RollenDaoImpl;
import de.egladil.common.config.AbstractEgladilConfiguration;
import de.egladil.common.config.IEgladilConfiguration;
import de.egladil.common.persistence.JpaPersistPrivateModule;

public class BVPersistenceModule extends JpaPersistPrivateModule {

	public BVPersistenceModule(String pathConfigRoot) {
		super(BVPersistenceUnit.class, pathConfigRoot);
	}

	@Override
	protected String getPersistenceUnitName() {
		return "bvPU";
	}

	@Override
	protected IEgladilConfiguration getConfigurations() {
		return new AbstractEgladilConfiguration(getPathConfigRoot()) {

			/* serialVersionUID */
			private static final long serialVersionUID = 1L;

			@Override
			protected String getConfigFileName() {
				return "bv_persistence.properties";
			}
		};
	}

	@Override
	protected void doConfigure() {
		bind(IBenutzerDao.class).to(BenutzerDaoImpl.class);
		expose(IBenutzerDao.class);

		bind(IRollenDao.class).to(RollenDaoImpl.class);
		expose(IRollenDao.class);

		bind(IAktivierungDao.class).to(AktivierungDaoImpl.class);
		expose(IAktivierungDao.class);

		bind(IMailqueueDao.class).to(MailqueueDaoImpl.class);
		expose(IMailqueueDao.class);
	}

}
