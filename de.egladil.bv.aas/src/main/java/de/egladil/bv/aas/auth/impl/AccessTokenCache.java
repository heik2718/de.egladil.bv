//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.auth.impl;

import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import de.egladil.bv.aas.auth.AccessToken;
import de.egladil.common.config.IEgladilConfiguration;
import de.egladil.crypto.provider.CryptoConfigurationKeys;
import de.egladil.crypto.provider.impl.EgladilCryptoConfiguration;

/**
 * AccessTokenCache ist initialisiert mit expireAfterWrite. Dies realisiert einen Timeout-Mechanismus bei Inaktivität.
 */
@Singleton
public class AccessTokenCache {

	/**
	 * Der Key ist die accessTokenId (also die UUID des angemeldeten Benutzers).
	 */
	private LoadingCache<String, AccessToken> cache;

	/**
	* AccessTokenCache
	*/
	public AccessTokenCache() {
	}

	/**
	 * Erzeugt eine Instanz von AccessTokenCache
	 */
	@Inject
	public AccessTokenCache(@Named("configRoot") final String pathConfigRoot) {
		init(pathConfigRoot);
	}

	private void init(final String pathConfigRoot) {
		final IEgladilConfiguration cryptoConfig = new EgladilCryptoConfiguration(pathConfigRoot);
		cryptoConfig.init(pathConfigRoot);
		final int minutesExpiration = cryptoConfig.getIntegerProperty(CryptoConfigurationKeys.LOGIN_ACCESS_TOKEN_EXPIRE_TIME);
		final long maxSize = cryptoConfig.getLongProperty(CryptoConfigurationKeys.LOGIN_ACCESS_TOKEN_CACHE_SIZE);

		cache = CacheBuilder.newBuilder().maximumSize(maxSize).expireAfterWrite(minutesExpiration, TimeUnit.MINUTES)
			.build(new CacheLoader<String, AccessToken>() {

				@Override
				public AccessToken load(final String accessTokenId) throws Exception {
					return new AccessToken(accessTokenId);
				}
			});
	}

	/**
	 * Sucht das AccessToken anhand des primaryPrincipals.
	 *
	 * @param accessTokenId
	 * @return AccessToken oder null;
	 * @throws NullPointerException weitergereicht von com.goolgle.common.cache - falls accessTokenId null.
	 */
	public AccessToken findAccessTokenIfPresent(final String accessTokenId) throws NullPointerException {
		return cache.getIfPresent(accessTokenId);
	}

	/**
	 * @return
	 */
	public ConcurrentMap<String, AccessToken> getAllEntries() {
		return cache.asMap();
	}

	/**
	 * Tauscht das AccessToken aus.
	 *
	 * @param accessToken
	 * @throws NullPointerException weitergereicht von com.goolgle.common.cache - falls accessToken null.
	 */
	public void refresh(final AccessToken accessToken) throws NullPointerException {
		cache.put(accessToken.getAccessTokenId(), accessToken);
	}

	/**
	 * Das AccessToken wird rausgekantet.
	 *
	 * @param accessTokenId
	 * @throws NullPointerException weitergereicht von com.goolgle.common.cache - falls accessTokenId null.
	 */
	public void invalidate(final String accessTokenId) throws NullPointerException {
		cache.invalidate(accessTokenId);
	}
}
