//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.auth;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.common.persistence.ILoggable;
import de.egladil.common.validation.annotations.Honeypot;
import de.egladil.common.validation.annotations.Passwort;

/**
 * Die EmailBasedCredentials enthalten die für eine Authentisierung relevanten Informationen, wenn die Email-Adresse als
 * username fungiert.<br>
 * <ul>
 * <li><b>username:</b> die Mailadresse</li>
 * <li><b>password:</b> das Passwort</li>
 * <li><b>clientId:</b> die Kennung der Anwendung (siehe {@link Anwendung})</li>
 * <li><b>kleber:</b> Inhalt eines Honeypot-Felds im Client zum Ausschluss von Bot-Attacken</li>
 * <li><b>temporaryCsrfToken:</b> das beim Aufruf einer Anwendung von Server erzeugte CSRF-Token</li>
 * </ul>
 */
public class EmailBasedCredentials implements ILoggable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@NotNull
	@Email
	@Pattern(regexp = ".+@.+\\..+", message = "{org.hibernate.validator.constraints.Email.message}")
	@JsonProperty
	private String username;

	@Passwort
	@NotBlank
	@JsonProperty
	private String password;

	@Honeypot
	@JsonProperty
	private String kleber;

	/**
	 * Flag, mit dem geprüft werden kann, ob die clear-Methode gelaufen ist wegen HeapDumps.
	 */
	@JsonIgnore
	private boolean cleared;

	/**
	 * Erzeugt eine Instanz von EmailBasedCredentials
	 */
	public EmailBasedCredentials() {
		// nix weiter
	}

	/**
	 * Erzeugt eine Instanz von EmailBasedCredentials
	 */
	public EmailBasedCredentials(final String email, final String password, final String kleber) {
		this.username = email;
		this.password = password;
		this.kleber = kleber;
	}

	/**
	 * Setzt alle attribute auf null oder den default zurück. die password bytes werden zunächst auf <tt>0x00</tt>
	 * gestzet, bevor sie genullt werden, damit im heap dump kein Klartextpasswort mehr auftauscht.
	 */
	public void clear() {
		this.username = null;
		if (this.password != null) {
			final char[] charr = password.toCharArray();
			for (int i = 0; i < charr.length; i++) {
				charr[i] = 0x00;
			}
			this.password = charr.toString();
			this.password = null;
		}
		cleared = true;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		result = prime * result + ((kleber == null) ? 0 : kleber.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final EmailBasedCredentials other = (EmailBasedCredentials) obj;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		if (kleber == null) {
			if (other.kleber != null)
				return false;
		} else if (!kleber.equals(other.kleber))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		return true;
	}

	@Override
	public String toBotLog() {
		return "EmailBasedCredentials [email=" + username + ", password=" + password + ", kleber=" + kleber + "]";
	}

	@Override
	public String toString() {
		return "EmailBasedCredentials [email=" + username + ", password=XXX, kleber=" + kleber + "]";
	}

	/**
	 * Liefert die Membervariable cleared
	 *
	 * @return die Membervariable cleared
	 */
	public boolean isCleared() {
		return cleared;
	}

	/**
	 * Liefert die Membervariable username
	 *
	 * @return die Membervariable username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Liefert die Membervariable password
	 *
	 * @return die Membervariable password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Liefert die Membervariable kleber
	 *
	 * @return die Membervariable kleber
	 */
	public String getKleber() {
		return kleber;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param username neuer Wert der Membervariablen username
	 */
	protected void setUsername(final String username) {
		this.username = username;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param password neuer Wert der Membervariablen password
	 */
	protected void setPassword(final String password) {
		this.password = password;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param kleber neuer Wert der Membervariablen kleber
	 */
	protected void setKleber(final String kleber) {
		this.kleber = kleber;
	}
}
