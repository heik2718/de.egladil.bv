//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.auth;

import java.io.Serializable;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * PrincipalMap ist ein Wrapper für eine Map mit den Principals, die für meine Anwendungen erforderlich sind.
 */
public class PrincipalMap implements Serializable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	public static final String TECH_ID = "technicalId";

	public static final String KEY_UUID = "UUID";

	public static final String KEY_EMAIL = "Email";

	public static final String KEY_LOGINNAME = "login";

	private Map<String, Principal> principalMap = new HashMap<>();

	public boolean isEmpty() {
		return this.principalMap == null || this.principalMap.isEmpty();
	}

	/**
	 *
	 * Fügt ein Principal hinzu.
	 *
	 * @param key
	 * @param value
	 */
	public void add(String key, Principal value) {
		principalMap.put(key, value);
	}

	/**
	 *
	 * Gibt das vorranige identifizierende Attribut zurück.
	 *
	 * @return
	 */
	public Optional<Principal> getPrimaryPrincipal() {
		if (isEmpty()) {
			return Optional.empty();
		}
		return Optional.of(principalMap.get(KEY_UUID));
	}

	/**
	 * @return
	 */
	public List<Principal> getAllPrincipals() {
		if (isEmpty()) {
			return Collections.emptyList();
		}
		List<Principal> result = new ArrayList<>();
		result.addAll(this.principalMap.values());
		return result;
	}

	/**
	 * Gibt das Principal mit dem gegebenen Key zurück, falls vorhanden. TODO
	 *
	 * @param key
	 * @return
	 */
	public Optional<Principal> getByKey(String key) {
		if (this.isEmpty() || !this.principalMap.containsKey(key)) {
			return Optional.empty();
		}
		return Optional.of(this.principalMap.get(key));
	}
}
