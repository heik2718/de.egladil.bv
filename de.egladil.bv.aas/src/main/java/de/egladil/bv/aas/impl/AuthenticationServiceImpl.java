//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.impl;

import java.io.Serializable;
import java.util.Base64;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.PersistenceException;

import org.apache.shiro.crypto.hash.Hash;
import org.hibernate.validator.constraints.Email;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.bv.aas.IAuthenticationService;
import de.egladil.bv.aas.auth.AccessTokenUtils;
import de.egladil.bv.aas.auth.AuthenticationInfo;
import de.egladil.bv.aas.auth.UsernamePasswordToken;
import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.domain.LoginSecrets;
import de.egladil.bv.aas.domain.Salz;
import de.egladil.bv.aas.storage.IBenutzerDao;
import de.egladil.common.exception.DisabledAccountException;
import de.egladil.common.exception.EgladilAuthenticationException;
import de.egladil.common.exception.EgladilBVException;
import de.egladil.common.exception.IncorrectCredentialsException;
import de.egladil.common.exception.UnknownAccountException;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.common.persistence.PersistenceExceptionMapper;
import de.egladil.common.validation.annotations.UuidString;
import de.egladil.crypto.provider.CryptoConfigurationKeys;
import de.egladil.crypto.provider.IEgladilCryptoUtils;

/**
 * @author root
 *
 */
@Singleton
public class AuthenticationServiceImpl implements IAuthenticationService, Serializable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	private static final Logger LOG = LoggerFactory.getLogger(AuthenticationServiceImpl.class);

	private Long delayMilliSeconds;

	private IBenutzerDao benutzerDao;

	private IEgladilCryptoUtils cryptoUtils;

	private AccessFrequencyGuard accessFrequencyGuard;

	/**
	 * AuthenticationServiceImpl
	 */
	public AuthenticationServiceImpl() {
	}

	/**
	 * Erzeugt eine Instanz von AuthenticationServiceImpl
	 */
	@Inject
	public AuthenticationServiceImpl(final IBenutzerDao benutzerDao, final AccessFrequencyGuard accessFrequencyGuard,
		final IEgladilCryptoUtils cryptoUtils) {
		this.benutzerDao = benutzerDao;
		this.accessFrequencyGuard = accessFrequencyGuard;
		this.cryptoUtils = cryptoUtils;
		init();
	}

	@PostConstruct
	private void init() {
		delayMilliSeconds = Long.valueOf(cryptoUtils.getConfig().getIntegerProperty(CryptoConfigurationKeys.LOGIN_BOT_DELAY));
	}

	@Override
	public AuthenticationInfo authenticate(final UsernamePasswordToken authenticationToken, final Anwendung anwendung)
		throws IllegalArgumentException, EgladilAuthenticationException, DisabledAccountException, EgladilStorageException {
		if (!(authenticationToken instanceof UsernamePasswordToken)) {
			throw new IllegalArgumentException("Funktioniert nur fuer UsernamePasswordToken");
		}
		final UsernamePasswordToken usernamePasswordToken = authenticationToken;
		Benutzerkonto benutzerkonto = benutzerDao.findBenutzerByLoginName(usernamePasswordToken.getUsername(), anwendung);
		if (benutzerkonto == null) {
			benutzerkonto = benutzerDao.findBenutzerByEmail(usernamePasswordToken.getUsername(), anwendung);

			if (benutzerkonto == null) {
				throw new UnknownAccountException("Kein Benutzerkonto mit [loginname oder email="
					+ usernamePasswordToken.getUsername() + ", Anwendung=" + anwendung + "] bekannt");
			}
		}
		if (!benutzerkonto.isAktiviert() || benutzerkonto.isGesperrt()) {
			throw new DisabledAccountException("Benutzerkonto  mit [loginname oder email=" + usernamePasswordToken.getUsername()
				+ ", Anwendung=" + anwendung + "] noch nicht oder nicht mehr aktiviert");
		}
		benutzerkonto = accessFrequencyGuard.checkFrequency(benutzerkonto, delayMilliSeconds);
		this.verifyPasswort(usernamePasswordToken.getPassword(), benutzerkonto);
		usernamePasswordToken.clear();
		return benutzerkonto;
	}

	@Override
	public AuthenticationInfo authenticateWithTemporaryPasswort(final Anwendung anwendung, @Email final String email,
		@UuidString final String password)
		throws IllegalArgumentException, EgladilAuthenticationException, DisabledAccountException {
		if (anwendung == null) {
			throw new IllegalArgumentException("anwendung null");
		}
		if (email == null) {
			throw new IllegalArgumentException("email null");
		}
		if (password == null) {
			throw new IllegalArgumentException("password null");
		}
		Benutzerkonto benutzerkonto = benutzerDao.findBenutzerByEmail(email, anwendung);
		if (benutzerkonto == null) {
			throw new UnknownAccountException("Kein Benutzerkonto mit [email=" + email + ", Anwendung=" + anwendung + "] bekannt");
		}
		final Optional<Aktivierungsdaten> optAkt = benutzerkonto.findAktivierungsdatenByConfirmCode(password);
		if (!optAkt.isPresent()) {
			throw new IncorrectCredentialsException("email stimmt, temporaeres passwort nicht");
		}
		if (benutzerkonto.isGesperrt()) {
			throw new DisabledAccountException("Das Benutzerkonto " + benutzerkonto.toString() + " ist gesperrt.");
		}
		benutzerkonto = accessFrequencyGuard.checkFrequency(benutzerkonto, delayMilliSeconds);
		return benutzerkonto;
	}

	void verifyPasswort(final char[] password, final Benutzerkonto benutzerkonto) throws IncorrectCredentialsException {
		final LoginSecrets loginSecrets = benutzerkonto.getLoginSecrets();
		final Salz salz = benutzerkonto.getLoginSecrets().getSalz();

		final boolean korrekt = cryptoUtils.isPasswordCorrect(password, loginSecrets.getPasswordhash(), salz.getWert(),
			salz.getAlgorithmName(), salz.getIterations());
		if (!korrekt) {
			throw new IncorrectCredentialsException("loginname stimmt, passwort nicht");
		}
	}

	@Override
	public Optional<Benutzerkonto> changePasswort(final Anwendung anwendung, final String benutzerUuid, final char[] pwdNeu)
		throws EgladilStorageException, EgladilBVException, EgladilDuplicateEntryException, EgladilConcurrentModificationException,
		IllegalArgumentException, UnknownAccountException {
		if (anwendung == null || benutzerUuid == null || pwdNeu == null) {
			throw new IllegalArgumentException(anwendung == null ? "anwendung"
				: (benutzerUuid == null ? "benutzerUuid" : (pwdNeu == null ? "pwdNeu" : "seltsam")));
		}
		try {
			final Benutzerkonto benutzerkonto = benutzerDao.findBenutzerByUUID(benutzerUuid);
			if (benutzerkonto == null) {
				LOG.warn("Possible Attac: kein Benutzerkonto mit UUID {} bekannt. Anwendung: {}", benutzerUuid, anwendung);
				throw new UnknownAccountException("kein passendes Benutzerkonto gefunden.");
			}
			if (!benutzerkonto.getAnwendung().equals(anwendung)) {
				LOG.warn("Possible Attac: Anwendung und UUID passen nicht zusammen: [UUID={}, Anwendung={}]", benutzerUuid,
					anwendung);
				throw new UnknownAccountException("Anwendung und UUID passen nicht zusammen.");
			}

			final Hash hash = cryptoUtils.hashPassword(pwdNeu);

			final LoginSecrets loginSecrets = new LoginSecrets();
			loginSecrets.setPasswordhash(Base64.getEncoder().encodeToString(hash.getBytes()));

			final Salz salz = new Salz();
			salz.setAlgorithmName(hash.getAlgorithmName());
			salz.setIterations(hash.getIterations());
			salz.setWert(hash.getSalt().toBase64());
			loginSecrets.setSalz(salz);

			benutzerkonto.setLoginSecrets(loginSecrets);
			final Benutzerkonto result = this.benutzerDao.persist(benutzerkonto);
			LOG.debug("Benutzer {} geaendert", result.toString());
			return Optional.of(result);
		} catch (final PersistenceException e) {
			final Optional<EgladilDuplicateEntryException> integrityException = PersistenceExceptionMapper
				.toEgladilDuplicateEntryException(e);
			if (integrityException.isPresent()) {
				throw integrityException.get();
			}
			final Optional<EgladilConcurrentModificationException> optEx = PersistenceExceptionMapper
				.toEgladilConcurrentModificationException(e);
			if (optEx.isPresent()) {
				final String msg = "Jemand anders das Passwort inzwischen geändert";
				throw new EgladilConcurrentModificationException(msg);
			}
			throw new EgladilStorageException("Fehler in einer Datenbanktransaktion", e);
		} catch (final UnknownAccountException e) {
			throw e;
		} catch (final Exception e) {
			throw new EgladilBVException("Fehler beim Ändern des Passworts", e);
		}
	}

	/**
	 * Generiert ein secure random.
	 *
	 * @return String
	 */
	@Override
	public String createCsrfToken() {
		final String salt = cryptoUtils.generateRandomString(128);
		final String token = salt + System.currentTimeMillis();
		final String checksum = new AccessTokenUtils().shaChecksum(token);
		return checksum;
	}
}
