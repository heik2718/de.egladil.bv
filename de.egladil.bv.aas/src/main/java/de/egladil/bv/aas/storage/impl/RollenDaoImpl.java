//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.storage.impl;

import java.io.Serializable;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

import de.egladil.bv.aas.domain.Role;
import de.egladil.bv.aas.domain.Rolle;
import de.egladil.bv.aas.storage.BVPersistenceUnit;
import de.egladil.bv.aas.storage.IRollenDao;
import de.egladil.common.persistence.BaseDaoImpl;
import de.egladil.common.persistence.EgladilStorageException;

/**
 * RollenDaoImpl
 */
@Singleton
@BVPersistenceUnit
public class RollenDaoImpl extends BaseDaoImpl<Rolle> implements Serializable, IRollenDao {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	/**
	 * Erzeugt eine Instanz von RollenDaoImpl
	 */
	@Inject
	public RollenDaoImpl(final Provider<EntityManager> emp) {
		super(emp);
	}

	@Override
	public Rolle findByRole(final Role role) throws EgladilStorageException {
		final TypedQuery<Rolle> query = getEntityManager().createQuery("from Rolle r where r.role = :role", Rolle.class);
		query.setParameter("role", role);
		try {
			final Rolle rolle = query.getSingleResult();
			return rolle;
		} catch (final Exception e) {
			final String msg = "Konnte Rolle mit Role=" + role + " nicht finden. " + e.getMessage();
			throw new EgladilStorageException(msg, e);
		}
	}
}
