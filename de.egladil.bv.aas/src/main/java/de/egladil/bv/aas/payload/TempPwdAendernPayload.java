//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.payload;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import de.egladil.bv.aas.validation.ValidTempPwdAendernPayload;
import de.egladil.common.validation.annotations.UuidString;

/**
 * TempPwdAendernPayload
 */
@ValidTempPwdAendernPayload
public class TempPwdAendernPayload extends AbstractNeuesPasswortPayload {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@NotNull
	@UuidString
	@Size(min = 1, max = 40)
	private String passwort;

	@Override
	public String toString() {
		return "TempPwdAendernPayload [tempPassword=XXX, passwortNeu=XXX, passwortNeuWdh=XXX, kleber=" + super.getKleber() + "]";
	}

	@Override
	public String toBotLog() {
		return "TempPwdAendernPayload [passwort=" + passwort + ", passwortNeu=" + super.getPasswortNeu() + ", passwortNeuWdh="
			+ super.getPasswortNeuWdh() + ", kleber=" + super.getKleber() + "]";
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + superHashCode();
		result = prime * result + ((passwort == null) ? 0 : passwort.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		final TempPwdAendernPayload other = (TempPwdAendernPayload) obj;
		if (!other.isEqual(obj)) {
			return false;
		}
		if (passwort == null) {
			if (other.passwort != null)
				return false;
		} else if (!passwort.equals(other.passwort))
			return false;
		return true;
	}

	@Override
	public String getPasswort() {
		return passwort;
	}

	@Override
	public void setPasswort(final String passwort) {
		this.passwort = passwort;
	}
}
