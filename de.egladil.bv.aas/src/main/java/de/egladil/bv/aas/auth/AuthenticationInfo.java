//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.auth;

import java.io.Serializable;

/**
 * Die Daten eines authentisierten Users im Kontext von JAAS.
 */
public interface AuthenticationInfo extends Serializable {

	/**
	 * Aus dieser wird im wesentlichen der primaryPrincipal verwendet.
	 * 
	 * @return PrincipalMap
	 */
	PrincipalMap getPrincipalMap();
}
