//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas;

import javax.validation.ConstraintViolationException;

import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.domain.UniqueIdentifier;
import de.egladil.bv.aas.payload.Registrierungsanfrage;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.EgladilStorageException;

/**
 * IRegistrierungService
 */
public interface IRegistrierungService {

	/**
	 * Erzeugt ein Benutzerkonto sowie eine temporär gültige Registrierungsbestätigung zur gegebenen Anwendung mit der
	 * gegebenen EnsureRole. Die Registrierungsbestätigung wird zurückgegeben und kann von den Aufrufern als Mail versendet
	 * werden.<br>
	 * <br>
	 * <strong>Achtung:</strong> Die Exception-Message kann schützenswerte Details enthalten. Also nicht einfach an
	 * Clients weitergeben.
	 *
	 * @param loginName
	 * @param email
	 * @param passwort
	 * @param role
	 * @param anwendung
	 * @return Registrierungsbestaetigung
	 */
	Aktivierungsdaten register(Registrierungsanfrage registrierungsanfrage) throws ConstraintViolationException,
		IllegalArgumentException, EgladilDuplicateEntryException, EgladilConcurrentModificationException, EgladilStorageException;

	/**
	 * Sucht die zum confirmationCode gehörende Registrierungsbestätigung.
	 *
	 * @param confirmationCode
	 * @return Registrierungsbestaetigung oder null
	 */
	Aktivierungsdaten findByConfirmationCode(UniqueIdentifier confirmationCode);

	/**
	 * Aktiviert das zur registrierungsbestaetigung gehörende Benutzerkonto und gibt es zurück. Alle fachlichen
	 * Validierungen (expiration check usw.) müssen bereits erfolgt sein. Hier wird nur noch das Benutzerkonto aktivert.
	 *
	 * @param aktivierungsdaten Aktivierungsdaten
	 * @return Benutzerkonto
	 * @throws EgladilServerException
	 */
	Benutzerkonto activateBenutzer(Aktivierungsdaten aktivierungsdaten)
		throws IllegalArgumentException, ConstraintViolationException, EgladilDuplicateEntryException,
		EgladilConcurrentModificationException, EgladilStorageException;
}
