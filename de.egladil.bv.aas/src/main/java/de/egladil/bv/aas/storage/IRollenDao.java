//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.storage;

import de.egladil.bv.aas.domain.Role;
import de.egladil.bv.aas.domain.Rolle;
import de.egladil.common.exception.EgladilBVException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.common.persistence.IBaseDao;

/**
 * IRollenDao
 */
public interface IRollenDao extends IBaseDao<Rolle> {

	/**
	 * Tja, was wohl.
	 *
	 * @param role
	 * @return EnsureRole
	 * @throws EgladilServerException
	 * @throws EgladilStorageException
	 */
	Rolle findByRole(Role role) throws EgladilBVException, EgladilStorageException;
}
