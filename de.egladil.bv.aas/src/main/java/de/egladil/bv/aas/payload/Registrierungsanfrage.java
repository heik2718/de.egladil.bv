//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.payload;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Role;
import de.egladil.bv.aas.validation.ValidRegistrierungsanfrage;
import de.egladil.common.validation.annotations.Passwort;
import de.egladil.common.validation.annotations.StringLatin;

/**
 * Registrierung
 */
@ValidRegistrierungsanfrage
public class Registrierungsanfrage implements Serializable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@StringLatin
	@NotBlank
	@Length(min = 1, max = 100)
	private String loginName;

	@NotBlank
	@Passwort
	private String passwort;

	@NotNull
	@Email
	@Pattern(regexp = ".+@.+\\..+", message = "{org.hibernate.validator.constraints.Email.message}")
	private String email;

	@NotNull
	private Role role;

	@NotNull
	private Anwendung anwendung;

	/**
	 * Erzeugt eine Instanz von Registrierungsanfrage
	 */
	public Registrierungsanfrage() {
	}

	/**
	 *
	 * Erzeugt eine Instanz von Registrierungsanfrage
	 */
	public Registrierungsanfrage(final String loginName, final String passwort, final String email, final Role role,
		final Anwendung anwendung) {
		this.loginName = loginName;
		this.passwort = passwort;
		this.email = email;
		this.role = role;
		this.anwendung = anwendung;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Registrierung [loginName=" + loginName + ", passwort=XXXX, email=" + email + ", role=" + role + ", anwendung="
			+ anwendung + "]";
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((anwendung == null) ? 0 : anwendung.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((loginName == null) ? 0 : loginName.hashCode());
		result = prime * result + ((passwort == null) ? 0 : passwort.hashCode());
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Registrierungsanfrage other = (Registrierungsanfrage) obj;
		if (anwendung != other.anwendung)
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (loginName == null) {
			if (other.loginName != null)
				return false;
		} else if (!loginName.equals(other.loginName))
			return false;
		if (passwort == null) {
			if (other.passwort != null)
				return false;
		} else if (!passwort.equals(other.passwort))
			return false;
		if (role != other.role)
			return false;
		return true;
	}

	/**
	 * Liefert die Membervariable loginName
	 *
	 * @return die Membervariable loginName
	 */
	public String getLoginName() {
		return loginName;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param loginName neuer Wert der Membervariablen loginName
	 */
	public void setLoginName(final String loginName) {
		this.loginName = loginName;
	}

	/**
	 * Liefert die Membervariable passwort
	 *
	 * @return die Membervariable passwort
	 */
	public String getPasswort() {
		return passwort;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param passwort neuer Wert der Membervariablen passwort
	 */
	public void setPasswort(final String passwort) {
		this.passwort = passwort;
	}

	/**
	 * Liefert die Membervariable email
	 *
	 * @return die Membervariable email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param email neuer Wert der Membervariablen email
	 */
	public void setEmail(final String email) {
		this.email = email;
	}

	/**
	 * Liefert die Membervariable role
	 *
	 * @return die Membervariable role
	 */
	public Role getRole() {
		return role;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param role neuer Wert der Membervariablen role
	 */
	public void setRole(final Role role) {
		this.role = role;
	}

	/**
	 * Liefert die Membervariable anwendung
	 *
	 * @return die Membervariable anwendung
	 */
	public Anwendung getAnwendung() {
		return anwendung;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param anwendung neuer Wert der Membervariablen anwendung
	 */
	public void setAnwendung(final Anwendung anwendung) {
		this.anwendung = anwendung;
	}
}
