//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import de.egladil.common.persistence.IDomainObject;

/**
 * Eine Berechtigung wird später zu einer Permission umgewandelt.
 *
 * @author heike
 *
 */
@Entity
@Table(name = "kat_rechte")
public class Berechtigung implements Serializable, IDomainObject {

	private static final long serialVersionUID = 2L;

	@Id
	@Column(name = "ID")
	private Long id;

	@Column(name = "PERMISSION")
	@NotNull
	private String permission;

	@ManyToOne
	@JoinColumn(name = "ROLLE")
	private Rolle rolle;

	/**
	 *
	 */
	public Berechtigung() {
	}

	@Override
	public String toString() {
		return "Berechtigung [permission=" + permission + ", rolle=" + (rolle == null ? "null" : rolle) + "]";
	}

	/**
	 * @return the berechtigung
	 */
	public String getPermission() {
		return permission;
	}

	/**
	 * @param berechtigung the berechtigung to set
	 */
	public void setPermission(final String berechtigung) {
		this.permission = berechtigung;
	}

	/**
	 *
	 * @see de.egladil.common.persistence.IDomainObject#getId()
	 */
	@Override
	public Long getId() {
		return id;
	}

}
