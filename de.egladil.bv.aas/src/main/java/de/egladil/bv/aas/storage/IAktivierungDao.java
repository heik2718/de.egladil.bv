//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.storage;

import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.common.exception.EgladilBVException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.common.persistence.IBaseDao;

/**
 * IRegistrirungDao
 */
public interface IAktivierungDao extends IBaseDao<Aktivierungsdaten> {

	/**
	 * Tja, was wohl.
	 *
	 * @param confirmationCode
	 * @return
	 * @throws EgladilServerException
	 * @throws EgladilStorageException
	 */
	Aktivierungsdaten findByConfirmationCode(String confirmationCode) throws EgladilBVException, EgladilStorageException;

}
