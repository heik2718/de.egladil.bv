//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.domain;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import de.egladil.bv.aas.impl.PeriodChecker;
import de.egladil.common.persistence.IDomainObject;
import de.egladil.common.validation.annotations.UuidString;

/**
 * Kapselt die Attribute, die für die Aktivierung eines Benutzerkontos erforderlich sind.
 *
 * @author heike
 *
 */
@Entity
@Table(name = "aktivierungsdaten")
public class Aktivierungsdaten implements Serializable, IDomainObject {

	/**
	 *
	 */
	private static final long serialVersionUID = 2L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	@Column(name = "CONFIRM_CODE", length = 40)
	@NotNull
	@UuidString
	@Size(min = 1, max = 40)
	private String confirmationCode;

	@Column(name = "CONFIRM_EXPIRETIME")
	private Date expirationTime;

	@Column(name = "CONFIRMED")
	@Convert(converter = BooleanToIntegerConverter.class)
	private boolean confirmed;

	@NotNull
	@ManyToOne(optional = false)
	@JoinColumn(name = "BENUTZER", referencedColumnName = "ID")
	private Benutzerkonto benutzerkonto;

	@Version
	@Column(name = "VERSION")
	private int version;

	/**
	 *
	 */
	public Aktivierungsdaten() {
	}

	@Override
	public String toString() {
		final String expires = expirationTime == null ? "null" : new SimpleDateFormat("dd.MM.yyyy hh:mm:ss").format(expirationTime);
		return "Registrierungsbestaetigung [confirmationCode=" + confirmationCode + ", expirationTime=" + expires
			+ ", benutzerkontoUUID=" + (getBenutzerkonto() == null ? "null" : getBenutzerkonto().getUuid()) + "]";
	}

	/**
	 *
	 * @see de.egladil.common.persistence.IDomainObject#getId()
	 */
	@Override
	public Long getId() {
		return this.id;
	}

	public String getConfirmationCode() {
		return this.confirmationCode;
	}

	public void setConfirmationCode(final String code) {
		this.confirmationCode = code;
	}

	public Date getExpirationTime() {
		return this.expirationTime;
	}

	public void setExpirationTime(final Date date) {
		this.expirationTime = date;
	}

	/**
	 * @return the benutzerkonto
	 */
	public Benutzerkonto getBenutzerkonto() {
		return benutzerkonto;
	}

	/**
	 * @param benutzerkonto the benutzerkonto to set
	 */
	public void setBenutzerkonto(final Benutzerkonto benutzerkonto) {
		this.benutzerkonto = benutzerkonto;
	}

	/**
	 * Zwischen jetzt und expiration gibt es eine Kulanzzeitspanne von 60s.
	 *
	 * @param now
	 * @return
	 */
	public boolean isExpired(final Date now) {
		final boolean result = !new PeriodChecker().isPeriodLessEqualExpectedPeriod(expirationTime, now, 60000);
		return result;
	}

	/**
	 * Liefert die Membervariable confirmed
	 *
	 * @return die Membervariable confirmed
	 */
	public boolean isConfirmed() {
		return confirmed;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param confirmed neuer Wert der Membervariablen confirmed
	 */
	public void setConfirmed(final boolean confirmed) {
		this.confirmed = confirmed;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((confirmationCode == null) ? 0 : confirmationCode.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Aktivierungsdaten other = (Aktivierungsdaten) obj;
		if (confirmationCode == null) {
			if (other.confirmationCode != null)
				return false;
		} else if (!confirmationCode.equals(other.confirmationCode))
			return false;
		return true;
	}

}
