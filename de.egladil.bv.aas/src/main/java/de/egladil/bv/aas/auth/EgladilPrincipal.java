//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.auth;

import java.io.Serializable;
import java.security.Principal;
import java.util.Objects;

/**
 * Implementierung vin Principal.
 */
public class EgladilPrincipal implements Principal, Serializable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	private String name;

	/**
	 * Erzeugt eine Instanz von EgladilPrincipal
	 */
	public EgladilPrincipal(String name) {
		this.name = name;
	}

	/**
	 * @see java.security.Principal#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		final EgladilPrincipal principal = (EgladilPrincipal) o;
		return Objects.equals(this.name, principal.name);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(name);
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EgladilPrincipal [name=" + name + "]";
	}
}
