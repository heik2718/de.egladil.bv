//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.impl;

import java.time.temporal.ChronoUnit;
import java.util.Base64;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.shiro.crypto.hash.Hash;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.bv.aas.IRegistrierungService;
import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.domain.LoginSecrets;
import de.egladil.bv.aas.domain.Rolle;
import de.egladil.bv.aas.domain.Salz;
import de.egladil.bv.aas.domain.UniqueIdentifier;
import de.egladil.bv.aas.payload.Registrierungsanfrage;
import de.egladil.bv.aas.storage.IAktivierungDao;
import de.egladil.bv.aas.storage.IBenutzerDao;
import de.egladil.bv.aas.storage.IRollenDao;
import de.egladil.common.config.TimeUtils;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.common.persistence.PersistenceExceptionMapper;
import de.egladil.common.persistence.utils.PrettyStringUtils;
import de.egladil.common.validation.EgladilValidationDelegate;
import de.egladil.crypto.provider.CryptoConfigurationKeys;
import de.egladil.crypto.provider.IEgladilCryptoUtils;

/**
 * RegistrierungServiceImpl
 */
@Singleton
public class RegistrierungServiceImpl implements IRegistrierungService {

	private static final Logger LOG = LoggerFactory.getLogger(RegistrierungServiceImpl.class);

	private int registrationKeyExpireHours;

	private IEgladilCryptoUtils cryptoUtils;

	private IAktivierungDao registrierungDao;

	private IRollenDao rollenDao;

	private EgladilValidationDelegate validationDelegate;

	private IBenutzerDao benutzerDao;

	/**
	 * RegistrierungServiceImpl
	 */
	public RegistrierungServiceImpl() {
	}

	/**
	 * Erzeugt eine Instanz von RegistrierungServiceImpl
	 */
	@Inject
	public RegistrierungServiceImpl(final IAktivierungDao regDao, final IBenutzerDao benutzerDao, final IRollenDao rollenDao,
		final IEgladilCryptoUtils cryptoUtils) {
		this.benutzerDao = benutzerDao;
		this.registrierungDao = regDao;
		this.rollenDao = rollenDao;
		this.cryptoUtils = cryptoUtils;
		validationDelegate = new EgladilValidationDelegate();
		init();
	}

	@PostConstruct
	public void init() {
		registrationKeyExpireHours = cryptoUtils.getConfig()
			.getIntegerProperty(CryptoConfigurationKeys.REGISTRATION_KEY_EXPIRE_PERIOD);
	}

	@Override
	public Aktivierungsdaten register(@NotNull @Valid final Registrierungsanfrage anfrage) throws ConstraintViolationException,
		IllegalArgumentException, EgladilDuplicateEntryException, EgladilConcurrentModificationException, EgladilStorageException {
		validationDelegate.check(anfrage);

		try {
			final Rolle rolle = rollenDao.findByRole(anfrage.getRole());
			final Benutzerkonto benutzer = createBenutzerkonto(anfrage.getLoginName(), anfrage.getEmail(), anfrage.getAnwendung(),
				rolle);

			final LoginSecrets loginSecrets = createLoginSecrets(anfrage.getPasswort());
			benutzer.setLoginSecrets(loginSecrets);

			final Aktivierungsdaten registrierungsbestaetigung = createRegistrierungsbestaetigung();
			benutzer.addAktivierungsdaten(registrierungsbestaetigung);

			try {
				final Benutzerkonto persisted = benutzerDao.persist(benutzer);
				if (persisted != null) {
					LOG.debug("Registrierungsbestaetigung erzeugt: {}", persisted);
				}

				return registrierungsbestaetigung;
			} catch (final PersistenceException e) {
				final Optional<EgladilDuplicateEntryException> integrityException = PersistenceExceptionMapper
					.toEgladilDuplicateEntryException(e);
				if (integrityException.isPresent()) {
					throw integrityException.get();
				}
				final Optional<EgladilConcurrentModificationException> optEx = PersistenceExceptionMapper
					.toEgladilConcurrentModificationException(e);
				if (optEx.isPresent()) {
					final String msg = "Jemand anders hat die Benutzerdaten inzwischen geändert";
					throw new EgladilConcurrentModificationException(msg);
				}
				throw new EgladilStorageException("Fehler in einer Datenbanktransaktion", e);
			}
		} catch (final PersistenceException e) {
			final Optional<EgladilDuplicateEntryException> integrityException = PersistenceExceptionMapper
				.toEgladilDuplicateEntryException(e);
			if (integrityException.isPresent()) {
				throw integrityException.get();
			}
			final String msg = "Persistierungsfehler beim Registrieren eines Benutzers: " + anfrage.toString();
			throw new EgladilStorageException(msg, e);
		} finally {
			PrettyStringUtils.erase(anfrage.getPasswort());
		}
	}

	/**
	 * @see de.egladil.bv.aas.IRegistrierungService#findByConfirmationCode(de.egladil.bv.aas.domain.UniqueIdentifier)
	 */
	@Override
	public Aktivierungsdaten findByConfirmationCode(final UniqueIdentifier confirmationCode) {
		validationDelegate.check(confirmationCode);
		final Aktivierungsdaten registrierungsbestaetigung = registrierungDao.findByConfirmationCode(confirmationCode.getUuid());
		return registrierungsbestaetigung;
	}

	/**
	 * @see de.egladil.bv.aas.IRegistrierungService#activateBenutzer(de.egladil.bv.aas.domain.Aktivierungsdaten)
	 */
	@Override
	public Benutzerkonto activateBenutzer(final Aktivierungsdaten aktivierungsdaten)
		throws IllegalArgumentException, ConstraintViolationException, EgladilDuplicateEntryException,
		EgladilConcurrentModificationException, EgladilStorageException {
		validationDelegate.check(aktivierungsdaten);
		final Benutzerkonto benutzerkonto = benutzerDao.findById(Benutzerkonto.class, aktivierungsdaten.getBenutzerkonto().getId())
			.orElse(null);

		final boolean activationChanged = !benutzerkonto.isAktiviert();
		benutzerkonto.setAktiviert(true);
		aktivierungsdaten.setConfirmed(true);

		try {
			final Benutzerkonto result = benutzerDao.persist(benutzerkonto);
			if (activationChanged) {
				// FIXME Protokollierung einbauen!!!!
				LOG.info("Benutzerkonto aktiviert: {}", benutzerkonto);
			}
			return result;
		} catch (final PersistenceException e) {
			final Optional<EgladilDuplicateEntryException> integrityException = PersistenceExceptionMapper
				.toEgladilDuplicateEntryException(e);
			if (integrityException.isPresent()) {
				throw integrityException.get();
			}
			final Optional<EgladilConcurrentModificationException> optEx = PersistenceExceptionMapper
				.toEgladilConcurrentModificationException(e);
			if (optEx.isPresent()) {
				final String msg = "Jemand anders hat die Benutzerdaten inzwischen geändert";
				throw new EgladilConcurrentModificationException(msg);
			}
			throw new EgladilStorageException("Fehler in einer Datenbanktransaktion", e);
		}
	}

	/**
	 *
	 * @return
	 */
	private Aktivierungsdaten createRegistrierungsbestaetigung() {
		final Aktivierungsdaten registrierungsbestaetigung = new Aktivierungsdaten();
		registrierungsbestaetigung.setConfirmationCode(UUID.randomUUID().toString());
		final Date jetzt = new Date();
		final Date expireTime = TimeUtils.calculateExpireTime(jetzt, registrationKeyExpireHours, ChronoUnit.HOURS);
		registrierungsbestaetigung.setExpirationTime(expireTime);
		return registrierungsbestaetigung;
	}

	/**
	 *
	 * @param loginName
	 * @param email
	 * @param anwendung
	 * @param rolle
	 * @return
	 */
	private Benutzerkonto createBenutzerkonto(final String loginName, final String email, final Anwendung anwendung,
		final Rolle rolle) {
		final Benutzerkonto benutzer = new Benutzerkonto();
		benutzer.setAnwendung(anwendung);
		benutzer.addRolle(rolle);
		benutzer.setEmail(email);
		benutzer.setLoginName(loginName);
		benutzer.setUuid(UUID.randomUUID().toString());
		return benutzer;
	}

	/**
	 * Erzeugt eine Instanz von LoginSecrets mit fem gegebenen Passwort.
	 *
	 * @param passwort
	 * @return
	 */
	private LoginSecrets createLoginSecrets(final String passwort) {
		final Hash hash = cryptoUtils.hashPassword(passwort.toCharArray());
		final LoginSecrets loginSecrets = new LoginSecrets();
		loginSecrets.setPasswordhash(Base64.getEncoder().encodeToString(hash.getBytes()));

		final Salz salz = new Salz();
		salz.setAlgorithmName(hash.getAlgorithmName());
		salz.setIterations(hash.getIterations());
		salz.setWert(hash.getSalt().toBase64());
		loginSecrets.setSalz(salz);
		return loginSecrets;
	}

	/**
	 * Zu Testzwecken kann man hier eine andere Implementierung setzen.
	 *
	 * @param validationDelegate neuer Wert der Membervariablen validationDelegate
	 */
	protected void setValidationDelegate(final EgladilValidationDelegate validationDelegate) {
		this.validationDelegate = validationDelegate;
	}
}
