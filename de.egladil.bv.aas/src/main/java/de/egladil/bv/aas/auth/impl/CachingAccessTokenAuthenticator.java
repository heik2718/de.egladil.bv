//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.auth.impl;

import java.security.Principal;
import java.util.Optional;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.joda.time.DateTime;

import de.egladil.bv.aas.auth.AccessToken;
import de.egladil.bv.aas.auth.EgladilPrincipal;
import io.dropwizard.auth.AuthenticationException;
import io.dropwizard.auth.Authenticator;

/**
 * Diese Klasse enthält die Magie des Authentisierens von Resourcen, die mit @Auth annotiert sind. Sie wird beim
 * OAuthCredentialAuthFilter registriert. Dieser ruft die Methode authenticate auf.<br>
 * <br>
 * Der AccessTokenCache wird beim Login gefüllt. Der Authenticator sucht lediglich nach im Cache vorgandenen Werten.
 * Wenn kein Wert mehr vorhanden ist, hat entweder kein Login stattgefunden oder es gab ein Timeout. Die Länge des
 * Timeouts wird beim Erzeugen des LoadingCaches aus der Crypto-Konfiguration gesetzt.
 */
@Singleton
public class CachingAccessTokenAuthenticator implements Authenticator<String, Principal> {

	private AccessTokenCache accessTokenCache;

	/**
	 * CachingAccessTokenAuthenticator
	 */
	public CachingAccessTokenAuthenticator() {
	}

	/**
	 * Erzeugt eine Instanz von CachingAccessTokenAuthenticator
	 */
	@Inject
	public CachingAccessTokenAuthenticator(final AccessTokenCache accessTokenCache) {
		this.accessTokenCache = accessTokenCache;
	}

	/**
	 * Die Methode authentisiert das AccessToken (also die SessionID). Der Parameter wird durch dropwizard aus dem
	 * Authorization-Header geholt und an diese Methode als Parameter übergeben. Bei erfolgreicher Authentisierung wird
	 * das zum AccessToken gehörende Principal (ein EgladilPrincipal) zurückgegeben. Es enthält als primary principal
	 * die UUID des Benutzerkontos. Falls es kein passendes AccessToken gibt, wird ein leeres Optional zurückgegeben.
	 * Das ist ein Hinweis auf fehlende Authentisierung.<br>
	 * <br>
	 * Aktualisiert bei jedem Erfolg das gecachete AccessToken.
	 *
	 * @see io.dropwizard.auth.Authenticator#authenticate(java.lang.Object)
	 *
	 * @param accessTokenId String - darf nicht null sein!
	 */
	@Override
	public Optional<Principal> authenticate(final String accessTokenId) throws AuthenticationException, NullPointerException {
		final AccessToken accessToken = accessTokenCache.findAccessTokenIfPresent(accessTokenId);
		if (accessToken != null) {
			// Bei jedem authentisierten Zugriff auf die API refreshen, damit der Timeout nicht zuschlägt
			accessToken.setLastAccessUTC(new DateTime());
			accessTokenCache.refresh(accessToken);
			final EgladilPrincipal principal = new EgladilPrincipal(accessToken.getPrimaryPrincipal());
			return Optional.<Principal> of(principal);
		}
		return Optional.empty();
	}
}
