//=====================================================
// Projekt: de.egladil.mkm.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.auth;

import javax.servlet.ServletRequest;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.eclipse.jetty.http.HttpField;
import org.eclipse.jetty.http.HttpFields;
import org.eclipse.jetty.server.Request;

/**
 * Methoden zum Umgang mit ServletRequests.
 */
public class ServletUtils {

	/**
	 * Erstellt einen Dump des Requests, wenn möglich.
	 *
	 * @param request
	 * @return String
	 */
	public String dumpRequest(ServletRequest request) {
		String dump = "";
		if (request instanceof Request) {
			Request req = (Request) request;
			StringBuffer sb = new StringBuffer();
			sb.append(getPath(req));
			sb.append("\n   ");
			try {
				Object fields = FieldUtils.readField(request, "_fields", true);
				if (fields instanceof HttpFields) {
					HttpFields httpFields = (HttpFields) fields;
					for (HttpField f : httpFields) {
						sb.append(f);
						sb.append("\n   ");
					}
					dump = sb.toString();
				}
			} catch (IllegalAccessException e) {
				dump = "kein dump moeglich: " + e.getMessage() + "\n";
			}
		} else {
			dump = "war leider kein " + Request.class.getName() + "\n";
		}
		return dump;
	}

	private String getPath(Request req){
		return req.getMethod() + " " + req.getRequestURI();
	}
}
