//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author heike
 *
 */
@Entity(name = "Salz")
@Table(name = "slz")
public class Salz implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 2L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	@Column(name = "ALGORITHM", length = 10)
	@NotBlank
	private String algorithmName;

	@Column(name = "ROUNDS")
	private int iterations;

	@Column(name = "WERT", length = 1000)
	private String wert;

	@Version
	@Column(name = "VERSION")
	private int version;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(final Long id) {
		this.id = id;
	}

	/**
	 * @return the algorithmName
	 */
	public String getAlgorithmName() {
		return algorithmName;
	}

	/**
	 * @param algorithmName the algorithmName to set
	 */
	public void setAlgorithmName(final String algorithmName) {
		this.algorithmName = algorithmName;
	}





	/**
	 * @return the wert
	 */
	public String getWert() {
		return wert;
	}

	/**
	 * @param wert the wert to set
	 */
	public void setWert(final String wert) {
		this.wert = wert;
	}

	/**
	 * @return the version
	 */
	public int getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(final int version) {
		this.version = version;
	}

	/**
	 * @return the iterations
	 */
	public int getIterations() {
		return iterations;
	}

	/**
	 * @param iterations the iterations to set
	 */
	public void setIterations(final int iterations) {
		this.iterations = iterations;
	}

}
