//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.domain;

/**
 * ISperrbar
 */
public interface ISperrbar {

	boolean isGesperrt();
}
