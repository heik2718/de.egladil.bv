//=====================================================
// Projekt: dropwizard-oauth2-provider
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.auth;

import java.util.Optional;

/**
 * Registry für die AccessToken.
 */
public interface IAccessTokenDAO {

	/**
	 * Erzeugt ein AccessToken für den gegebenen User. Wird nach erfolgreicher Authentisierung beim Login aufgerufen.
	 *
	 * @param authentikationInfo AuthenticationInfo - darf nicht null sein.
	 * @return AccessToken
	 */
	AccessToken generateNewAccessToken(final AuthenticationInfo authentikationInfo) throws NullPointerException;

	/**
	 * Aktualsiert das AccessToken.<br>
	 * <br>
	 * Wird durch den Authenticator bei jedem Zugriff über eine mit Auth annotierte Methode aufgerufen.<br>
	 * <br>
	 * Die Methode ich nicht void, damit man sie Mocken kann.
	 *
	 * @param accessToken AccessToken - darf nicht null sein.
	 * @return String die accessTokenId
	 * @throws NullPointerException weitergereicht von com.google.common.cache falls accessToken null.
	 */
	String refresh(AccessToken accessToken) throws NullPointerException;

	/**
	 * Aktualisiert das csrf-Token.<br>
	 * <br>
	 * Wird durch den CsrfFilter aufgerufen.<br>
	 * <br>
	 * Die Methode ich nicht void, damit man sie Mocken kann.
	 *
	 * @param csrfToken
	 * @return String
	 * @throws NullPointerException weitergereicht von com.google.common.cache falls csrfToken null.
	 */
	String refreshCsrfToken(String csrfToken) throws NullPointerException;

	/**
	 * Kanonische find-Methode mit der accessTokenId.
	 *
	 * @param accessTokenId String - darf nicht null sein.
	 * @return Optional
	 * @throws NullPointerException weitergereicht von com.google.common.cache falls accessTokenId null.
	 */
	Optional<AccessToken> findAccessTokenById(final String accessTokenId) throws NullPointerException;

	/**
	 * Sucht das AccessToken mit der gegebenen Benutzer-UUID.
	 *
	 * @param benutzerUuid
	 * @return Optional
	 * @throws NullPointerException
	 */
	Optional<AccessToken> findAccessTokenByPrimaryPrincipal(final String benutzerUuid) throws NullPointerException;

	/**
	 * Prüft, ob das gegebene csrfToken bekannt ist.<br>
	 * Wird für temporäre token vor einer Authentisierung benötigt, da es auch auf nicht authentisierte Anwendungen
	 * CSRF-Atacken geben kann.
	 *
	 * @param csrfToken String - darf nicht null sein.
	 * @return boolean
	 * @throws NullPointerException weitergereicht von com.goolgle.common.cache - falls tokenKey null.
	 *
	 */
	boolean isTemporaryCsrfToken(String csrfToken) throws NullPointerException;

	/**
	 * Invalidiert das AccessToken mit der gegeben id.<br>
	 * <br>
	 * Die Methode ich nicht void, damit man sie Mocken kann.
	 *
	 * @param accessTokenId String - darf nicht null sein
	 * @return TODO
	 * @throws NullPointerException weitergereicht von com.goolgle.common.cache - falls accessTokenId null.
	 */
	String invalidateAccessToken(String accessTokenId) throws NullPointerException;

	/**
	 * Persistiert ein temporäres CSRF-Token.<br>
	 * <br>
	 * Die Methode ich nicht void, damit man sie Mocken kann.
	 *
	 * @param csrfToken String - darf nicht null sein
	 * @return TODO
	 * @throws NullPointerException weitergereicht von com.goolgle.common.cache - falls csrfToken null.
	 */
	String registerTemporaryCsrfToken(String csrfToken) throws NullPointerException;

	/**
	 * Verwirft das gegebene temporäre CSRF-Token. Muss nach erfolgreicher Authentisierung aufgerufen werden.<br>
	 * <br>
	 * Die Methode ich nicht void, damit man sie Mocken kann.
	 *
	 * @param csrfToken String - darf nicht null sein
	 * @return TODO
	 * @throws NullPointerException weitergereicht von com.goolgle.common.cache - falls csrfToken null.
	 */
	String invalidateTemporaryCsrfToken(String csrfToken) throws NullPointerException;
}