//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas;

import java.util.Optional;

import org.hibernate.validator.constraints.Email;

import de.egladil.bv.aas.auth.AuthenticationInfo;
import de.egladil.bv.aas.auth.UsernamePasswordToken;
import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.common.exception.DisabledAccountException;
import de.egladil.common.exception.EgladilAuthenticationException;
import de.egladil.common.exception.EgladilBVException;
import de.egladil.common.exception.IncorrectCredentialsException;
import de.egladil.common.exception.UnknownAccountException;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.common.validation.annotations.UuidString;
import de.egladil.crypto.provider.EgladilEncryptionException;

/**
 * AuthenticationService
 */
public interface IAuthenticationService {

	/**
	 * Das Passwort des Benutzers mit der gegebenen benutzerUuid wird geändert.<br>
	 * <br>
	 * <b>Achtung:</b> Diese Aktion setzt voraus, dass der Benutzer bereits authentisiert ist!
	 *
	 * @param anwendung
	 * @param benutzerUuid
	 * @param pwdNeu
	 * @return
	 */
	Optional<Benutzerkonto> changePasswort(Anwendung anwendung, String benutzerUuid, char[] pwdNeu)
		throws IllegalArgumentException, UnknownAccountException, EgladilStorageException, EgladilBVException,
		EgladilDuplicateEntryException, EgladilConcurrentModificationException;

	/**
	 * Authentisiert einen Benutzer mit einem temporären Passwort.
	 *
	 * @param anwendung
	 * @param email
	 * @param password
	 *
	 * @return AuthenticationInfo das Benutzerkonto.
	 * @throws IllegalArgumentException
	 * @throws UnknownAccountException
	 * @throws IncorrectCredentialsException
	 */
	AuthenticationInfo authenticateWithTemporaryPasswort(Anwendung anwendung, @Email String email, @UuidString String password)
		throws IllegalArgumentException, EgladilAuthenticationException, DisabledAccountException, EgladilStorageException;

	/**
	 * Authentifiziert einen Benutzer mit den gegebenen Credentials zur gegebenen Anwendung. Gibt einen Benutzer zurück,
	 * falls korrekt authentisiert. PersistenceExceptions werden geloggt und nicht weitergereicht.
	 *
	 * @param usernamePasswordToken
	 * @param anwendung
	 * @return AuthenticationInfo
	 */
	AuthenticationInfo authenticate(UsernamePasswordToken usernamePasswordToken, Anwendung anwendung)
		throws IllegalArgumentException, EgladilAuthenticationException, DisabledAccountException, EgladilStorageException;

	/**
	 * Erzeugt ein Token.
	 *
	 * @return String
	 */
	String createCsrfToken() throws EgladilEncryptionException;
}
