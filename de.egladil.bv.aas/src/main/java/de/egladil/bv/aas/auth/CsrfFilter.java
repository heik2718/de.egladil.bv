//=====================================================
// Projekt: de.egladil.mkm.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.auth;

import java.io.IOException;
import java.util.Enumeration;
import java.util.List;
import java.util.Optional;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Priorities;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import com.google.common.base.Splitter;

import de.egladil.bv.aas.auth.impl.CsrfFilterUtils;
import de.egladil.common.config.IEgladilConfiguration;
import de.egladil.common.persistence.utils.PrettyStringUtils;

/**
 * Dieser Filter prüft das temporäre CSRF-Token oder gleicht das AccessToken mit dem CSRF-Token ab.
 *
 * @author heikew
 *
 */
@Priority(Priorities.HEADER_DECORATOR)
@Singleton
public class CsrfFilter implements Filter {

	private static final Logger LOG = LoggerFactory.getLogger(CsrfFilter.class);

	private static final Splitter ORIGIN_SPLITTER = Splitter.on("/").trimResults();

	private static final Splitter COMMA_SPLITTER = Splitter.on(",").trimResults();

	private static final String KEY_TARGET_ORIGIN = "csrf.targetOrigin";

	private static final String KEY_ALLOW_ORIGIN = "cors.allowOrigin";

	private static final String KEY_BLOCK_ON_MISSING_ORIGIN_REFERER = "csrf.blockOnMissingOriginReferer";

	private static final String KEY_OPEN_DATA_URLS = "csrf.ignorableUrls";

	private final IAccessTokenDAO accessTokenDAO;

	private final String allowOrigin;

	private final String targetOrigin;

	private final boolean blockOnMissingOriginReferer;

	private final List<String> openDataUrls;

	private final String USER_KEY = "username";

	private final String CSRF_TOKEN = "csrf";

	/**
	 * @param accessTokenDAO
	 */
	@Inject
	public CsrfFilter(final IAccessTokenDAO accessTokenDAO, final IEgladilConfiguration corsConfiguration) {
		this.accessTokenDAO = accessTokenDAO;
		allowOrigin = corsConfiguration.getProperty(KEY_ALLOW_ORIGIN);
		blockOnMissingOriginReferer = Boolean.valueOf(corsConfiguration.getProperty(KEY_BLOCK_ON_MISSING_ORIGIN_REFERER));
		targetOrigin = corsConfiguration.getProperty(KEY_TARGET_ORIGIN);

		final String urls = corsConfiguration.getProperty(KEY_OPEN_DATA_URLS);
		openDataUrls = COMMA_SPLITTER.splitToList(urls);
		LOG.info(toLog());
	}

	@Override
	public void init(final FilterConfig filterConfig) throws ServletException {
		// keine Initialisierung erforderlich
	}

	@Override
	public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
		throws IOException, ServletException {

		final HttpServletRequest req = (HttpServletRequest) request;

		final String pathInfo = req.getPathInfo();
		final String method = req.getMethod();
		final String clientIP = getClientIp(req);

		final boolean isOpendataRequest = CsrfFilterUtils.canPassWithoutCsrfToken(openDataUrls, pathInfo, method);
		if (isOpendataRequest) {
			final String origin = extractOriginOrReferer(req.getHeader("Origin"));
			final String referer = extractOriginOrReferer(req.getHeader("Referer"));
			unregisterUsername();
			unregisterCsrfToken();
			LOG.info("{} - {} , Origin={}, Referrer={}", clientIP, pathInfo, origin, referer);
		} else {
			validateOriginAndRefererHeader(response, req);
			final HttpServletResponse res = (HttpServletResponse) response;

			final String bearerHeader = req.getHeader(IAuthConstants.BEARER_KEY);
			final String xsrfToken = req.getHeader(IAuthConstants.XSRF_HEADER_KEY);

			if (StringUtils.isNotBlank(bearerHeader) && StringUtils.isNotBlank(xsrfToken)) {
				registerCsrfToken(xsrfToken);
				registerUsername(bearerHeader);

				final String bearer = new AccessTokenUtils().extractAccessTokenId(bearerHeader);
				final Optional<AccessToken> accessToken = accessTokenDAO.findAccessTokenById(bearer);

				if (!accessToken.isPresent()) {
					final String details = "accessToken erloschen";
					logTimeoutAndSend(request, res, details);
					return;
				} else {
					final String expectedCsrfToken = accessToken.get().getCsrfToken();
					if (!xsrfToken.equals(expectedCsrfToken)) {
						final String details = "accessToken.CsrfToken ungleich xsrfToken";
						logErrorAndSend(request, res, details);
						return;
					}
				}
			}
			if (!isOpendataRequest) {
				// openDataRequest wurde schon geloggt
				LOG.info("{} - {} {}", clientIP, method, pathInfo);
			}
		}

		chain.doFilter(request, response);
	}

	private String getClientIp(final HttpServletRequest request) {
		String remoteAddr = "";
		if (request != null) {
			remoteAddr = request.getHeader("X-FORWARDED-FOR");
			if (remoteAddr == null || "".equals(remoteAddr)) {
				remoteAddr = request.getRemoteAddr();
			}
		}
		if (remoteAddr.length() > 3) {
			remoteAddr = new String(remoteAddr.substring(0, remoteAddr.length() - 3));
		}
		return remoteAddr;
	}

	/**
	 * Validiert die Header-Parameter 'Origin' und 'Referer'.
	 *
	 * @param response
	 * @param req
	 * @throws IOException
	 */
	private void validateOriginAndRefererHeader(final ServletResponse response, final HttpServletRequest req) throws IOException {
		final String origin = req.getHeader("Origin");
		final String referer = req.getHeader("Referer");
		LOG.debug("Origin = [{}], Referer = [{]}", origin, referer);

		if (StringUtils.isBlank(origin) && StringUtils.isBlank(referer)) {
			final String details = "Header Origin UND Referer fehlen";
			if (blockOnMissingOriginReferer) {
				logErrorAndSend(req, (HttpServletResponse) response, details);
			}
		}

		if (!StringUtils.isBlank(origin)) {
			checkHeaderTarget(origin, req, response);
		}
		if (!StringUtils.isBlank(referer)) {
			checkHeaderTarget(referer, req, response);
		}
	}

	private void checkHeaderTarget(final String headerValue, final ServletRequest req, final ServletResponse response)
		throws IOException {
		final String extractedValue = extractOriginOrReferer(headerValue);
		if (extractedValue == null) {
			return;
		}

		if (!targetOrigin.equals(extractedValue)) {
			final String details = "targetOrigin != extractedOrigin: [targetOrigin=" + targetOrigin + ", extractedOriginOrReferer="
				+ extractedValue + "]";
			logErrorAndSend(req, (HttpServletResponse) response, details);
		}
	}

	/**
	 * TODO
	 *
	 * @param headerValue
	 * @return
	 */

	private String extractOriginOrReferer(final String headerValue) {
		if (StringUtils.isBlank(headerValue)) {
			return null;
		}
		final String value = headerValue.replaceAll("http://", "").replaceAll("https://", "");
		final List<String> token = ORIGIN_SPLITTER.splitToList(value);
		final String extractedOrigin = token == null || token.isEmpty() ? value : token.get(0);
		return extractedOrigin;
	}

	/**
	 * Der Authentisierungsfehler wird geloggt und ein entsprechender Response erzeugt.
	 *
	 * @param request
	 * @param res
	 * @throws IOException
	 */
	private void logErrorAndSend(final ServletRequest request, final HttpServletResponse res, final String details)
		throws IOException {
		final String dump = getRequesInfos(request);
		LOG.warn("Possible CSRF-Attack: {} - {}", details, dump);
		addCORSHeader(res);
		res.setContentType("application/json");
		res.sendError(Response.Status.UNAUTHORIZED.getStatusCode());
	}

	/**
	 * Wenn das XSRF-Token oder das AccessToken nicht null sind, aber nicht mehr bekannt, dann war der Client zu lange
	 * inaktiv.
	 *
	 * @param request
	 * @param res
	 * @throws IOException
	 */
	private void logTimeoutAndSend(final ServletRequest request, final HttpServletResponse res, final String details)
		throws IOException {
		LOG.debug("Session Timeout: {}", details);
		addCORSHeader(res);
		res.setContentType("application/json");
		res.sendError(Response.Status.REQUEST_TIMEOUT.getStatusCode());
	}

	/**
	 * TODO
	 *
	 * @param request
	 * @return
	 */

	private String getRequesInfos(final ServletRequest request) {
		final HttpServletRequest httpRequest = (HttpServletRequest) request;
		final Enumeration<String> headerNames = httpRequest.getHeaderNames();
		final StringBuffer sb = new StringBuffer();
		sb.append(" <--- Request Headers --- ");
		while (headerNames.hasMoreElements()) {
			final String headerName = headerNames.nextElement();
			sb.append(headerName);
			sb.append(":");
			final Enumeration<String> headerValues = httpRequest.getHeaders(headerName);
			if (headerValues.hasMoreElements()) {
				sb.append(headerValues.nextElement());
				sb.append(", ");
			}
			sb.append(" -- ");
		}
		sb.append(" Headers Request ---> ");
		final String dump = sb.toString();
		return dump;
	}

	/**
	 * @param res
	 */
	private void addCORSHeader(final HttpServletResponse res) {
		res.setHeader("Access-Control-Allow-Origin", allowOrigin);
		res.setHeader("Access-Control-Allow-Credentials", "true");
	}

	private String toLog() {
		return "CsrfFilter [allowOrigin=" + allowOrigin + ", targetOrigin=" + targetOrigin + ", blockOnMissingOriginReferer="
			+ blockOnMissingOriginReferer + ", openDataUrls=" + PrettyStringUtils.collectionToDefaultString(openDataUrls) + "]";
	}

	@Override
	public void destroy() {
		// kein Destroyment nötig
	}

	/**
	 * Register the user in the MDC under USER_KEY.
	 *
	 * @param bearer String the AuthenticationHeader
	 * @return true id the user can be successfully registered
	 */
	private boolean registerUsername(final String bearer) {
		if (bearer != null && bearer.trim().length() > 0) {
			final String plainBearer = bearer.replace("Bearer ", "");
			final Optional<AccessToken> optToken = accessTokenDAO.findAccessTokenById(plainBearer);
			if (optToken.isPresent()) {
				final AccessToken accessToken = optToken.get();
				final String userName = accessToken.getPrimaryPrincipal();
				if (userName != null) {
					MDC.put(USER_KEY, userName.substring(0, 8));
				} else {
					MDC.put(USER_KEY, "");
				}
				return true;
			}
		}
		return false;
	}

	/**
	 * Register the user in the MDC under USER_KEY.
	 *
	 * @param username
	 * @return true id the user can be successfully registered
	 */
	private boolean registerCsrfToken(final String csrfToken) {
		if (csrfToken != null && csrfToken.trim().length() > 0) {
			String token = csrfToken.replace("Bearer ", "");
			token = token.substring(0, 8);
			MDC.put(CSRF_TOKEN, token);
			return true;
		}
		return false;
	}

	/**
	 * Unregister the user in the MDC under USER_KEY.
	 */
	private void unregisterUsername() {
		MDC.put(USER_KEY, "");
	}

	/**
	 * Unregister the csrf-Token in the MDC under USER_KEY.
	 */
	private void unregisterCsrfToken() {
		MDC.put(CSRF_TOKEN, "");
	}
}
