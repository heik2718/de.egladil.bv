//=====================================================
// Projekt: de.egladil.mkm.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.domain;

/**
 * MessageEntity ist ein Objekt, mit dem eine unverfängliche Meldung im Response zurückgegeben werden kann.
 */
public class MessageEntity {

	private String message;

	/**
	 * Erzeugt eine Instanz von MessageEntity
	 */
	public MessageEntity(String message) {
		this.message = message;
	}

	/**
	* Liefert die Membervariable message
	* @return die Membervariable  message
	*/
	public String getMessage() {
		return message;
	}

}
