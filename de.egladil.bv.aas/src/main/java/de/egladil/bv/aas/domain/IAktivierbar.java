//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.domain;

/**
 * @author heikew
 *
 */
public interface IAktivierbar {

	boolean isAktiviert();


}
