//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.storage.impl;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.storage.BVPersistenceUnit;
import de.egladil.bv.aas.storage.IBenutzerDao;
import de.egladil.common.persistence.BaseDaoImpl;
import de.egladil.common.persistence.EgladilStorageException;

/**
 * @author heike
 *
 */
@Singleton
@BVPersistenceUnit
public class BenutzerDaoImpl extends BaseDaoImpl<Benutzerkonto> implements IBenutzerDao, Serializable {

	private static final long serialVersionUID = 4L;

	/**
	 * Erzeugt eine Instanz von BenutzerDaoImpl
	 */
	@Inject
	public BenutzerDaoImpl(final Provider<EntityManager> emp) {
		super(emp);
	}

	@Override
	public Benutzerkonto findBenutzerByLoginName(final String loginName, final Anwendung anwendung) throws EgladilStorageException {
		try {
			final TypedQuery<Benutzerkonto> query = getEntityManager().createQuery(
				"from Benutzerkonto b where lower(b.loginName) = :loginName and b.anwendung = :anwendung", Benutzerkonto.class);
			query.setParameter("loginName", loginName.toLowerCase());
			query.setParameter("anwendung", anwendung);

			final List<Benutzerkonto> result = query.getResultList();
			if (result.isEmpty()) {
				return null;
			}
			if (result.size() > 1) {
				throw new EgladilStorageException("Benutzer mit Loginname [" + loginName + "] und Mandant [" + anwendung
					+ "] existiert mehr als einmal - kann eigentlich nicht sein");
			}
			return result.get(0);
		} catch (final Exception e) {
			throw new EgladilStorageException("Unerwartete Exception beim Suchen eines Benutzerkontos", e);
		}
	}

	@Override
	public Benutzerkonto findBenutzerByEmail(final String email, final Anwendung anwendung) throws EgladilStorageException {
		try {
			final TypedQuery<Benutzerkonto> query = getEntityManager().createQuery(
				"from Benutzerkonto b where lower(b.email) = :email and b.anwendung = :anwendung", Benutzerkonto.class);
			query.setParameter("email", email.toLowerCase());
			query.setParameter("anwendung", anwendung);

			final List<Benutzerkonto> result = query.getResultList();
			if (result.isEmpty()) {
				return null;
			}
			if (result.size() > 1) {
				throw new EgladilStorageException("Benutzer mit Email [" + email + "] und Mandant [" + anwendung
					+ "] existiert mehr als einmal - kann eigentlich nicht sein");
			}
			return result.get(0);
		} catch (final Exception e) {
			throw new EgladilStorageException("Unerwartete Exception beim Suchen eines Benutzerkontos: " + e.getMessage(), e);
		}
	}

	@Override
	public List<Benutzerkonto> findByEmailLike(final String email, final Anwendung anwendung)
		throws PersistenceException, IllegalArgumentException {

		if (StringUtils.isBlank(email)) {
			throw new IllegalArgumentException("email blank");
		}
		if (anwendung == null) {
			throw new IllegalArgumentException("anwendung null");
		}

		try {
			final TypedQuery<Benutzerkonto> query = getEntityManager().createQuery(
				"from Benutzerkonto b where lower(b.email) like :email and b.anwendung = :anwendung", Benutzerkonto.class);
			query.setParameter("email", "%" + email.toLowerCase() + "%");
			query.setParameter("anwendung", anwendung);

			final List<Benutzerkonto> trefferliste = query.getResultList();

			return trefferliste;
		} catch (final Exception e) {
			throw new EgladilStorageException(
				"Unerwartete Exception beim Suchen eines Benutzerkontos mit email unscharf: " + e.getMessage(), e);
		}
	}

	/**
	 * @see de.egladil.bv.aas.storage.IBenutzerDao#findBenutzerByUUID(java.lang.String)
	 */
	@Override
	public Benutzerkonto findBenutzerByUUID(final String uuid) throws EgladilStorageException {
		try {
			final TypedQuery<Benutzerkonto> query = getEntityManager().createQuery("from Benutzerkonto b where b.uuid = :uuid",
				Benutzerkonto.class);
			query.setParameter("uuid", uuid);

			final List<Benutzerkonto> result = query.getResultList();
			if (result.isEmpty()) {
				return null;
			}
			if (result.size() > 1) {
				throw new EgladilStorageException(
					"Benutzer mit UUID [" + uuid + "] existiert mehr als einmal - kann eigentlich nicht sein");
			}
			return result.get(0);
		} catch (final Exception e) {
			throw new EgladilStorageException("Unerwartete Exception beim Suchen eines Benutzerkontos", e);
		}
	}

	@Override
	public Optional<Benutzerkonto> findBenutzerByMostSignificantCharsOfUUID(final String chars) throws EgladilStorageException {
		try {
			final TypedQuery<Benutzerkonto> query = getEntityManager().createQuery("from Benutzerkonto b where b.uuid LIKE :uuid",
				Benutzerkonto.class);
			query.setParameter("uuid", chars + "%");

			final List<Benutzerkonto> result = query.getResultList();
			if (result.isEmpty()) {
				return Optional.empty();
			}
			if (result.size() > 1) {
				throw new EgladilStorageException(
					"Benutzer mit UUID like [" + chars + "] existiert mehr als einmal - kann eigentlich nicht sein");
			}
			return Optional.of(result.get(0));
		} catch (final Exception e) {
			throw new EgladilStorageException("Unerwartete Exception beim Suchen eines Benutzerkontos", e);
		}
	}
}
