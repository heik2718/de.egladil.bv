//=====================================================
// Projekt: de.egladil.common.validation
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.validation;

import java.util.Arrays;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;

import de.egladil.bv.aas.domain.Role;

/**
 * EnsureRoleValidator
 */
public class EnsureRoleValidator implements ConstraintValidator<EnsureRole, String> {

	private List<String> annotationStrings;

	@Override
	public void initialize(final EnsureRole constraintAnnotation) {
		annotationStrings = Arrays.asList(constraintAnnotation.erlaubteRollen());
	}

	@Override
	public boolean isValid(final String value, final ConstraintValidatorContext context) {
		if (StringUtils.isBlank(value)) {
			return true;
		}
		if (!annotationStrings.contains(value)) {
			return false;
		}
		for (final String str : annotationStrings) {
			try {
				Role.valueOf(str);
			} catch (final IllegalArgumentException e) {
				return false;
			}
		}
		return true;
	}
}
