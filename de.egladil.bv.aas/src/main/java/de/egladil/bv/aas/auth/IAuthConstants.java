//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================
	
package de.egladil.bv.aas.auth;

/**
* IAuthConstants
*/
public interface IAuthConstants {
	
	String XSRF_HEADER_KEY = "X-XSRF-TOKEN";

	String BEARER_KEY = "Authorization";

}
	