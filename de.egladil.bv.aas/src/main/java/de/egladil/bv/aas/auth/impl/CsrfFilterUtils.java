//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.auth.impl;

import java.util.List;

/**
 * CsrfFilterUtils
 */
public final class CsrfFilterUtils {

	private static final String OPTIONS = "OPTIONS";

	private static final String HEAD = "HEAD";

	private static final String GET = "GET";

	private static final String WILDCARDSUFFIX = "/**";

	/**
	 * CsrfFilterUtils
	 */
	private CsrfFilterUtils() {
	}

	/**
	 * @param pathInfo
	 * @return boolean
	 */
	public static boolean canPassWithoutCsrfToken(final List<String> openDataUrls, final String pathInfo, final String method) {
		if (method == null) {
			throw new IllegalArgumentException("method null");
		}
		if (OPTIONS.equals(method) || HEAD.equals(method)) {
			return true;
		}
		if (!GET.equals(method)) {
			return false;
		}
		if (openDataUrls == null) {
			return false;
		}
		if (pathInfo == null) {
			throw new IllegalArgumentException("pathInfo null");
		}
		for (final String part : openDataUrls) {
			if (part.endsWith(WILDCARDSUFFIX)) {
				final String prefix = new String(part.substring(0, part.length() - WILDCARDSUFFIX.length()));
				if (pathInfo.toLowerCase().startsWith(prefix.toLowerCase())) {
					return true;
				}
			} else {
				if (pathInfo.equalsIgnoreCase(part)) {
					return true;
				}
			}
		}
		return false;
	}
}
