//=====================================================
// Projekt: de.egladil.email.storage
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import de.egladil.common.persistence.IDomainObject;

/**
 * Mail nur das Zeug zum Senden. Versendet wird über den email-service mit der dortigen Konfiguration.
 */
@Entity
@Table(name = "mailqueue")
public class MailqueueItem implements IDomainObject, Serializable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	@NotNull
	@Size(max = 2000)
	@Column(name = "RECIPIENTS")
	private String recipients;

	@NotNull
	@Size(max = 100)
	@Column(name = "SUBJECT")
	private String subject;

	@NotNull
	@Size(max = 4000)
	@Column(name = "BODY")
	private String mailBody;

	@Size(max = 2000)
	@Column(name = "STATUSMESSAGE")
	private String statusmessage;

	@NotNull
	@Enumerated(EnumType.STRING)
	private MailqueueItemStatus status = MailqueueItemStatus.WAITING;

	@Version
	@Column(name = "VERSION")
	private int version;

	@Override
	public String toString() {
		return "MailqueueItem [id=" + id + ", subject=" + subject + ", status=" + status + ", recipients=" + recipients + "]";
	}

	/**
	 * Liefert die Membervariable id
	 *
	 * @return die Membervariable id
	 */
	@Override
	public Long getId() {
		return id;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param id neuer Wert der Membervariablen id
	 */
	public void setId(final Long id) {
		this.id = id;
	}

	/**
	 * Liefert die Membervariable to
	 *
	 * @return die Membervariable to
	 */
	public String getRecipients() {
		return recipients;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param to neuer Wert der Membervariablen to
	 */
	public void setRecipients(final String to) {
		this.recipients = to;
	}

	/**
	 * Liefert die Membervariable subject
	 *
	 * @return die Membervariable subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param subject neuer Wert der Membervariablen subject
	 */
	public void setSubject(final String subject) {
		this.subject = subject;
	}

	/**
	 * Liefert die Membervariable statusmessage
	 *
	 * @return die Membervariable statusmessage
	 */
	public String getStatusmessage() {
		return statusmessage;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param statusmessage neuer Wert der Membervariablen statusmessage
	 */
	public void setStatusmessage(final String statusmessage) {
		this.statusmessage = statusmessage;
	}

	public String getMailBody() {
		return mailBody;
	}

	public void setMailBody(final String mailBody) {
		this.mailBody = mailBody;
	}

	public MailqueueItemStatus getStatus() {
		return status;
	}

	public void setStatus(final MailqueueItemStatus status) {
		this.status = status;
	}

}
