//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import de.egladil.bv.aas.payload.TempPwdAendernPayload;

/**
 * PasswortNeuEmailValidator
 */
public class TempPwdAendernPayloadValidator implements ConstraintValidator<ValidTempPwdAendernPayload, TempPwdAendernPayload> {

	/**
	 * @see javax.validation.ConstraintValidator#initialize(java.lang.annotation.Annotation)
	 */
	@Override
	public void initialize(final ValidTempPwdAendernPayload constraintAnnotation) {
		// nix erforderlich
	}

	/**
	 * @see javax.validation.ConstraintValidator#isValid(java.lang.Object, javax.validation.ConstraintValidatorContext)
	 */
	@Override
	public boolean isValid(final TempPwdAendernPayload value, final ConstraintValidatorContext context) {
		if (value == null) {
			return true;
		}
		if (value.getPasswortNeu() != null && !value.getPasswortNeu().equals(value.getPasswortNeuWdh())) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate("{de.egladil.constraints.neuesPasswort.neuePasswoerter}").addBeanNode()
				.addConstraintViolation();
			return false;
		}
		if (value.getPasswort() != null && value.getPasswort().equals(value.getPasswortNeu())) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate("{de.egladil.constraints.changePasswort.altNeuGleich}").addBeanNode()
				.addConstraintViolation();
			return false;
		}
		return true;
	}
}
