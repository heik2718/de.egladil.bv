//=====================================================
// Projekt: de.egladil.mkm.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.auth;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import de.egladil.common.exception.EgladilBVException;

/**
 * Methoden zum Umgang mit AccessToken.
 *
 * @author heikew
 *
 */
public class AccessTokenUtils {

	private static final String ACCESS_TOKEN_ID_PREFIX = "Bearer ";

	/**
	 * Holt die accessTokenId aus dem gegebenen Header-Parameter heraus.<br>
	 * <br>
	 * Bei Dropwizard beginnt der http-Header Authorization immer mit einem Prefix Bearer. In dieser Komponente wird
	 * aber nur die eigentliche accessTokenId benötigt.
	 *
	 * @param headerParameter
	 * @return String
	 * @throws EgladilServerException
	 */
	public String extractAccessTokenId(String headerParameter) throws EgladilBVException {
		if (headerParameter == null) {
			throw new EgladilBVException("headerParameter war null");
		}
		String accessTokenId = headerParameter.replaceAll(ACCESS_TOKEN_ID_PREFIX, "");
		return accessTokenId;
	}

	/**
	 * Berechnet eine SHA-256-Checksumme zum gegebenen String.
	 *
	 * @param str
	 * @return
	 * @throws EgladilServerException
	 */
	public String shaChecksum(String str) throws EgladilBVException {
		try {
			MessageDigest mDigest = MessageDigest.getInstance("SHA-256");
			byte[] result = mDigest.digest(str.getBytes());
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < result.length; i++) {
				sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
			}

			return sb.toString();
		} catch (NoSuchAlgorithmException e) {
			throw new EgladilBVException(e.getMessage(), e);
		}
	}
}
