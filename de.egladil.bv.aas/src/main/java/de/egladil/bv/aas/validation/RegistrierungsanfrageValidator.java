//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import de.egladil.bv.aas.payload.Registrierungsanfrage;

/**
 * RegistrierungsanfrageValidator
 */
public class RegistrierungsanfrageValidator
	implements ConstraintValidator<ValidRegistrierungsanfrage, Registrierungsanfrage> {

	/**
	 * @see javax.validation.ConstraintValidator#initialize(java.lang.annotation.Annotation)
	 */
	@Override
	public void initialize(ValidRegistrierungsanfrage constraintAnnotation) {
		// nix erforderlich
	}

	/**
	 * @see javax.validation.ConstraintValidator#isValid(java.lang.Object, javax.validation.ConstraintValidatorContext)
	 */
	@Override
	public boolean isValid(Registrierungsanfrage value, ConstraintValidatorContext context) {
		if (value == null){
			return true;
		}
		if (value.getRole() == null  || value.getAnwendung() == null){
			return true;
		}
		if (!value.getAnwendung().getAssignedRollen().contains(value.getRole())){
			return false;
		}
		return true;
	}
}
