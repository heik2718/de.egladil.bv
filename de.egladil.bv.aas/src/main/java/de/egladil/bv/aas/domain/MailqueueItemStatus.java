//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.domain;

/**
* MailqueueItemStatus
*/
public enum MailqueueItemStatus {

	FAILURE,
	SENT,
	WAITING;
}
