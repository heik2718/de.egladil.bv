//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 *
 */
@Target({ ElementType.TYPE, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = TempPwdAendernPayloadValidator.class)
@Documented
public @interface ValidTempPwdAendernPayload {

	String message() default "{de.egladil.constraints.passwortNeuEmail}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
