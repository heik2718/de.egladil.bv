package de.egladil.bv.aas;

import java.util.List;
import java.util.Optional;

import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.domain.Role;
import de.egladil.common.exception.DisabledAccountException;
import de.egladil.common.exception.EgladilAuthenticationException;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.EgladilStorageException;

public interface IBenutzerService {

	/**
	 * Sucht das Benutzerkonto mit gegebenem loginNamen zu der gegenen Anwendung.
	 *
	 * @param loginName
	 * @param anwendung
	 * @return Benutzerkonto
	 * @throws EgladilStorageException
	 * @throws EgladilServerException
	 */
	Benutzerkonto findBenutzerkontoByLoginName(String loginName, Anwendung anwendung) throws EgladilStorageException;

	/**
	 * Sucht die Benutzerkonten der gegebenen Anwendung, deren email like gegebene email ist.
	 *
	 * @param email String darf nicht null sein.
	 * @param anwendung Anwendung
	 * @return List
	 * @throws EgladilStorageException
	 * @throws IllegalArgumentException
	 */
	List<Benutzerkonto> findByEmailLike(String email, Anwendung anwendung) throws EgladilStorageException, IllegalArgumentException;

	/**
	 * Sucht das Benutzerkonto anhand der UUID.
	 *
	 * @param principalName
	 * @return Benutzerkonto oder null
	 */
	Benutzerkonto findBenutzerkontoByUUID(String principalName);

	/**
	 * Sucht das Benutzerkonto anhand der ersten Zeichen der UUID.
	 *
	 * @param firstChars String darf nicht blank sein.
	 * @return Optional<Benutzerkonto>
	 */
	Optional<Benutzerkonto> findBenutzerkontoByFirstCharsOfUUID(String firstChars)
		throws IllegalArgumentException, EgladilStorageException;

	/**
	 * Falls es ein Benutzerkonto mit dieser Mailadresse gibt, wird ein Eintrag in Aktivierungsdaten erzeugt.
	 *
	 * @param email
	 * @param anwendung
	 * @param expiresInMinutes Glültigkeit der Aktivierungsdaten in Minuten.
	 * @return Aktivierungsdaten
	 */
	Aktivierungsdaten generateResetPasswordCode(String email, Anwendung anwendung, int expiresInMinutes)
		throws IllegalArgumentException, EgladilAuthenticationException, DisabledAccountException, EgladilStorageException;

	/**
	 * Speichert das gegeben Benutzerkonto und gibt das gespeicherte zurück.
	 *
	 * @param benutzer
	 * @return
	 */
	Benutzerkonto persistBenutzerkonto(Benutzerkonto benutzer)
		throws EgladilDuplicateEntryException, EgladilConcurrentModificationException, EgladilStorageException;

	/**
	 * Prüft, ob der Benutzer diese EnsureRole hat.<br>
	 * <br>
	 * FIXME kann weg, wenn ich mal Zeit hatte, die PrincipalMap beim Authentisieren odrentlich zu füllen und das
	 * AccessToken umgeschrieben haben auf PrincipalMap.
	 *
	 * @param role
	 * @param principalName
	 * @return
	 * @throws IllegalArgumentException
	 * @throws EgladilStorageException
	 */
	boolean isBenutzerInRole(Role role, String principalName) throws IllegalArgumentException, EgladilStorageException;

	/**
	 * Für Integrationstests, solange es noch kein Docker gibt.
	 *
	 * @param tempPwd
	 */
	void setTempPasswordForTest(String tempPwd);
}