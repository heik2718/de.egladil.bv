//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

/**
 * Enthält alle Klassen für eine sichere Authentisierung in einer DropWizard-Application.
 * 
 */
package de.egladil.bv.aas.auth;