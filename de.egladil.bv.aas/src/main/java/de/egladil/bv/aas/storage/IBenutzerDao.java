//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.storage;

import java.util.List;
import java.util.Optional;

import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.common.persistence.IBaseDao;

public interface IBenutzerDao extends IBaseDao<Benutzerkonto> {

	/**
	 * Sucht den Benutzer mit diesem Loginnamen. Die Suche findet caseinsensitive statt.
	 *
	 * @param loginName
	 * @param anwendung
	 * @return Benutzerkonto oder null, wenn es nicht gefunden wurde.
	 */
	Benutzerkonto findBenutzerByLoginName(String loginName, Anwendung anwendung) throws EgladilStorageException;

	/**
	 * Sucht den Benutzer mit dieser Mailadresse. Die Suche findet caseinsensitive statt.
	 *
	 * @param email
	 * @param anwendung
	 * @return Benutzerkonto oder null, wenn es nicht gefunden wurde.
	 */
	Benutzerkonto findBenutzerByEmail(String email, Anwendung anwendung) throws EgladilStorageException;

	/**
	 *
	 * @param email String darf nicht null sein
	 * @param anwendung Anwendung darf nicht null sein
	 * @return List
	 * @throws EgladilStorageException
	 * @throws IllegalArgumentException
	 */
	List<Benutzerkonto> findByEmailLike(final String email, final Anwendung anwendung)
		throws EgladilStorageException, IllegalArgumentException;

	/**
	 * Sucht den Benutzer mit dieser uuid.
	 *
	 * @param uuid
	 * @return Benutzerkonto oder null, wenn es nicht gefunden wurde.
	 */
	Benutzerkonto findBenutzerByUUID(String uuid) throws EgladilStorageException;

	/**
	 * Sucht ein Benutzerkonto, dessen UUID mit chars beginnt.
	 *
	 * @param chars die ersten Zeichen der UUID. Darf nicht blank sein!
	 * @return Optional<Benutzerkonto>
	 */
	Optional<Benutzerkonto> findBenutzerByMostSignificantCharsOfUUID(String chars)
		throws EgladilStorageException, IllegalArgumentException;
}