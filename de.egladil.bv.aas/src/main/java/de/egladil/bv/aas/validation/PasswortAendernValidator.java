//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import de.egladil.bv.aas.payload.PasswortAendernPayload;

/**
 * PasswortAendernValidator
 */
public class PasswortAendernValidator implements ConstraintValidator<ValidPasswortAendernAnfrage, PasswortAendernPayload> {

	/**
	 * @see javax.validation.ConstraintValidator#initialize(java.lang.annotation.Annotation)
	 */
	@Override
	public void initialize(ValidPasswortAendernAnfrage constraintAnnotation) {
		// nix erforderlich
	}

	/**
	 * @see javax.validation.ConstraintValidator#isValid(java.lang.Object, javax.validation.ConstraintValidatorContext)
	 */
	@Override
	public boolean isValid(PasswortAendernPayload value, ConstraintValidatorContext context) {
		if (value == null) {
			return true;
		}
		if (value.getPasswortNeu() != null && !value.getPasswortNeu().equals(value.getPasswortNeuWdh())) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate("{de.egladil.constraints.neuesPasswort.neuePasswoerter}").addBeanNode()
				.addConstraintViolation();
			return false;
		}
		if (value.getPasswort() != null && value.getPasswort().equals(value.getPasswortNeu())){
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate("{de.egladil.constraints.changePasswort.altNeuGleich}").addBeanNode()
				.addConstraintViolation();
			return false;
		}
		return true;
	}
}
