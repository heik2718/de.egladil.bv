RENAME TABLE blqzbv.mails TO blqzbv.mailqueue;

ALTER TABLE blqzbv.mailqueue
   change COLUMN `RECIPIENT` `RECIPIENTS` varchar(2000)  COLLATE utf8_unicode_ci NOT NULL;

ALTER TABLE blqzbv.mailqueue
   change COLUMN `SUBJECT` `SUBJECT` varchar(100)  COLLATE utf8_unicode_ci NOT NULL;

ALTER TABLE blqzbv.mailqueue
   change COLUMN `MESSAGE` `BODY` varchar(2000)  COLLATE utf8_unicode_ci NOT NULL;

ALTER TABLE blqzbv.mailqueue
   change COLUMN `STATUSMESSAGE` `STATUSMESSAGE` varchar(2000)  COLLATE utf8_unicode_ci NOT NULL;

ALTER TABLE blqzbv.mailqueue ADD COLUMN  `VERSION` int(10) NOT NULL DEFAULT '0';

ALTER TABLE blqzbv.mailqueue change COLUMN `SENT` `DATE_MODIFIED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;

ALTER TABLE blqzbv.mailqueue ADD COLUMN  `STATUS` varchar(10)  COLLATE utf8_unicode_ci NOT NULL;
