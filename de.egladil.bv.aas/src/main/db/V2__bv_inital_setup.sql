-- MySQL dump 10.15  Distrib 10.0.28-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: localhost
-- ------------------------------------------------------
-- Server version	10.0.28-MariaDB-0+deb8u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `blqzbv`
--

--
-- Table structure for table `aktivierungsdaten`
--

DROP TABLE IF EXISTS `aktivierungsdaten`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aktivierungsdaten` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CONFIRM_CODE` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `CONFIRM_EXPIRETIME` datetime NOT NULL,
  `BENUTZER` int(10) unsigned NOT NULL,
  `VERSION` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uk_aktivierungsdaten_1` (`CONFIRM_CODE`),
  KEY `fk_registrierung_benutzer` (`BENUTZER`),
  CONSTRAINT `fk_aktivierung_benutzer` FOREIGN KEY (`BENUTZER`) REFERENCES `benutzer` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=183 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `benutzer`
--

DROP TABLE IF EXISTS `benutzer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `benutzer` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `LOGINNAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `AKTIVIERT` tinyint(1) NOT NULL,
  `PID` int(10) NOT NULL,
  `VERSION` int(10) DEFAULT NULL,
  `EMAIL` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ANWENDUNG` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATE_MODIFIED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `GESPERRT` tinyint(1) NOT NULL DEFAULT '0',
  `UUID` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uk_benutzer_3` (`UUID`),
  UNIQUE KEY `uk_benutzer_1` (`LOGINNAME`,`ANWENDUNG`),
  UNIQUE KEY `uk_benutzer_2` (`EMAIL`,`ANWENDUNG`),
  KEY `fk_benutzer_pw` (`PID`),
  CONSTRAINT `fk_benutzer_pw` FOREIGN KEY (`PID`) REFERENCES `pw` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=189 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `benutzerrollen`
--

DROP TABLE IF EXISTS `benutzerrollen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `benutzerrollen` (
  `ROLLE_ID` int(10) unsigned NOT NULL,
  `BENUTZER_ID` int(10) unsigned NOT NULL,
  UNIQUE KEY `uk_benutzerrollen` (`ROLLE_ID`,`BENUTZER_ID`),
  KEY `fk_br_benutzer` (`BENUTZER_ID`),
  CONSTRAINT `fk_br_benutzer` FOREIGN KEY (`BENUTZER_ID`) REFERENCES `benutzer` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_br_rolle` FOREIGN KEY (`ROLLE_ID`) REFERENCES `kat_rollen` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ipsperren`
--

DROP TABLE IF EXISTS `ipsperren`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ipsperren` (
  `IP` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `LOGINFAILS` int(3) DEFAULT NULL,
  `PENALTYEND` datetime DEFAULT NULL,
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `kat_rechte`
--

DROP TABLE IF EXISTS `kat_rechte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kat_rechte` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PERMISSION` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ROLLE` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uk_rechte_1` (`PERMISSION`,`ROLLE`),
  KEY `fk_rechte_rollen` (`ROLLE`),
  CONSTRAINT `fk_rechte_rollen` FOREIGN KEY (`ROLLE`) REFERENCES `kat_rollen` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kat_rechte`
--

LOCK TABLES `kat_rechte` WRITE;
/*!40000 ALTER TABLE `kat_rechte` DISABLE KEYS */;
INSERT INTO `kat_rechte` VALUES (3,'*',1),(4,'*',5),(5,'*',7),(6,'*',9),(7,'*',10);
/*!40000 ALTER TABLE `kat_rechte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kat_rollen`
--

DROP TABLE IF EXISTS `kat_rollen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kat_rollen` (
  `NAME` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uk_rollen_1` (`NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kat_rollen`
--

LOCK TABLES `kat_rollen` WRITE;
/*!40000 ALTER TABLE `kat_rollen` DISABLE KEYS */;
INSERT INTO `kat_rollen` VALUES ('BQ_ADMIN',1),('BQ_AUTOR',2),('BQ_SPIELER',3),('BQ_TRANSLATOR',4),('BV_ADMIN',9),('MKM_ADMIN',10),('MKV_ADMIN',5),('MKV_LEHRER',6),('MKV_PRIVAT',11),('WB_ADMIN',7),('WB_AUTOR',8);
/*!40000 ALTER TABLE `kat_rollen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mails`
--

DROP TABLE IF EXISTS `mails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mails` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `RECIPIENT` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `SUBJECT` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `MESSAGE` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `SENT` datetime DEFAULT NULL,
  `STATUSMESSAGE` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pw`
--

DROP TABLE IF EXISTS `pw`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pw` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `PWHASH` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `VERSION` int(10) DEFAULT NULL,
  `SLZ` int(10) unsigned NOT NULL,
  `LOGIN_PERIOD_STARTTIME` datetime DEFAULT NULL,
  `LAST_LOGIN_ATTEMPT` datetime DEFAULT NULL,
  `DATE_MODIFIED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `LOCK_EXPIRATION_TIME` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_pw_slz` (`SLZ`),
  CONSTRAINT `fk_pw_slz` FOREIGN KEY (`SLZ`) REFERENCES `slz` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=230 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `slz`
--

DROP TABLE IF EXISTS `slz`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slz` (
  `ROUNDS` int(10) NOT NULL,
  `ALGORITHM` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `VERSION` int(10) DEFAULT NULL,
  `WERT` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `DATE_MODIFIED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=218 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-27 15:52:21
