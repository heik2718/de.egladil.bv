//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * BenutzerkontoTest
 */
public class BenutzerkontoTest {

	private static final String CONFIRMCODE = "Hallo645";

	private Benutzerkonto benutzerkonto;

	@BeforeEach
	public void setUp(){
		benutzerkonto = new Benutzerkonto();
	}

	@Test
	@DisplayName("throws IllegalArgumentException when parameter null")
	public void findAktivierungsdatenByConfirmCodeParamNull() {
		final Throwable exception = assertThrows(IllegalArgumentException.class, () -> {
			benutzerkonto.findAktivierungsdatenByConfirmCode(null);
		});
		assertEquals("confirmCode null", exception.getMessage());
		// Act + Assert

	}

	@Test
	@DisplayName("returns empty list on empty list")
	public void findAktivierungsdatenByConfirmCodeEmptyList() {
		// Act
		final Optional<Aktivierungsdaten> opt = benutzerkonto.findAktivierungsdatenByConfirmCode(CONFIRMCODE);

		// Assert
		assertFalse(opt.isPresent());
	}

	@Test
	@DisplayName("returns empty list on null list")
	public void findAktivierungsdatenByConfirmCodeNullList() {
		// Arrange
		benutzerkonto.setAktivierungsdaten(null);

		// Act
		final Optional<Aktivierungsdaten> opt = benutzerkonto.findAktivierungsdatenByConfirmCode(CONFIRMCODE);

		// Assert
		assertFalse(opt.isPresent());
	}

	@Test
	@DisplayName("returns empty list when not contained")
	public void findAktivierungsdatenByConfirmCodeNotContained() {
		// Arrange
		benutzerkonto.setAktivierungsdaten(null);
		final Aktivierungsdaten aktivierungsdaten = new Aktivierungsdaten();
		aktivierungsdaten.setConfirmationCode(CONFIRMCODE);
		benutzerkonto.addAktivierungsdaten(aktivierungsdaten);

		// Act
		final Optional<Aktivierungsdaten> opt = benutzerkonto.findAktivierungsdatenByConfirmCode("Zrwt53Vgsv");

		// Assert
		assertFalse(opt.isPresent());
	}

	@Test
	@DisplayName("returns aktivierungsdaten list when contained")
	public void findAktivierungsdatenByConfirmCodeContained() {
		// Arrange
		final Benutzerkonto benutzerkonto = new Benutzerkonto();
		benutzerkonto.setAktivierungsdaten(null);
		final Aktivierungsdaten aktivierungsdaten = new Aktivierungsdaten();
		aktivierungsdaten.setConfirmationCode(CONFIRMCODE);
		benutzerkonto.addAktivierungsdaten(aktivierungsdaten);

		// Act
		final Optional<Aktivierungsdaten> opt = benutzerkonto.findAktivierungsdatenByConfirmCode(CONFIRMCODE);

		// Assert
		assertTrue(opt.isPresent());
	}
}
