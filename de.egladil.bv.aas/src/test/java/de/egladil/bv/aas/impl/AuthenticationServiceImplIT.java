//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.google.inject.Inject;
import com.google.inject.persist.Transactional;

import de.egladil.bv.aas.AbstractGuiceIT;
import de.egladil.bv.aas.auth.UsernamePasswordToken;
import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.storage.IBenutzerDao;
import de.egladil.common.exception.IncorrectCredentialsException;
import de.egladil.common.exception.UnknownAccountException;

/**
 * AuthenticationServiceImplTest
 */
public class AuthenticationServiceImplIT extends AbstractGuiceIT {

	@Inject
	private AuthenticationServiceImpl authenticationService;

	@Inject
	private IBenutzerDao benutzerDao;

	@Inject
	private AccessFrequencyGuard accessFrequencyGuard;

	@Test
	@Transactional
	@DisplayName("findBenutzerByLoginName should verify password")
	public void findBenutzerByLoginName() {
		// Arrange
		final Benutzerkonto benutzerkonto = benutzerDao.findBenutzerByLoginName("heike1", Anwendung.BQ);
		assertNotNull("Testaufbau: Benutzerkonto fehlt", benutzerkonto);

		// Act
		authenticationService.verifyPasswort("Qwertz!1".toCharArray(), benutzerkonto);
		assertFalse(benutzerkonto.getRollen().isEmpty());
	}

	@Test
	public void authenticate_calls_check_frequency() {
		// Arrange
		final Benutzerkonto benutzerkonto = benutzerDao.findBenutzerByLoginName("heike1", Anwendung.BQ);
		assertNotNull("Testaufbau: Benutzerkonto fehlt", benutzerkonto);
		accessFrequencyGuard.setAnzCheckFrequencyCalled(0);

		// Act
		authenticationService.authenticate(new UsernamePasswordToken("heike1", "Qwertz!1".toCharArray()), Anwendung.BQ);
		assertFalse(benutzerkonto.getRollen().isEmpty());

		// Assert
		assertEquals(1, accessFrequencyGuard.getAnzCheckFrequencyCalled());
	}

	@Test
	@DisplayName("verifyTemporaryPassword throws UnknownAccountException when email anwendung not found")
	@Transactional
	public void verifyTemporaryPasswordEmailNotFound() {
		final Throwable exception = assertThrows(UnknownAccountException.class, () -> {
			authenticationService.authenticateWithTemporaryPasswort(Anwendung.MKV, "x@x.de", "hdget7Z5");
		});
		assertEquals("Kein Benutzerkonto mit [email=x@x.de, Anwendung=MKV] bekannt", exception.getMessage());

	}

	@Test
	@DisplayName("verifyTemporaryPassword throws IncorrectCredentialsException when tempPassword not found")
	@Transactional
	public void verifyTemporaryPasswordTempPasswordNotFound() {
		final Throwable exception = assertThrows(IncorrectCredentialsException.class, () -> {
			authenticationService.authenticateWithTemporaryPasswort(Anwendung.MKV, "huhu@egladil.de", "hdget7Z5");
		});
		assertEquals("email stimmt, temporaeres passwort nicht", exception.getMessage());
	}
}
