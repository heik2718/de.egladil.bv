//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas;

import java.util.HashMap;
import java.util.Map;

import com.google.inject.AbstractModule;
import com.google.inject.Binder;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Names;
import com.google.inject.persist.PersistService;
import com.google.inject.persist.jpa.JpaPersistModule;

import de.egladil.bv.aas.auth.impl.CachingAccessTokenAuthenticator;
import de.egladil.bv.aas.config.BVPersistenceModule;
import de.egladil.bv.aas.impl.AuthenticationServiceImpl;
import de.egladil.bv.aas.impl.BenutzerServiceImpl;
import de.egladil.bv.aas.impl.RegistrierungServiceImpl;
import de.egladil.common.config.AbstractEgladilConfiguration;
import de.egladil.common.config.IEgladilConfiguration;
import de.egladil.crypto.provider.IEgladilCryptoUtils;
import de.egladil.crypto.provider.impl.EgladilCryptoUtilsImpl;
import io.dropwizard.auth.Authenticator;

/**
 * MKVModule
 */
public class BVTestModule extends AbstractModule {

	private final String pathConfigRoot;

	/**
	 * Erzeugt eine Instanz von BVTestModule
	 */
	public BVTestModule(final String pathConfigRoot) {
		this.pathConfigRoot = pathConfigRoot;
	}

	/**
	 * @see com.google.inject.AbstractModule#configure()
	 */
	@Override
	protected void configure() {
		loadProperties(binder());
		install(new BVPersistenceModule(this.pathConfigRoot));

		createPersistModule();
		bind(IAuthenticationService.class).to(AuthenticationServiceImpl.class);
		// bind(Authenticator.class).to(CachingAccessTokenAuthenticator.class);
		bind(Authenticator.class).to(CachingAccessTokenAuthenticator.class);
		bind(IBenutzerService.class).to(BenutzerServiceImpl.class);
		bind(IEgladilCryptoUtils.class).to(EgladilCryptoUtilsImpl.class);

		bind(IRegistrierungService.class).to(RegistrierungServiceImpl.class);
	}

	@Singleton
	public static class JPAInitializer {

		@Inject
		public JPAInitializer(final PersistService service) {
			service.start();
		}

	}

	private JpaPersistModule createPersistModule() {
		final IEgladilConfiguration persistenceConfig = new AbstractEgladilConfiguration(pathConfigRoot) {

			/* serialVersionUID */
			private static final long serialVersionUID = 1L;

			@Override
			protected String getConfigFileName() {
				return "bv_persistence.properties";
			}
		};
		final JpaPersistModule jpaModule = new JpaPersistModule("de.egladil.bv.aas");
		jpaModule.properties(persistenceConfig.getConfigurationMap());
		return jpaModule;
	}

	protected void loadProperties(final Binder binder) {
		final Map<String, String> properties = new HashMap<>();
		properties.put("configRoot", pathConfigRoot);
		Names.bindProperties(binder, properties);
	}
}
