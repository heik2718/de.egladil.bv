//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.google.inject.Inject;
import com.google.inject.persist.Transactional;

import de.egladil.bv.aas.AbstractGuiceIT;
import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.domain.Role;
import de.egladil.bv.aas.payload.Registrierungsanfrage;

/**
 * BenutzerServiceImplIT
 */
public class RegistrierungServiceImplIT extends AbstractGuiceIT {

	@Inject
	private RegistrierungServiceImpl registrierunsgsservice;

	@Test
	@DisplayName("register klappt")
	@Transactional
	public void test1() {
		// Arrange
		final String loginName = "BenutzerServiceImplIT-" + System.currentTimeMillis();
		final String passwort = "geH31m!!";
		final String email = "mail-" + System.currentTimeMillis() + "@test.com";

		// Act
		final Aktivierungsdaten bestaetigung = registrierunsgsservice
			.register(new Registrierungsanfrage(loginName, passwort, email, Role.BQ_SPIELER, Anwendung.BQ));

		// Assert
		assertNotNull(bestaetigung);
		final Benutzerkonto benutzerkonto = bestaetigung.getBenutzerkonto();
		assertFalse(benutzerkonto.isAktiviert());
		assertFalse(benutzerkonto.isGesperrt());
	}

	@Test
	@DisplayName("register mit langem Passwort klappt")
	@Transactional
	public void test2() {
		// Arrange
		final String loginName = "BenutzerServiceImplIT-" + System.currentTimeMillis();
		final String passwort = "Langes geH31ms Passwort mit mehr als 20 Zeichen";
		assertTrue(passwort.length() > 20);
		final String email = "mail-" + System.currentTimeMillis() + "@test.com";

		// Act
		final Aktivierungsdaten bestaetigung = registrierunsgsservice
			.register(new Registrierungsanfrage(loginName, passwort, email, Role.BQ_SPIELER, Anwendung.BQ));

		// Assert
		assertNotNull(bestaetigung);
		final Benutzerkonto benutzerkonto = bestaetigung.getBenutzerkonto();
		assertFalse(benutzerkonto.isAktiviert());
		assertFalse(benutzerkonto.isGesperrt());
	}
}
