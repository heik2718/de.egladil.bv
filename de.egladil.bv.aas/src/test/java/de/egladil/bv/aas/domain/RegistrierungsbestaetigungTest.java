//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.domain;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * RegistrierungsbestaetigungTest
 */
public class RegistrierungsbestaetigungTest {

	private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");

	@Test
	@DisplayName("isExpired returns true when expired the previuos second")
	public void testPreviousSecond() throws Exception {
		// Arrange
		final Date expirationTime = SIMPLE_DATE_FORMAT.parse("15.10.2016 07:09:24");
		final Date now = SIMPLE_DATE_FORMAT.parse("15.10.2016 07:10:25");

		final Aktivierungsdaten regBest = new Aktivierungsdaten();
		regBest.setExpirationTime(expirationTime);

		// Act
		final boolean expired = regBest.isExpired(now);

		// Assert
		assertTrue(expired);
	}

	@Test
	@DisplayName("isExpired returns false when expired exactly this second")
	public void isExpiredNow() throws Exception {
		// Arrange
		final Date expirationTime = SIMPLE_DATE_FORMAT.parse("15.10.2016 07:09:24");
		final Date now = SIMPLE_DATE_FORMAT.parse("15.10.2016 07:10:24");

		final Aktivierungsdaten regBest = new Aktivierungsdaten();
		regBest.setExpirationTime(expirationTime);

		// Act
		final boolean expired = regBest.isExpired(now);

		// Assert
		assertFalse(expired);
	}

}
