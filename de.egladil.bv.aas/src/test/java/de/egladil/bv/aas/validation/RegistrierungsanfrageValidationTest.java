//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.validation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Role;
import de.egladil.bv.aas.payload.Registrierungsanfrage;

/**
 * RegistrierungsanfrageValidationTest
 */
public class RegistrierungsanfrageValidationTest {

	private static final Logger LOG = LoggerFactory.getLogger(RegistrierungsanfrageValidationTest.class);

	@Test
	@DisplayName("passes when valid")
	public void validateValid() {
		// Arrange
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();
		final Registrierungsanfrage anfrage = createValid();

		// Act
		final Set<ConstraintViolation<Registrierungsanfrage>> errors = validator.validate(anfrage);

		// Assert
		assertTrue(errors.isEmpty());
	}

	@Test
	@DisplayName("fails when anwendung null")
	public void validateAnwendungNull() {
		// Arrange
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();
		final Registrierungsanfrage anfrage = createValid();
		anfrage.setAnwendung(null);

		// Act
		final Set<ConstraintViolation<Registrierungsanfrage>> errors = validator.validate(anfrage);

		// Assert
		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<Registrierungsanfrage> cv = errors.iterator().next();
		LOG.info(cv.getMessage());
		assertEquals("anwendung", cv.getPropertyPath().toString());
	}

	@Test
	@DisplayName("fails when role null")
	public void validateRoleNull() {
		// Arrange
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();
		final Registrierungsanfrage anfrage = createValid();
		anfrage.setRole(null);

		// Act
		final Set<ConstraintViolation<Registrierungsanfrage>> errors = validator.validate(anfrage);

		// Assert
		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<Registrierungsanfrage> cv = errors.iterator().next();
		LOG.info(cv.getMessage());
		assertEquals("role", cv.getPropertyPath().toString());
	}

	@Test
	@DisplayName("fails when loginName null")
	public void validateLoginNameNull() {
		// Arrange
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();
		final Registrierungsanfrage anfrage = createValid();
		anfrage.setLoginName(null);

		// Act
		final Set<ConstraintViolation<Registrierungsanfrage>> errors = validator.validate(anfrage);

		// Assert
		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<Registrierungsanfrage> cv = errors.iterator().next();
		LOG.info(cv.getMessage());
		assertEquals("loginName", cv.getPropertyPath().toString());
	}

	@Test
	@DisplayName("fails when loginName invalid")
	public void validateLoginNameInvalid() {
		// Arrange
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();
		final Registrierungsanfrage anfrage = createValid();
		anfrage.setLoginName(anfrage.getLoginName() + "&lt;");

		// Act
		final Set<ConstraintViolation<Registrierungsanfrage>> errors = validator.validate(anfrage);

		// Assert
		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<Registrierungsanfrage> cv = errors.iterator().next();
		LOG.info(cv.getMessage());
		assertEquals("loginName", cv.getPropertyPath().toString());
	}

	@Test
	@DisplayName("fails when loginName empty")
	public void validateLoginNameEmpty() {
		// Arrange
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();
		final Registrierungsanfrage anfrage = createValid();
		anfrage.setLoginName("");

		// Act
		final Set<ConstraintViolation<Registrierungsanfrage>> errors = validator.validate(anfrage);

		// Assert
		assertFalse(errors.isEmpty());
		assertEquals(2, errors.size());

		final Iterator<ConstraintViolation<Registrierungsanfrage>> iter = errors.iterator();

		final Set<String> properties = new HashSet<>();

		while (iter.hasNext()) {
			final ConstraintViolation<Registrierungsanfrage> cv = iter.next();
			properties.add(cv.getPropertyPath().toString());
			LOG.info(cv.getMessage());
		}

		assertEquals(1, properties.size());
		assertTrue(properties.contains("loginName"));
	}

	@Test
	@DisplayName("fails when loginName blank")
	public void validateLoginNameBlank() {
		// Arrange
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();
		final Registrierungsanfrage anfrage = createValid();
		anfrage.setLoginName(" ");

		// Act
		final Set<ConstraintViolation<Registrierungsanfrage>> errors = validator.validate(anfrage);

		// Assert
		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final Iterator<ConstraintViolation<Registrierungsanfrage>> iter = errors.iterator();

		final Set<String> properties = new HashSet<>();

		while (iter.hasNext()) {
			final ConstraintViolation<Registrierungsanfrage> cv = iter.next();
			properties.add(cv.getPropertyPath().toString());
			LOG.info(cv.getMessage());
		}

		assertEquals(1, properties.size());
		assertTrue(properties.contains("loginName"));
	}

	@Test
	@DisplayName("fails when loginName too long")
	public void validateLoginNameTooLong() {
		// Arrange
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();
		final Registrierungsanfrage anfrage = createValid();
		final StringBuffer sb = new StringBuffer();
		for (int i = 0; i < 101; i++) {
			sb.append("a");
		}

		anfrage.setLoginName(sb.toString());
		assertEquals("Testsetting fehlerhaft", 101, anfrage.getLoginName().length());

		// Act
		final Set<ConstraintViolation<Registrierungsanfrage>> errors = validator.validate(anfrage);

		// Assert
		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<Registrierungsanfrage> cv = errors.iterator().next();
		LOG.info(cv.getMessage());
		assertEquals("loginName", cv.getPropertyPath().toString());
	}

	@Test
	@DisplayName("fails when password invalid")
	public void validatePasswortInvalid() {
		// Arrange
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();
		final Registrierungsanfrage anfrage = createValid();
		anfrage.setPasswort("Qwertz~");

		// Act
		final Set<ConstraintViolation<Registrierungsanfrage>> errors = validator.validate(anfrage);

		// Assert
		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<Registrierungsanfrage> cv = errors.iterator().next();
		LOG.info(cv.getMessage());
		assertEquals("passwort", cv.getPropertyPath().toString());
	}

	@Test
	@DisplayName("fails when password null")
	public void validatePasswortNull() {
		// Arrange
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();
		final Registrierungsanfrage anfrage = createValid();
		anfrage.setPasswort(null);

		// Act
		final Set<ConstraintViolation<Registrierungsanfrage>> errors = validator.validate(anfrage);

		// Assert
		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<Registrierungsanfrage> cv = errors.iterator().next();
		LOG.info(cv.getMessage());
		assertEquals("passwort", cv.getPropertyPath().toString());
	}

	@Test
	@DisplayName("fails when password leer")
	public void validatePasswortLeer() {
		// Arrange
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();
		final Registrierungsanfrage anfrage = createValid();
		anfrage.setPasswort("");

		// Act
		final Set<ConstraintViolation<Registrierungsanfrage>> errors = validator.validate(anfrage);

		// Assert
		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<Registrierungsanfrage> cv = errors.iterator().next();
		LOG.info(cv.getMessage());
		assertEquals("passwort", cv.getPropertyPath().toString());
	}

	@Test
	@DisplayName("fails when password blank")
	public void validatePasswortBlank() {
		// Arrange
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();
		final Registrierungsanfrage anfrage = createValid();
		anfrage.setPasswort(" ");

		// Act
		final Set<ConstraintViolation<Registrierungsanfrage>> errors = validator.validate(anfrage);

		// Assert
		assertFalse(errors.isEmpty());
		assertEquals(2, errors.size());

		final Iterator<ConstraintViolation<Registrierungsanfrage>> iter = errors.iterator();

		final Set<String> properties = new HashSet<>();

		while (iter.hasNext()) {
			final ConstraintViolation<Registrierungsanfrage> cv = iter.next();
			properties.add(cv.getPropertyPath().toString());
			LOG.info(cv.getMessage());
		}

		assertEquals(1, properties.size());
		assertTrue(properties.contains("passwort"));
	}

	@Test
	@DisplayName("fails when email null")
	public void validateEmailNull() {
		// Arrange
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();
		final Registrierungsanfrage anfrage = createValid();
		anfrage.setEmail(null);

		// Act
		final Set<ConstraintViolation<Registrierungsanfrage>> errors = validator.validate(anfrage);

		// Assert
		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<Registrierungsanfrage> cv = errors.iterator().next();
		LOG.info(cv.getMessage());
		assertEquals("email", cv.getPropertyPath().toString());
	}

	@Test
	@DisplayName("fails when email empty")
	public void validateEmailEmpty() {
		// Arrange
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();
		final Registrierungsanfrage anfrage = createValid();
		anfrage.setEmail("");

		// Act
		final Set<ConstraintViolation<Registrierungsanfrage>> errors = validator.validate(anfrage);

		// Assert
		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<Registrierungsanfrage> cv = errors.iterator().next();
		LOG.info(cv.getMessage());
		assertEquals("email", cv.getPropertyPath().toString());
	}

	@Test
	@DisplayName("fails when email blank")
	public void validateEmailBlank() {
		// Arrange
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();
		final Registrierungsanfrage anfrage = createValid();
		anfrage.setEmail(" ");

		// Act
		final Set<ConstraintViolation<Registrierungsanfrage>> errors = validator.validate(anfrage);

		// Assert
		assertFalse(errors.isEmpty());
		assertEquals(2, errors.size());

		final Iterator<ConstraintViolation<Registrierungsanfrage>> iter = errors.iterator();

		final Set<String> properties = new HashSet<>();

		while (iter.hasNext()) {
			final ConstraintViolation<Registrierungsanfrage> cv = iter.next();
			properties.add(cv.getPropertyPath().toString());
			LOG.info(cv.getMessage());
		}

		assertEquals(1, properties.size());
		assertTrue(properties.contains("email"));
	}

	@Test
	@DisplayName("fails when email invalid")
	public void validateEmailInvalid() {
		// Arrange
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();
		final Registrierungsanfrage anfrage = createValid();
		anfrage.setEmail("mail@provider");

		// Act
		final Set<ConstraintViolation<Registrierungsanfrage>> errors = validator.validate(anfrage);

		// Assert
		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final Iterator<ConstraintViolation<Registrierungsanfrage>> iter = errors.iterator();

		final Set<String> properties = new HashSet<>();

		while (iter.hasNext()) {
			final ConstraintViolation<Registrierungsanfrage> cv = iter.next();
			properties.add(cv.getPropertyPath().toString());
			LOG.info(cv.getMessage());
		}

		assertEquals(1, properties.size());
		assertTrue(properties.contains("email"));
	}

	@Test
	@DisplayName("fails when email too long")
	public void validateWmailTooLong() {
		// Arrange
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();
		final Registrierungsanfrage anfrage = createValid();
		final String suffix = "@lange-lange-domaine.com";
		final StringBuffer sb = new StringBuffer();
		for (int i = 0; i < 231; i++) {
			sb.append("a");
		}
		anfrage.setEmail(sb.toString() + suffix);
		assertEquals("Testsetting", 255, anfrage.getEmail().length());

		// Act
		final Set<ConstraintViolation<Registrierungsanfrage>> errors = validator.validate(anfrage);

		// Assert
		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final Iterator<ConstraintViolation<Registrierungsanfrage>> iter = errors.iterator();

		final Set<String> properties = new HashSet<>();

		while (iter.hasNext()) {
			final ConstraintViolation<Registrierungsanfrage> cv = iter.next();
			properties.add(cv.getPropertyPath().toString());
			LOG.info(cv.getMessage());
		}

		assertEquals(1, properties.size());
		assertTrue(properties.contains("email"));
	}

	@Test
	public void validate_fails_when_anwendung_role_not_consistent() {
		// Arrange
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();
		final Registrierungsanfrage anfrage = createValid();
		anfrage.setAnwendung(Anwendung.BQ);
		anfrage.setRole(Role.MKV_PRIVAT);

		// Act
		final Set<ConstraintViolation<Registrierungsanfrage>> errors = validator.validate(anfrage);

		// Assert
		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final Iterator<ConstraintViolation<Registrierungsanfrage>> iter = errors.iterator();

		final Set<String> properties = new HashSet<>();

		while (iter.hasNext()) {
			final ConstraintViolation<Registrierungsanfrage> cv = iter.next();
			properties.add(cv.getPropertyPath().toString());
			LOG.info(cv.getMessage());
		}

		assertEquals(1, properties.size());
	}

	private Registrierungsanfrage createValid() {
		final Registrierungsanfrage anfrage = new Registrierungsanfrage("Valider Loginname", "Qwertz!2", "mail@provider.com",
			Role.MKV_LEHRER, Anwendung.MKV);
		return anfrage;
	}

}
