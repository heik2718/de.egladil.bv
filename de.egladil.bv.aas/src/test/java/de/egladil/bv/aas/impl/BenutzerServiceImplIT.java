//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.google.inject.Inject;
import com.google.inject.persist.Transactional;

import de.egladil.bv.aas.AbstractGuiceIT;
import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.storage.IAktivierungDao;
import de.egladil.common.exception.UnknownAccountException;

/**
 * BenutzerServiceImplIT
 */
public class BenutzerServiceImplIT extends AbstractGuiceIT {

	@Inject
	private IAktivierungDao aktivierungDao;

	@Inject
	private IBenutzerService benutzerService;

	@Test
	@DisplayName("generateResetPasswordCode klappt")
	@Transactional
	public void test1() {
		// Arrange
		final String email = "ingo@egladil.de";
		final Anwendung anwendung = Anwendung.MKV;

		// Act
		final Aktivierungsdaten aktivierungsdaten = benutzerService.generateResetPasswordCode(email, anwendung, 10);

		// Assert
		assertNotNull(aktivierungsdaten);
		final Aktivierungsdaten persistente = aktivierungDao.findByConfirmationCode(aktivierungsdaten.getConfirmationCode());
		final Benutzerkonto benutzerkonto = persistente.getBenutzerkonto();
		assertEquals(email, benutzerkonto.getLoginName());
		assertEquals(email, benutzerkonto.getEmail());
	}

	@Test
	@DisplayName("generateResetPasswordCode fails when email unknown")
	@Transactional
	public void test2() {
		// Arrange
		final String email = "z@z.de";
		final Anwendung anwendung = Anwendung.MKV;

		final Throwable exception = assertThrows(UnknownAccountException.class, () -> {
			benutzerService.generateResetPasswordCode(email, anwendung, 10);
		});
		assertEquals("Benutzerkonto nicht gefunden", exception.getMessage());
	}
}
