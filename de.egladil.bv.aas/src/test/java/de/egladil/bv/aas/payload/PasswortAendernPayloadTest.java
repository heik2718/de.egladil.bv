//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.payload;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.common.persistence.utils.PrettyStringUtils;

/**
 * PasswortAendernAnfrageTest
 */
public class PasswortAendernPayloadTest {

	private static final Logger LOG = LoggerFactory.getLogger(PasswortAendernPayloadTest.class);

	private Validator validator;

	private PasswortAendernPayload payload;

	@BeforeEach
	public void setUp() {
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		validator = validatorFactory.getValidator();
		payload = validPasswortAendernAnfrage();
	}

	private PasswortAendernPayload validPasswortAendernAnfrage() {
		final PasswortAendernPayload result = new PasswortAendernPayload();
		result.setEmail("mail@provider.com");
		result.setKleber("");
		result.setPasswort("start 123");
		result.setPasswortNeu("start987");
		result.setPasswortNeuWdh("start987");
		return result;
	}

	@Test
	@DisplayName("validate passes when valid")
	public void validateValid() {
		final Set<ConstraintViolation<PasswortAendernPayload>> errors = validator.validate(payload);

		assertTrue(errors.isEmpty());
	}

	@Test
	@DisplayName("validate fails when kleber Leerzeichen")
	public void validateKleberLeerzeichen() {
		// Arrange

		payload.setKleber(" ");

		// Act + Assert
		final Set<ConstraintViolation<PasswortAendernPayload>> errors = validator.validate(payload);

		assertEquals(1, errors.size());

		final ConstraintViolation<PasswortAendernPayload> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("kleber", cv.getPropertyPath().toString());
	}

	@Test
	@DisplayName("validate fails when kleber nicht leer")
	public void validateKleberNichtLeer() {
		// Arrange

		payload.setKleber("h");

		// Act + Assert
		final Set<ConstraintViolation<PasswortAendernPayload>> errors = validator.validate(payload);

		assertEquals(1, errors.size());

		final ConstraintViolation<PasswortAendernPayload> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("kleber", cv.getPropertyPath().toString());
	}

	@Test
	@DisplayName("validate fails when password invalid")
	public void validatePasswordInvalid() {
		// Arrange

		payload.setPasswort("Graf <KOKS>ч1");

		// Act + Assert
		final Set<ConstraintViolation<PasswortAendernPayload>> errors = validator.validate(payload);

		assertEquals(1, errors.size());

		final ConstraintViolation<PasswortAendernPayload> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("passwort", cv.getPropertyPath().toString());
	}

	@Test
	@DisplayName("validate fails when passwordNeu invalid")
	public void validatePasswordNeuInvalid() {
		// Arrange
		payload.setPasswortNeu("Graf <KOKS>1ч");
		payload.setPasswortNeuWdh("Graf <KOKS>1ч");

		// Act + Assert
		final Set<ConstraintViolation<PasswortAendernPayload>> errors = validator.validate(payload);

		assertEquals(2, errors.size());

		final Set<String> propertyNames = new HashSet<>();

		final Iterator<ConstraintViolation<PasswortAendernPayload>> iter = errors.iterator();

		while (iter.hasNext()) {
			final ConstraintViolation<PasswortAendernPayload> cv = iter.next();
			LOG.debug(cv.getMessage());
			propertyNames.add(cv.getPropertyPath().toString());
		}

		assertEquals(2, propertyNames.size());
		assertTrue(propertyNames.contains("passwortNeu"));
		assertTrue(propertyNames.contains("passwortNeuWdh"));
	}

	@Test
	@DisplayName("validate fails when passwordNeu null")
	public void validatePasswordNeuNullull() {
		// Arrange

		payload.setPasswortNeu(null);

		// Act + Assert
		final Set<ConstraintViolation<PasswortAendernPayload>> errors = validator.validate(payload);

		assertEquals(1, errors.size());

		final ConstraintViolation<PasswortAendernPayload> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("passwortNeu", cv.getPropertyPath().toString());
	}

	@Test
	@DisplayName("validate fails when password null")
	public void validatePasswordNull() {
		// Arrange

		payload.setPasswort(null);

		// Act + Assert
		final Set<ConstraintViolation<PasswortAendernPayload>> errors = validator.validate(payload);

		assertEquals(1, errors.size());

		final ConstraintViolation<PasswortAendernPayload> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("passwort", cv.getPropertyPath().toString());
	}

	@Test
	@DisplayName("validate fails when passwortNeuWdg null")
	public void validatePasswordNeuWdhNull() {
		// Arrange

		payload.setPasswortNeuWdh(null);

		// Act + Assert
		final Set<ConstraintViolation<PasswortAendernPayload>> errors = validator.validate(payload);

		assertEquals(2, errors.size());

		final Set<String> propertyNames = new HashSet<>();

		final Iterator<ConstraintViolation<PasswortAendernPayload>> iter = errors.iterator();

		while (iter.hasNext()) {
			final ConstraintViolation<PasswortAendernPayload> cv = iter.next();
			LOG.debug(cv.getMessage());
			propertyNames.add(cv.getPropertyPath().toString());
		}

		LOG.info(PrettyStringUtils.collectionToDefaultString(propertyNames));
		assertEquals(2, propertyNames.size());
		assertTrue(propertyNames.contains("passwortNeuWdh"));
	}

	@Test
	@DisplayName("validate fails when passwortNeu ungleich passwordNeuWdh")
	public void validatePasswordNeuPasswordNeuWdh() {
		// Arrange
		payload.setPasswortNeu("Graf<KOKS>1");
		payload.setPasswortNeuWdh("Graf<KOKS>2");

		// Act + Assert
		final Set<ConstraintViolation<PasswortAendernPayload>> errors = validator.validate(payload);

		assertEquals(1, errors.size());

		final ConstraintViolation<PasswortAendernPayload> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
	}

	@Test
	@DisplayName("validate fails when passwortNeu gleich password")
	public void validatePasswordPasswordNeu() {
		// Arrange
		payload.setPasswortNeu(payload.getPasswort());
		payload.setPasswortNeuWdh(payload.getPasswort());

		// Act + Assert
		final Set<ConstraintViolation<PasswortAendernPayload>> errors = validator.validate(payload);

		assertEquals(1, errors.size());

		final ConstraintViolation<PasswortAendernPayload> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
	}
}
