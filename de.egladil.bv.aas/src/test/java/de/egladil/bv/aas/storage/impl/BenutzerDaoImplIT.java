//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.storage.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import javax.persistence.PersistenceException;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.persist.Transactional;

import de.egladil.bv.aas.AbstractGuiceIT;
import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.domain.Role;
import de.egladil.bv.aas.storage.IBenutzerDao;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.PersistenceExceptionMapper;

/**
 * BenutzerDaoTest
 */
public class BenutzerDaoImplIT extends AbstractGuiceIT {

	private static final Logger LOG = LoggerFactory.getLogger(BenutzerDaoImplIT.class);

	@Inject
	private IBenutzerDao benutzerDao;

	@Test
	@DisplayName("should load_benutzer by correct loginname")
	@Transactional
	public void findBenutzerByLoginNameFound() {
		// Arrange
		final String loginName = "heike1";

		// Act
		final Benutzerkonto benutzerkonto = benutzerDao.findBenutzerByLoginName(loginName, Anwendung.BQ);

		// Assert
		assertNotNull(benutzerkonto);
		assertNotNull(benutzerkonto.rollenAsRoleArray());

		final Role[] roles = benutzerkonto.rollenAsRoleArray();
		for (final Role r : roles) {
			LOG.debug(r.toString());
		}
	}

	@Test
	@DisplayName("findBenutzerByUUID klappt")
	@Transactional
	public void findBenutzerByUUIDWorks() {
		// Arrange
		final String uuid = "a4f3d6a3-ca9d-490d-a63e-309f1451821d";

		// Act
		final Benutzerkonto benutzerkonto = benutzerDao.findBenutzerByUUID(uuid);

		// Assert
		assertNotNull(benutzerkonto);
		assertFalse(benutzerkonto.getRollen().isEmpty());
	}

	@Test
	@DisplayName("gibt null zurück, wenn uuid null")
	@Transactional
	public void findBenutzerByUUIDParameterNull() {
		// Arrange
		final String uuid = null;

		// Act
		final Benutzerkonto benutzerkonto = benutzerDao.findBenutzerByUUID(uuid);

		// Assert
		assertNull(benutzerkonto);
	}

	@Test
	@DisplayName("persist klappt")
	public void persistSuccess() {
		// Arrange
		final Benutzerkonto benutzerkonto = benutzerDao.findBenutzerByEmail("hoehoe@egladil.de", Anwendung.MKV);
		assertNotNull("Test kann nicht starten: kein Benutzerkonto vorhanden", benutzerkonto);
		final boolean aktiviert = benutzerkonto.isAktiviert();
		benutzerkonto.setAktiviert(!benutzerkonto.isAktiviert());

		// Act
		try {
			final Benutzerkonto persisted = benutzerDao.persist(benutzerkonto);
			// Assert
			assertEquals(!aktiviert, persisted.isAktiviert());
		} catch (final PersistenceException e) {
			final Optional<EgladilConcurrentModificationException> optOpt = PersistenceExceptionMapper
				.toEgladilConcurrentModificationException(e);
			if (optOpt.isPresent()) {
				// Klappt nur manuell mit Breakpoint und update version in der DB.
				LOG.info(optOpt.get().getMessage());
			}
		}
	}

	@Test
	public void findBenutzerByMostSignificantCharsOfUUID_klappt() {
		// Arrange
		final String chars = "ff741267";

		// Act
		final Optional<Benutzerkonto> opt = benutzerDao.findBenutzerByMostSignificantCharsOfUUID(chars);

		// Assert
		assertTrue(opt.isPresent());
	}
}
