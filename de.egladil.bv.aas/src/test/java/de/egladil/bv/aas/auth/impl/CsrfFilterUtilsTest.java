//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.auth.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

/**
 * CsrfFilterUtilsTest
 */
public class CsrfFilterUtilsTest {

	private static final List<String> OPEN_DATA_SUBPATHS = Arrays
		.asList(new String[] { "/registrierungen/lehrer/confirmation", "/files", "/wettbewerbe/**" });

	@Test
	void optionsCanPass() {
		// Arrange
		final String pathInfo = null;
		final String method = "OPTIONS";

		// Act + Assert
		assertTrue(CsrfFilterUtils.canPassWithoutCsrfToken(OPEN_DATA_SUBPATHS, pathInfo, method));
	}

	@Test
	void headCanPass() {
		// Arrange
		final String pathInfo = null;
		final String method = "HEAD";

		// Act + Assert
		assertTrue(CsrfFilterUtils.canPassWithoutCsrfToken(OPEN_DATA_SUBPATHS, pathInfo, method));
	}

	@Test
	void postMustNotPass() {
		// Arrange
		final String pathInfo = null;
		final String method = "POST";

		// Act + Assert
		assertFalse(CsrfFilterUtils.canPassWithoutCsrfToken(OPEN_DATA_SUBPATHS, pathInfo, method));
	}

	@Test
	void putMustNotPass() {
		// Arrange
		final String pathInfo = null;
		final String method = "PUT";

		// Act + Assert
		assertFalse(CsrfFilterUtils.canPassWithoutCsrfToken(OPEN_DATA_SUBPATHS, pathInfo, method));
	}

	@Test
	void deleteMustNotPass() {
		// Arrange
		final String pathInfo = null;
		final String method = "DELETE";

		// Act + Assert
		assertFalse(CsrfFilterUtils.canPassWithoutCsrfToken(OPEN_DATA_SUBPATHS, pathInfo, method));
	}

	@Test
	void methodNullThrows() {
		// Act + Assert
		final Throwable exception = assertThrows(IllegalArgumentException.class, () -> {
			CsrfFilterUtils.canPassWithoutCsrfToken(OPEN_DATA_SUBPATHS, "/", null);
		});
		assertEquals("method null", exception.getMessage());
	}

	@Test
	void pathInfoNullThrowsWhenMethodGET() {
		// Act + Assert
		final Throwable exception = assertThrows(IllegalArgumentException.class, () -> {
			CsrfFilterUtils.canPassWithoutCsrfToken(OPEN_DATA_SUBPATHS, null, "GET");
		});
		assertEquals("pathInfo null", exception.getMessage());
	}

	@Test
	void getMustNotPassWhenOpenDataUrlsNull() {
		// Arrange
		final String pathInfo = "/foo";
		final String method = "GET";

		// Act + Assert
		assertFalse(CsrfFilterUtils.canPassWithoutCsrfToken(null, pathInfo, method));
	}

	@Test
	void getPassesWithAbsolutePathInfo() {
		// Arrange
		final String pathInfo = "/files";
		final String method = "GET";

		// Act + Assert
		assertTrue(CsrfFilterUtils.canPassWithoutCsrfToken(OPEN_DATA_SUBPATHS, pathInfo, method));
	}

	@Test
	void getPassesWithPathParameters() {
		// Arrange
		final String pathInfo = "/wettbewerbe/2017/1";
		final String method = "GET";

		// Act + Assert
		assertTrue(CsrfFilterUtils.canPassWithoutCsrfToken(OPEN_DATA_SUBPATHS, pathInfo, method));
	}
}
