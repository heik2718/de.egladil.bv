//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


/**
 * AccessPeriodValidationTest
 */
public class PeriodCheckerTest {

	private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("ddMMyyyy hh:mm:ss:S");

	@Test
	@DisplayName("shall return false")
	public void test1() throws ParseException {
		// Arrange
		final long delayMilliseconds = 500;
		final Date lastLogin = SIMPLE_DATE_FORMAT.parse("15062016 21:34:13:1");
		final Date now = SIMPLE_DATE_FORMAT.parse("15062016 21:34:13:502");

		final PeriodChecker validator = new PeriodChecker();

		// Act
		final boolean result = validator.isPeriodLessEqualExpectedPeriod(lastLogin, now, delayMilliseconds);

		// Assert
		assertFalse(result);

	}

	@Test
	@DisplayName("shall return true when exact delay")
	public void test2() throws ParseException {
		// Arrange
		final int delayMilliseconds = 500;
		final Date lastLogin = SIMPLE_DATE_FORMAT.parse("15062016 21:34:13:1");
		final Date now = SIMPLE_DATE_FORMAT.parse("15062016 21:34:13:501");

		final PeriodChecker validator = new PeriodChecker();

		// Act
		final boolean result = validator.isPeriodLessEqualExpectedPeriod(lastLogin, now, delayMilliseconds);

		// Assert
		assertTrue(result);
	}
}
