//=====================================================
// Projekt: de.egladil.email.storage
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.storage.impl;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.google.inject.Inject;
import com.google.inject.persist.Transactional;

import de.egladil.bv.aas.AbstractGuiceIT;
import de.egladil.bv.aas.domain.MailqueueItem;
import de.egladil.bv.aas.domain.MailqueueItemStatus;
import de.egladil.bv.aas.storage.IMailqueueDao;

/**
 * MailsDaoImplTest
 */
public class MailsDaoImplIT extends AbstractGuiceIT {

	@Inject
	private IMailqueueDao mailsDao;

	@Test
	@DisplayName("persist klappt")
	@Transactional
	public void persistSuccess() throws InterruptedException {
		// Arrange
		final MailqueueItem mail = new MailqueueItem();
		mail.setRecipients("heike@egladil.de");
		mail.setSubject("Mailstoragetest");
		mail.setMailBody("Das ist eine Testmail");
		mail.setStatusmessage("noch nicht gesendet");
		mail.setStatus(MailqueueItemStatus.WAITING);

		// Act + Assert
		mailsDao.persist(mail);
	}

	@Test
	@DisplayName("findNextMails klappt")
	public void findNextMailsSuccess() {
		// Act
		final List<MailqueueItem> nextMails = mailsDao.findNextMails();

		// Assert
		assertNotNull(nextMails);
	}
}
