//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.validation;

import static org.junit.Assert.assertEquals;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * EnsureRoleValidatorTest
 */
public class EnsureRoleValidatorTest {

	private class TestObject {

		@EnsureRole(erlaubteRollen = { "MKV_ADMIN" })
		private final String adminString = "MKV_ADMIN";

		@EnsureRole(erlaubteRollen = { "MKV_PRIVAT", "MKV_LEHRER" })
		private final String invalidString = "BV_ADMIN";

		@EnsureRole(erlaubteRollen = { "MKV_ADMIN" })
		private final String emptyString = "";

		@EnsureRole(erlaubteRollen = { "MKV_LEHRER", "MKV_PRIVAT" })
		private final String nullString = null;

		/**
		 * Erzeugt eine Instanz von TestObject
		 */
		public TestObject() {
			super();
		}
	}

	@Test
	@DisplayName("fails when nicht erlaubte rolle")
	public void validateNichtErlaubt() {
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();

		// Act
		final Set<ConstraintViolation<TestObject>> errors = validator.validate(new TestObject());

		// Assert
		assertEquals(1, errors.size());
		final ConstraintViolation<TestObject> cv = errors.iterator().next();
		final Object invalidValue = cv.getInvalidValue();
		assertEquals("BV_ADMIN", invalidValue);
	}
}
