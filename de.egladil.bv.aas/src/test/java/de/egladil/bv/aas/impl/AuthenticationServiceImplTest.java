//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.storage.IBenutzerDao;
import de.egladil.common.config.OsUtils;
import de.egladil.crypto.provider.IEgladilCryptoUtils;
import de.egladil.crypto.provider.impl.EgladilCryptoUtilsImpl;

/**
 * AuthenticationServiceTest
 */
public class AuthenticationServiceImplTest {

	private IBenutzerDao benutzerDao;

	private AuthenticationServiceImpl authenticationService;

	private AccessFrequencyGuard accessFrequencyGuard;

	@BeforeEach
	public void setUp() {
		final IEgladilCryptoUtils cryptoUtils = new EgladilCryptoUtilsImpl(OsUtils.getDevConfigRoot());
		benutzerDao = Mockito.mock(IBenutzerDao.class);
		accessFrequencyGuard = Mockito.mock(AccessFrequencyGuard.class);
		authenticationService = new AuthenticationServiceImpl(benutzerDao, accessFrequencyGuard, cryptoUtils);
	}

	@Nested
	@DisplayName("testen authenticateWithTemporaryPasswort")
	class VerifyTempPassword {
		@Test
		@DisplayName("throws when anwendung null")
		public void test1() {
			final Throwable exception = assertThrows(IllegalArgumentException.class, () -> {
				authenticationService.authenticateWithTemporaryPasswort(null, "huhu@egladil.de", "hdget7Z5");
			});
			assertEquals("anwendung null", exception.getMessage());
		}

		@Test
		@DisplayName("throws when email null")
		public void test2() {
			final Throwable exception = assertThrows(IllegalArgumentException.class, () -> {
				authenticationService.authenticateWithTemporaryPasswort(Anwendung.MKV, null, "hdget7Z5");
			});
			assertEquals("email null", exception.getMessage());
		}

		@Test
		@DisplayName("throws when tempPassword null")
		public void test3() {
			final Throwable exception = assertThrows(IllegalArgumentException.class, () -> {
				authenticationService.authenticateWithTemporaryPasswort(Anwendung.MKV, "huhu@egladil.de", null);
			});
			assertEquals("password null", exception.getMessage());
		}
	}

	@Nested
	@DisplayName("testen changePassword")
	class ChangePassword {
		@Test
		@DisplayName("throws when anwendung null")
		public void test1() {
			final Throwable exception = assertThrows(IllegalArgumentException.class, () -> {
				authenticationService.changePasswort(null, "sghasga", "sahgfuiag3".toCharArray());
			});
			assertEquals("anwendung", exception.getMessage());
		}

		@Test
		@DisplayName("throws when benutzerUuid null")
		public void test2() {
			final Throwable exception = assertThrows(IllegalArgumentException.class, () -> {
				authenticationService.changePasswort(Anwendung.MKV, null, "sahgfuiag3".toCharArray());
			});
			assertEquals("benutzerUuid", exception.getMessage());
		}

		@Test
		@DisplayName("throws when passwordNeu null")
		public void test3() {
			final Throwable exception = assertThrows(IllegalArgumentException.class, () -> {
				authenticationService.changePasswort(Anwendung.MKV, "sghasga", null);
			});
			assertEquals("pwdNeu", exception.getMessage());
		}
	}

	@Test
	@DisplayName("authenticateWithTemporaryPasswort calls check frequency")
	public void authenticateWithTemporaryPasswort() {
		// Arrange
		final Benutzerkonto benutzerkonto = new Benutzerkonto();
		benutzerkonto.setAktiviert(true);
		benutzerkonto.setEmail("heike1@egladil.de");
		benutzerkonto.setUuid("gajgg-qgdg78");

		final String confirmCode = "haagq6sd";

		final Aktivierungsdaten aktivierungsdaten = new Aktivierungsdaten();
		aktivierungsdaten.setConfirmationCode(confirmCode);
		benutzerkonto.addAktivierungsdaten(aktivierungsdaten);

		Mockito.when(benutzerDao.findBenutzerByEmail(benutzerkonto.getEmail(), Anwendung.BQ)).thenReturn(benutzerkonto);

		// Act
		authenticationService.authenticateWithTemporaryPasswort(Anwendung.BQ, benutzerkonto.getEmail(), confirmCode);

		// Assert
		Mockito.verify(accessFrequencyGuard, Mockito.times(1)).checkFrequency(benutzerkonto, 400);
	}
}
