//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.impl;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.UUID;

import javax.validation.ConstraintViolationException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.domain.Role;
import de.egladil.bv.aas.domain.UniqueIdentifier;
import de.egladil.bv.aas.payload.Registrierungsanfrage;
import de.egladil.bv.aas.storage.IAktivierungDao;
import de.egladil.bv.aas.storage.IBenutzerDao;
import de.egladil.bv.aas.storage.IRollenDao;
import de.egladil.common.config.OsUtils;
import de.egladil.common.validation.EgladilValidationDelegate;
import de.egladil.crypto.provider.IEgladilCryptoUtils;
import de.egladil.crypto.provider.impl.EgladilCryptoUtilsImpl;

/**
 * RegistrierungServiceTest
 */
public class RegistrierungServiceImplTest {

	class EgladilValidationDelegateWrapper extends EgladilValidationDelegate {
		private final EgladilValidationDelegate wrappedObject;

		private boolean checkCalled;

		public EgladilValidationDelegateWrapper(final EgladilValidationDelegate wrappedObject) {
			super();
			this.wrappedObject = wrappedObject;
		}

		@Override
		public <T> void check(final T object) {
			checkCalled = true;
			wrappedObject.check(object);
		}

		/**
		 * Liefert die Membervariable validateCalled
		 *
		 * @return die Membervariable validateCalled
		 */
		boolean isCheckCalled() {
			return checkCalled;
		}

		/**
		 * Setzt die Membervariable
		 *
		 * @param validateCalled neuer Wert der Membervariablen validateCalled
		 */
		void setCheckCalled(final boolean validateCalled) {
			this.checkCalled = validateCalled;
		}

	}

	private IAktivierungDao regDao;

	private IBenutzerDao benutzerDao;

	private RegistrierungServiceImpl service;

	private EgladilValidationDelegateWrapper wrapper;

	@BeforeEach
	public void setUp() {
		regDao = Mockito.mock(IAktivierungDao.class);
		benutzerDao = Mockito.mock(IBenutzerDao.class);
		final IRollenDao rollenDao = Mockito.mock(IRollenDao.class);
		final IEgladilCryptoUtils cryptoUtils = new EgladilCryptoUtilsImpl(OsUtils.getDevConfigRoot());
		service = new RegistrierungServiceImpl(regDao, benutzerDao, rollenDao, cryptoUtils);
		wrapper = new EgladilValidationDelegateWrapper(new EgladilValidationDelegate());
		wrapper.setCheckCalled(false);
		service.setValidationDelegate(wrapper);
	}

	@Test
	@DisplayName("register fails early when object null")
	public void test1() {
		// Act
		try {
			service.register(null);
			fail("keine IllegalArgumentException");
		} catch (final IllegalArgumentException e) {
			assertTrue(wrapper.checkCalled);
		}
	}

	@Test
	@DisplayName("register fails early when object invalid")
	public void test2() {
		// Arrange
		final Registrierungsanfrage anfrage = new Registrierungsanfrage("iche", "invalidPassword", "valid@email.com",
			Role.MKV_LEHRER, Anwendung.MKV);

		// Act
		try {
			service.register(anfrage);
			fail("keine ConstraintViolationException");
		} catch (final ConstraintViolationException e) {
			assertTrue(wrapper.checkCalled);
		}
	}

	@Test
	@DisplayName("confirm fails early when parameter null")
	public void test3() {
		// Act
		try {
			service.activateBenutzer(null);
			fail("keine IllegalArgumentException");
		} catch (final IllegalArgumentException e) {
			assertTrue(wrapper.checkCalled);
		}
	}

	@Test
	@DisplayName("confirm fails early when parameter invalid")
	public void test4() {
		// Act
		final String uuid = "Mno-857-960!";
		final Aktivierungsdaten regBest = new Aktivierungsdaten();
		regBest.setConfirmationCode(uuid);
		regBest.setBenutzerkonto(new Benutzerkonto());

		try {
			service.activateBenutzer(regBest);
			fail("keine ConstraintViolationException");
		} catch (final ConstraintViolationException e) {
			assertTrue(wrapper.checkCalled);
		}
	}

	@Test
	@DisplayName("confirm fails early when parameter invalid with generated uuid")
	public void test5() {
		// Act
		final Aktivierungsdaten regBest = new Aktivierungsdaten();
		regBest.setConfirmationCode(UUID.randomUUID().toString());

		try {
			service.activateBenutzer(regBest);
			fail("keine ConstraintViolationException");
		} catch (final ConstraintViolationException e) {
			assertTrue(wrapper.checkCalled);
		}
	}

	@Test
	@DisplayName("findByConfirmationCode fails early when parameter null")
	public void test6() {
		// Act
		try {
			service.findByConfirmationCode(null);
			fail("keine IllegalArgumentException");
		} catch (final IllegalArgumentException e) {
			assertTrue(wrapper.checkCalled);
		}
	}

	@Test
	@DisplayName("findByConfirmationCode fails early when parameter invalid")
	public void test7() {
		// Act
		final String uuid = "Mno-857-960/";
		try {
			service.findByConfirmationCode(new UniqueIdentifier(uuid));
			fail("keine ConstraintViolationException");
		} catch (final ConstraintViolationException e) {
			assertTrue(wrapper.checkCalled);
		}
	}
}
