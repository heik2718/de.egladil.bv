//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas;

import org.junit.jupiter.api.BeforeEach;

import com.google.inject.Guice;
import com.google.inject.Injector;

import de.egladil.common.config.OsUtils;

/**
 * @author heikew
 *
 */
public class AbstractGuiceIT {

	@BeforeEach
	public void setUp() {
		final String configRoot = OsUtils.getDevConfigRoot();
		final Injector injector = Guice.createInjector(new BVTestModule(configRoot));
		injector.injectMembers(this);
	}
}
