//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.domain.Role;
import de.egladil.bv.aas.storage.IBenutzerDao;
import de.egladil.common.config.OsUtils;
import de.egladil.crypto.provider.IEgladilCryptoUtils;
import de.egladil.crypto.provider.impl.EgladilCryptoUtilsImpl;

/**
 * BenutzerServiceImplTest
 */
public class BenutzerServiceImplTest {

	private IBenutzerDao benutzerDao;

	private BenutzerServiceImpl benutzerService;

	private AccessFrequencyGuard accessFrequencyGuard;

	@BeforeEach
	public void setUp() {
		benutzerDao = Mockito.mock(IBenutzerDao.class);
		accessFrequencyGuard = Mockito.mock(AccessFrequencyGuard.class);
		final IEgladilCryptoUtils cryptoUtils = new EgladilCryptoUtilsImpl(OsUtils.getDevConfigRoot());
		benutzerService = new BenutzerServiceImpl(benutzerDao, accessFrequencyGuard, cryptoUtils);
	}

	@Nested
	@DisplayName("testen generateResetPassworCode")
	class GenerateResetPasswordCode {
		@Test
		@DisplayName("throws IllegalArgumentException when email null")
		public void test1() {
			final Throwable exception = assertThrows(IllegalArgumentException.class, () -> {
				benutzerService.generateResetPasswordCode(null, Anwendung.MKV, 30);
			});
			assertEquals("email null", exception.getMessage());
		}

		@Test
		@DisplayName("throws IllegalArgumentException when anwendung null")
		public void test2() {
			final Throwable exception = assertThrows(IllegalArgumentException.class, () -> {
				benutzerService.generateResetPasswordCode("z@z.de", null, 30);
			});
			assertEquals("anwendung null", exception.getMessage());
		}

		@Test
		@DisplayName("calls check frequency")
		public void test3() {
			// Arrange
			final Benutzerkonto benutzerkonto = new Benutzerkonto();
			benutzerkonto.setAktiviert(true);
			benutzerkonto.setEmail("heike1@egladil.de");
			benutzerkonto.setUuid("gajgg-qgdg78");
			Mockito.when(benutzerDao.findBenutzerByEmail(benutzerkonto.getEmail(), Anwendung.BQ)).thenReturn(benutzerkonto);
			Mockito.when(benutzerDao.persist(benutzerkonto)).thenReturn(benutzerkonto);
			Mockito.when(accessFrequencyGuard.checkFrequency(benutzerkonto, 400)).thenReturn(benutzerkonto);

			// Act
			benutzerService.generateResetPasswordCode(benutzerkonto.getEmail(), Anwendung.BQ, 30);

			// Assert
			Mockito.verify(accessFrequencyGuard, Mockito.times(1)).checkFrequency(benutzerkonto, 400);
		}
	}

	@Nested
	@DisplayName("testen isBenutzerInRole")
	class IsBenutzerInRole {
		@Test
		@DisplayName("throws IllegalArgumentException when role null")
		public void test1() {
			final Throwable exception = assertThrows(IllegalArgumentException.class, () -> {
				benutzerService.isBenutzerInRole(null, "gsqgi");
			});
			assertEquals("role null", exception.getMessage());
		}
		@Test
		@DisplayName("throws IllegalArgumentException when uuid null")
		public void test2() {
			final Throwable exception = assertThrows(IllegalArgumentException.class, () -> {
				benutzerService.isBenutzerInRole(Role.MKV_ADMIN, null);
			});
			assertEquals("principalName null", exception.getMessage());
		}

		@Test
		@DisplayName("returns false when uuid unknown")
		public void zesz3() {
			Mockito.when(benutzerDao.findBenutzerByUUID("UUUFFF")).thenReturn(null);
			assertFalse(benutzerService.isBenutzerInRole(Role.MKV_ADMIN, "UUUFFF"));
		}
	}


}
