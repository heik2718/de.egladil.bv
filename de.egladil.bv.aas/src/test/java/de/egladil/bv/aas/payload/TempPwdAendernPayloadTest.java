//=====================================================
// Projekt: de.egladil.common.webapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.payload;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TempPwdAendernPayloadTest.<br>
 * <br>
 * Es werden nur noch die Teile mit der Email und dem tempPassword getestet. kleber, passwordNeu, passwortNeuWdh sind
 * bereits durch Vererbung und PasswortAendernAnfrageTest abgesichert.
 */
public class TempPwdAendernPayloadTest {

	private static final Logger LOG = LoggerFactory.getLogger(TempPwdAendernPayloadTest.class);

	private Validator validator;

	private TempPwdAendernPayload payload;

	@BeforeEach
	public void setUp() {
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		validator = validatorFactory.getValidator();
		payload = validPasswortNeuEmailAnfrage();
	}

	private TempPwdAendernPayload validPasswortNeuEmailAnfrage() {
		final TempPwdAendernPayload result = new TempPwdAendernPayload();
		result.setKleber("");
		result.setEmail("luke@sky.com");
		result.setPasswortNeu("start 987");
		result.setPasswortNeuWdh("start 987");
		result.setPasswort("635Grfkhu");
		return result;
	}

	@Test
	@DisplayName("validation passes when valid")
	public void validateValid() {
		final Set<ConstraintViolation<TempPwdAendernPayload>> errors = validator.validate(payload);
		assertTrue(errors.isEmpty());
	}

	@Test
	@DisplayName("validation fails when email null")
	public void validateEmailNull() {
		// Arrange

		payload.setEmail(null);

		// Act + Assert
		final Set<ConstraintViolation<TempPwdAendernPayload>> errors = validator.validate(payload);

		assertEquals(1, errors.size());

		final ConstraintViolation<TempPwdAendernPayload> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("email", cv.getPropertyPath().toString());
	}

	@Test
	@DisplayName("validation fails when email invalid")
	public void validateEmailInvalid() {
		// Arrange
		payload.setEmail("www");

		// Act + Assert
		final Set<ConstraintViolation<TempPwdAendernPayload>> errors = validator.validate(payload);

		assertEquals(2, errors.size());

		final Set<String> propertyNames = new HashSet<>();
		final Iterator<ConstraintViolation<TempPwdAendernPayload>> iter = errors.iterator();

		while (iter.hasNext()) {
			final ConstraintViolation<TempPwdAendernPayload> cv = iter.next();
			LOG.debug(cv.getMessage());
			propertyNames.add(cv.getPropertyPath().toString());
		}

		assertEquals(1, propertyNames.size());
		assertEquals("email", propertyNames.iterator().next());
	}

	@Test
	@DisplayName("validation fails when email too long")
	public void validateEmailTooLong() {
		// Arrange
		final StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 250; i++) {
			sb.append("a");
		}
		final String email = sb.toString() + "@web.de";
		assertEquals("Testsetting: wollen 257 Zeichen", 257, email.length());

		payload.setEmail(email);

		// Act + Assert
		final Set<ConstraintViolation<TempPwdAendernPayload>> errors = validator.validate(payload);

		assertEquals(1, errors.size());

		final ConstraintViolation<TempPwdAendernPayload> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("email", cv.getPropertyPath().toString());
	}

	@Test
	@DisplayName("validation fails when tempPasswd invalid")
	public void validateTempPasswordinvalid() {
		// Arrange
		payload.setPasswort("Qwertz!289");

		// Act + Assert
		final Set<ConstraintViolation<TempPwdAendernPayload>> errors = validator.validate(payload);

		assertEquals(1, errors.size());

		final ConstraintViolation<TempPwdAendernPayload> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("passwort", cv.getPropertyPath().toString());
	}

	@Test
	@DisplayName("validation fails when tempPasswd too long")
	public void validateTempPasswdTooLong() {
		// Arrange
		final StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 41; i++) {
			sb.append("a");
		}
		final String tempPwd = sb.toString();
		assertEquals("Testsetting: wollen 41 Zeichen", 41, tempPwd.length());

		payload.setPasswort(tempPwd);

		// Act + Assert
		final Set<ConstraintViolation<TempPwdAendernPayload>> errors = validator.validate(payload);

		assertEquals(1, errors.size());

		final ConstraintViolation<TempPwdAendernPayload> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("passwort", cv.getPropertyPath().toString());
	}

	@Test
	@DisplayName("validation fails when tempPasswd blank")
	public void validateTempPasswdBlank() {
		// Arrange
		payload.setPasswort(" ");

		// Act + Assert
		final Set<ConstraintViolation<TempPwdAendernPayload>> errors = validator.validate(payload);

		assertEquals(1, errors.size());

		final ConstraintViolation<TempPwdAendernPayload> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("passwort", cv.getPropertyPath().toString());
	}

	@Test
	@DisplayName("validation fails when tempPasswd null")
	public void validateTempPasswdNull() {
		// Arrange
		payload.setPasswort(null);

		// Act + Assert
		final Set<ConstraintViolation<TempPwdAendernPayload>> errors = validator.validate(payload);

		assertEquals(1, errors.size());

		final ConstraintViolation<TempPwdAendernPayload> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("passwort", cv.getPropertyPath().toString());
	}

	@Test
	@DisplayName("validation fails when password neu gleich tempPasswd")
	public void validatePasswordNeuEqualsTempPassword() {
		// Arrange
		payload.setPasswortNeu(payload.getPasswort());
		payload.setPasswortNeuWdh(payload.getPasswort());

		// Act + Assert
		final Set<ConstraintViolation<TempPwdAendernPayload>> errors = validator.validate(payload);

		assertEquals(1, errors.size());

		final ConstraintViolation<TempPwdAendernPayload> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
	}
}
