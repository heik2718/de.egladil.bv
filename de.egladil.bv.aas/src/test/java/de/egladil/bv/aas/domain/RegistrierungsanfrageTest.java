//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.bv.aas.payload.Registrierungsanfrage;

/**
 * RegistrierungsanfrageTest
 */
public class RegistrierungsanfrageTest {

	private static final Logger LOG = LoggerFactory.getLogger(RegistrierungsanfrageTest.class);

	@Test
	@DisplayName("fails when loginname null")
	public void test1() {
		// Arrange
		final Registrierungsanfrage anfrage = completeAnfrage();
		anfrage.setLoginName(null);

		// Act + Assert
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();
		final Set<ConstraintViolation<Registrierungsanfrage>> errors = validator.validate(anfrage);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<Registrierungsanfrage> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("loginName", cv.getPropertyPath().toString());
	}

	@Test
	@DisplayName("fails when loginname blank")
	public void test2() {
		// Arrange
		final Registrierungsanfrage anfrage = completeAnfrage();
		anfrage.setLoginName(" ");

		// Act + Assert
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();
		final Set<ConstraintViolation<Registrierungsanfrage>> errors = validator.validate(anfrage);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<Registrierungsanfrage> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("loginName", cv.getPropertyPath().toString());
	}

	@Test
	@DisplayName("fails when loginname too long")
	public void test3() {
		// Arrange
		final Registrierungsanfrage anfrage = completeAnfrage();
		anfrage.setLoginName("ptionHV000183UnabletoloadjavaxelExpressionFactoryCheckthatyouhavetheELdependenciesontheclasspathoruse");

		assertEquals(101, anfrage.getLoginName().length());

		// Act + Assert
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();
		final Set<ConstraintViolation<Registrierungsanfrage>> errors = validator.validate(anfrage);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<Registrierungsanfrage> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("loginName", cv.getPropertyPath().toString());
	}

	@Test
	@DisplayName("fails when email null")
	public void test4() {
		// Arrange
		final Registrierungsanfrage anfrage = completeAnfrage();
		anfrage.setEmail(null);

		// Act + Assert
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();
		final Set<ConstraintViolation<Registrierungsanfrage>> errors = validator.validate(anfrage);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<Registrierungsanfrage> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("email", cv.getPropertyPath().toString());
	}

	@Test
	@DisplayName("fails when loginname invalid")
	public void test5() {
		// Arrange
		final Registrierungsanfrage anfrage = completeAnfrage();
		anfrage.setEmail("boese-emailadresse");

		// Act + Assert
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();
		final Set<ConstraintViolation<Registrierungsanfrage>> errors = validator.validate(anfrage);

		assertFalse(errors.isEmpty());
		assertEquals(2, errors.size());

		final ConstraintViolation<Registrierungsanfrage> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("email", cv.getPropertyPath().toString());
	}

	@Test
	@DisplayName("fails when role null")
	public void test6() {
		// Arrange
		final Registrierungsanfrage anfrage = completeAnfrage();
		anfrage.setRole(null);

		// Act + Assert
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();
		final Set<ConstraintViolation<Registrierungsanfrage>> errors = validator.validate(anfrage);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<Registrierungsanfrage> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("role", cv.getPropertyPath().toString());
	}

	@Test
	@DisplayName("fails when anwendung null")
	public void test7() {
		// Arrange
		final Registrierungsanfrage anfrage = completeAnfrage();
		anfrage.setAnwendung(null);

		// Act + Assert
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();
		final Set<ConstraintViolation<Registrierungsanfrage>> errors = validator.validate(anfrage);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<Registrierungsanfrage> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("anwendung", cv.getPropertyPath().toString());
	}

	@Test
	@DisplayName("fails when rolle und anwendung inkonsistent")
	public void test8() {
		// Arrange
		final Registrierungsanfrage anfrage = completeAnfrage();
		anfrage.setRole(Role.BQ_ADMIN);
		anfrage.setAnwendung(Anwendung.MKM);

		// Act + Assert
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();
		final Set<ConstraintViolation<Registrierungsanfrage>> errors = validator.validate(anfrage);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<Registrierungsanfrage> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());

	}

	@Test
	@DisplayName("passes when valid")
	public void validate_passes_when_valid() {
		// Arrange
		final Registrierungsanfrage anfrage = completeAnfrage();

		// Act + Assert
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();
		final Set<ConstraintViolation<Registrierungsanfrage>> errors = validator.validate(anfrage);

		assertTrue(errors.isEmpty());
	}

	/**
	 *
	 * @return
	 */
	private Registrierungsanfrage completeAnfrage() {
		return new Registrierungsanfrage("iche", "pwd3242Et mit längerem Passwort und Leerzeichen", "mail@provider.com", Role.BQ_AUTOR, Anwendung.BQ);
	}
}
