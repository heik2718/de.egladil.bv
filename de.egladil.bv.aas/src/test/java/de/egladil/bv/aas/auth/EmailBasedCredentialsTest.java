//=====================================================
// Projekt: de.egladil.mkm.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.auth;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author heikew
 *
 */
public class EmailBasedCredentialsTest {

	private static final Logger LOG = LoggerFactory.getLogger(EmailBasedCredentialsTest.class);

	@Test
	@DisplayName("validate passes when valid")
	public void validateValid() {
		// Arrange
		final EmailBasedCredentials credentials = completeCredentials();

		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();

		// Act
		final Set<ConstraintViolation<EmailBasedCredentials>> errors = validator.validate(credentials);

		// Assert
		assertTrue(errors.isEmpty());
	}

	@Test
	@DisplayName("validate fails when username null")
	public void validateUsernameNull() {
		// Arrange
		final EmailBasedCredentials credentials = completeCredentials();
		credentials.setUsername(null);

		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();

		// Act
		final Set<ConstraintViolation<EmailBasedCredentials>> errors = validator.validate(credentials);

		// Assert
		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<EmailBasedCredentials> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("username", cv.getPropertyPath().toString());
	}

	@Test
	@DisplayName("validate fails when username blank")
	public void validateUsernameBlank() {
		// Arrange
		final EmailBasedCredentials credentials = completeCredentials();
		credentials.setUsername(null);

		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();

		// Act
		final Set<ConstraintViolation<EmailBasedCredentials>> errors = validator.validate(credentials);

		// Assert
		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<EmailBasedCredentials> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("username", cv.getPropertyPath().toString());
	}

	@Test
	@DisplayName("validate fails when username too long")
	public void validateUsernameTooLong() {
		// Arrange
		final EmailBasedCredentials credentials = completeCredentials();
		final StringBuffer sb = new StringBuffer();
		for (int i = 0; i < 250; i++) {
			sb.append("a");
		}
		final String username = sb.toString() + "@pr.de";
		assertEquals("Test verhindert: brauchen 256 Zeichen", 256, username.length());
		credentials.setUsername(username);

		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();

		// Act
		final Set<ConstraintViolation<EmailBasedCredentials>> errors = validator.validate(credentials);

		// Assert
		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<EmailBasedCredentials> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("username", cv.getPropertyPath().toString());
	}

	@Test
	@DisplayName("validate fails when username keine Mailadresse")
	public void validateUsernameKeineMailadresse() {
		// Arrange
		final EmailBasedCredentials credentials = completeCredentials();
		credentials.setUsername("hallo");

		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();

		// Act
		final Set<ConstraintViolation<EmailBasedCredentials>> errors = validator.validate(credentials);

		// Assert
		assertFalse(errors.isEmpty());
		assertEquals(2, errors.size());

		final Set<String> properties = new HashSet<>();
		final Iterator<ConstraintViolation<EmailBasedCredentials>> iter = errors.iterator();
		while (iter.hasNext()) {
			final ConstraintViolation<EmailBasedCredentials> cv = iter.next();
			LOG.debug(cv.getMessage());
			properties.add(cv.getPropertyPath().toString());
		}
		assertEquals(1, properties.size());
		assertTrue(properties.contains("username"));

	}

	@Test
	@DisplayName("validate fails when password null")
	public void validatePasswordNull() {
		// Arrange
		final EmailBasedCredentials credentials = completeCredentials();
		credentials.setPassword(null);

		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();

		// Act
		final Set<ConstraintViolation<EmailBasedCredentials>> errors = validator.validate(credentials);

		// Assert
		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<EmailBasedCredentials> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("password", cv.getPropertyPath().toString());
	}

	private EmailBasedCredentials completeCredentials() {
		return new EmailBasedCredentials("mail@provider.de", "6eG 43drK", null);
	}
}
