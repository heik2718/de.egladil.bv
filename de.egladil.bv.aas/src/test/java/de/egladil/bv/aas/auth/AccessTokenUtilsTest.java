//=====================================================
// Projekt: de.egladil.mkm.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.auth;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.UUID;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import de.egladil.common.exception.EgladilBVException;

/**
 * @author heikew
 *
 */
public class AccessTokenUtilsTest {

	@Test
	@DisplayName("extractAccessTokenId schneidet Bearer- Präfix ab")
	public void extractAccessTokenIdMitBearer() {
		// Arrange
		final String accessTokenId = UUID.randomUUID().toString();
		final String headerParameter = "Bearer " + accessTokenId;
		final AccessTokenUtils utils = new AccessTokenUtils();

		// Act
		final String result = utils.extractAccessTokenId(headerParameter);

		// Assert
		assertEquals(accessTokenId, result);
	}

	@Test
	@DisplayName("extractAccessTokenId ändert nichts, wenn kein Bearer- Präfix vorhanden")
	public void extractAccessTokenIdOhneBearer() {
		// Arrange
		final String accessTokenId = UUID.randomUUID().toString();
		final String headerParameter = accessTokenId;
		final AccessTokenUtils utils = new AccessTokenUtils();

		// Act
		final String result = utils.extractAccessTokenId(headerParameter);

		// Assert
		assertEquals(accessTokenId, result);
	}

	@Test
	@DisplayName("extractAccessTokenId throws EgladilBVException when parameter null")
	public void extractAccessTokenIdParameterNull() {
		// Arrange
		final AccessTokenUtils utils = new AccessTokenUtils();

		// Act + Assert
		final Throwable exception = assertThrows(EgladilBVException.class, () -> {
			utils.extractAccessTokenId(null);
		});
		assertEquals("headerParameter war null", exception.getMessage());

	}
}
