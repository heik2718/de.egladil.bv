//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.aas.storage.impl;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.persist.Transactional;

import de.egladil.bv.aas.AbstractGuiceIT;
import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.bv.aas.storage.IAktivierungDao;

/**
 * @author heikew
 *
 */
public class AktivierungDaoImplIT extends AbstractGuiceIT {

	private static final Logger LOG = LoggerFactory.getLogger(AktivierungDaoImplIT.class);

	@Inject
	private IAktivierungDao dao;

	@Test
	@Transactional
	public void findByConfirmationCode_klappt() {
		// Arrange
		final String confirmationCode = "000f7c35-0825-4ed8-b685-d561a288871c";

		// Act
		final Aktivierungsdaten daten = dao.findByConfirmationCode(confirmationCode);

		LOG.info(daten == null ? "kein Treffer" : daten.toString());
	}
}
