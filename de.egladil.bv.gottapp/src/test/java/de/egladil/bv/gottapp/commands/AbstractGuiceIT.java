//=====================================================
// Projekt: de.egladil.bv.gottapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.gottapp.commands;

import org.junit.Before;

import com.google.inject.Guice;
import com.google.inject.Injector;

import de.egladil.bv.gottapp.di.GottAppInjector;
import de.egladil.common.config.OsUtils;

/**
 * AbstractGuiceIT
 */
public class AbstractGuiceIT {

	@Before
	public void setUp() {
		Injector injector = Guice.createInjector(new GottAppInjector(OsUtils.getDevConfigRoot()));
		injector.injectMembers(this);
	}

}
