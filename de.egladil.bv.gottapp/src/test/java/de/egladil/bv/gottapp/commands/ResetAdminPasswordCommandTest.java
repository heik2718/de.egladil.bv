//=====================================================
// Projekt: de.egladil.bv.gottapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.gottapp.commands;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.google.inject.Inject;

import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.domain.Berechtigung;
import de.egladil.bv.aas.domain.Role;
import de.egladil.bv.aas.domain.Rolle;
import de.egladil.bv.gottapp.jpa.IBerechtigungenDao;
import de.egladil.common.exception.EgladilAuthenticationException;

/**
 * CreateAdminCommandTest
 */
public class ResetAdminPasswordCommandTest extends AbstractGuiceIT {

	@Inject
	private ResetAdminPasswordCommand command;

	@Inject
	private IBerechtigungenDao berechtigungenDao;

	@Override
	@Before
	public void setUp() {
		super.setUp();

	}

	@Test
	public void should_find_the_admin_if_exists() {
		// Arrange
		final String login = "heike1";
		final Anwendung mandant = Anwendung.BQ;

		// Act 1
		final Benutzerkonto benutzer = command.checkBenutzer(login, mandant);

		// Assert 1
		assertNotNull(benutzer);
		assertEquals(login, benutzer.getLoginName());

		final Rolle rolle = berechtigungenDao.findRolleByRole(Role.BQ_ADMIN);

		final List<Berechtigung> berechtigungen = rolle.getBerechtigungen();
		assertFalse(berechtigungen.isEmpty());

	}

	@Test(expected = EgladilAuthenticationException.class)
	public void should_throw_exception_when_user_not_exists() {
		// Arrange
		final String login = "hurtzkrawel";
		final Anwendung mandant = Anwendung.BQ;
		// Act 1
		command.checkBenutzer(login, mandant);
	}

	@Test
	public void should_reset_admin_password() {
		// Arrange
		final String login = "heike1";
		final Anwendung mandant = Anwendung.BQ;

		// Act 1
		final char[] pwd = command.resetAdminPassword(login, mandant);

		// Assert 1
		assertNotNull(pwd);
		assertTrue(pwd.length > 0);
		System.out.println(new String(pwd));
	}
}
