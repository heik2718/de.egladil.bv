//=====================================================
// Projekt: de.egladil.bv.gottapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.gottapp.commands;

import static org.junit.Assert.assertNotNull;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import com.google.inject.Inject;

import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;

/**
 * CreateAdminCommandTest
 */
public class CreateAdminCommandTest extends AbstractGuiceIT {

	@Inject
	private CreateAdminCommand command;

	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void should_create_admin() {
		// Arrange
		String timestamp = new Date().getTime() + "";
		String login = "login" + timestamp;
		// String login = "heike1";
		String password = "hallo";
		String email = "mail." + timestamp + "@web.de";
		// String email = "mail@web.de";

		// Act 1
		Benutzerkonto persisted = command.createAdmin(login, email, Anwendung.BQ, password);

		// Assert 1
		assertNotNull(persisted);
		assertNotNull(persisted.getLoginName());
	}

	// @Test
	public void create_admin() {
		// Arrange
		String login = "heike4";
		// String login = "heike1";
		String password = "Qwertz!2";
		String email = "heike4@egladil.de";
		// String email = "mail@web.de";

		// Act 1
		Benutzerkonto persisted = command.createAdmin(login, email, Anwendung.MKM, password);

		// Assert 1
		assertNotNull(persisted);
		assertNotNull(persisted.getLoginName());
	}
}
