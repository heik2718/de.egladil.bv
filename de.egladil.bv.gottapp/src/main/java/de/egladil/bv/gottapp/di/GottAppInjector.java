//=====================================================
// Projekt: de.egladil.bv.gottapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.gottapp.di;

import java.util.HashMap;
import java.util.Map;

import com.google.inject.AbstractModule;
import com.google.inject.Binder;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Names;
import com.google.inject.persist.PersistService;
import com.google.inject.persist.jpa.JpaPersistModule;

import de.egladil.bv.gottapp.jpa.IAdminBenutzerDao;
import de.egladil.bv.gottapp.jpa.IBerechtigungenDao;
import de.egladil.bv.gottapp.jpa.impl.AdminBenutzerDao;
import de.egladil.bv.gottapp.jpa.impl.BerechtigungenDao;
import de.egladil.common.config.AbstractEgladilConfiguration;
import de.egladil.common.config.IEgladilConfiguration;
import de.egladil.crypto.provider.IEgladilCryptoUtils;
import de.egladil.crypto.provider.impl.EgladilCryptoUtilsImpl;

/**
 * GottAppInjector
 */
public class GottAppInjector extends AbstractModule {

	private final String pathConfigRoot;

	/**
	 * Erzeugt eine Instanz von GottAppInjector
	 */
	public GottAppInjector(String pathConfigRoot) {
		this.pathConfigRoot = pathConfigRoot;
	}

	/**
	 * @see com.google.inject.AbstractModule#configure()
	 */
	@Override
	protected void configure() {
		loadProperties(binder());

		install(createPersistModule());
		bind(JPAInitializer.class).asEagerSingleton();
		bind(IBerechtigungenDao.class).to(BerechtigungenDao.class);
		bind(IAdminBenutzerDao.class).to(AdminBenutzerDao.class);
		bind(IEgladilCryptoUtils.class).to(EgladilCryptoUtilsImpl.class);
	}

	@Singleton
	public static class JPAInitializer {

		@Inject
		public JPAInitializer(final PersistService service) {
			service.start();
		}

	}

	private JpaPersistModule createPersistModule() {
		IEgladilConfiguration persistenceConfig = new AbstractEgladilConfiguration(pathConfigRoot) {

			/* serialVersionUID */
			private static final long serialVersionUID = 1L;

			@Override
			protected String getConfigFileName() {
				return "bv_persistence.properties";
			}
		};
		JpaPersistModule jpaModule = new JpaPersistModule("de.egladil.bv.aas");
		jpaModule.properties(persistenceConfig.getConfigurationMap());
		return jpaModule;
	}

	protected void loadProperties(Binder binder) {
		Map<String, String> properties = new HashMap<>();
		properties.put("configRoot", pathConfigRoot);
		Names.bindProperties(binder, properties);
	}
}
