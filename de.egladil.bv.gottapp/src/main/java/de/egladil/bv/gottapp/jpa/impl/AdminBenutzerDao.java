//=====================================================
// Projekt: de.egladil.bv.gottapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.gottapp.jpa.impl;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;

import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.domain.Role;
import de.egladil.bv.aas.domain.Rolle;
import de.egladil.bv.gottapp.jpa.IAdminBenutzerDao;
import de.egladil.bv.gottapp.jpa.IBerechtigungenDao;
import de.egladil.common.persistence.EgladilStorageException;

/**
 * @author root
 *
 */
@Singleton
public class AdminBenutzerDao implements Serializable, IAdminBenutzerDao {

	private static final long serialVersionUID = -2;

	private Provider<EntityManager> emp;

	private final IBerechtigungenDao berechtigungenDao;

	@Inject
	public AdminBenutzerDao(Provider<EntityManager> emp, IBerechtigungenDao berechtigungenDao) {
		super();
		this.emp = emp;
		this.berechtigungenDao = berechtigungenDao;
	}

	/**
	 * @see de.egladil.bv.gottapp.jpa.IAdminBenutzerDao#createBenutzer(java.lang.String, java.lang.String,
	 * de.egladil.bv.aas.domain.Anwendung)
	 */
	@Override
	public Benutzerkonto createBenutzer(String loginName, String roleName, Anwendung anwendung) {
		Rolle r = berechtigungenDao.findRolleByRole(Role.valueOf(roleName));
		if (r == null) {
			throw new IllegalArgumentException("Kennen keine EnsureRole [" + roleName + "]");
		}
		Benutzerkonto benutzer = new Benutzerkonto();
		benutzer.setLoginName(loginName);
		benutzer.addRolle(r);
		benutzer.setAnwendung(anwendung);
		benutzer.setUuid(UUID.randomUUID().toString());
		return benutzer;
	}

	/**
	 * @see de.egladil.bv.gottapp.jpa.IAdminBenutzerDao#findBenutzerByLoginName(java.lang.String,
	 * de.egladil.bv.aas.domain.Anwendung)
	 */
	@Override
	public Benutzerkonto findBenutzerByLoginName(String loginName, Anwendung anwendung) {
		TypedQuery<Benutzerkonto> query = emp.get()
			.createQuery("from Benutzerkonto b where b.loginName = :loginName and b.anwendung = :anwendung", Benutzerkonto.class);
		query.setParameter("loginName", loginName);
		query.setParameter("anwendung", anwendung);

		List<Benutzerkonto> result = query.getResultList();
		if (result.isEmpty()) {
			return null;
		}
		if (result.size() > 1) {
			throw new EgladilStorageException("Benutzerkonto mit Loginname [" + loginName + "] und Anwendung [" + anwendung
				+ "]existiert mehr als einmal - kann eigentlich nicht sein");
		}

		return result.get(0);
	}

	/**
	 * @see de.egladil.bv.gottapp.jpa.IAdminBenutzerDao#storeBenutzer(de.egladil.bv.gottapp.domain.Benutzerkonto)
	 */
	@Override
	@Transactional
	public Benutzerkonto storeBenutzer(Benutzerkonto benutzer) {
		EntityManager em = emp.get();
		if (benutzer.getId() == null) {
			em.persist(benutzer);
			return benutzer;
		} else {
			return em.merge(benutzer);
		}
	}
}
