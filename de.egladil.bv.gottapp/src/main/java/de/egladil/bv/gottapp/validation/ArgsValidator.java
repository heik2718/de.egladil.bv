//=====================================================
// Projekt: de.egladil.bv.gottapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.gottapp.validation;

import org.apache.commons.lang3.StringUtils;

import com.beust.jcommander.ParameterException;

import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.common.persistence.utils.PrettyStringUtils;

public class ArgsValidator {

	public void validateCreate(String email, String password, String passwordConfirmation, String mandant)
		throws ParameterException {
		if (StringUtils.isBlank(email)) {
			throw new ParameterException("with cmd create option -e is mandatory");
		}
		if (StringUtils.isBlank(password)) {
			throw new ParameterException("with cmd create option -p1 is mandatory");
		}
		if (StringUtils.isBlank(passwordConfirmation)) {
			throw new ParameterException("with cmd create option -p2 is mandatory");
		}
		if (!passwordConfirmation.equals(password)) {
			throw new ParameterException("passwordConfirmation and password don't match");
		}
		checkMandant(mandant);
	}

	public void validateReset(String mandant) {
		checkMandant(mandant);
	}

	private void checkMandant(String mandant) {
		try {
			Anwendung.valueOf(mandant);
		} catch (Exception e) {
			String gueltig = PrettyStringUtils.collectionToDefaultString(Anwendung.asStringList());
			throw new ParameterException("mandant [" + mandant + "] ist ein ungueltiger Wert. Gultige Werte: " + gueltig);
		}
	}
}
