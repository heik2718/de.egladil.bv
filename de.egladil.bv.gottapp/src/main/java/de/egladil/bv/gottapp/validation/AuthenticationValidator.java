//=====================================================
// Projekt: de.egladil.bv.gottapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.gottapp.validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import de.egladil.bv.gottapp.GottApp;
import de.egladil.common.config.EgladilConfigurationException;
import de.egladil.common.config.IEgladilConfiguration;
import de.egladil.common.exception.EgladilAuthenticationException;
import de.egladil.common.persistence.utils.PrettyStringUtils;
import de.egladil.crypto.provider.impl.EgladilCryptoConfiguration;

@Singleton
public class AuthenticationValidator {
	private static final Logger LOG = LoggerFactory.getLogger(AuthenticationValidator.class);

	private final IEgladilConfiguration config;

	/**
	 * Erzeugt eine Instanz von AuthenticationValidator
	 */
	@Inject
	public AuthenticationValidator(@Named("configRoot") final String pathConfigRoot) {
		this.config = new EgladilCryptoConfiguration(pathConfigRoot);
		this.config.init(pathConfigRoot);
	}

	/**
	 * Prüft das gegebene secret gegen das, was konfiguriert ist.
	 *
	 * @param config
	 * @param secret
	 * @throws EgladilConfigurationException
	 */
	public void checkSecret(final String secret) throws EgladilConfigurationException {
		final char[] configuredSecret = config.getSecretProperty(GottApp.GOTT_SECRET);
		final String confSecretString = new String(configuredSecret);
		System.err.println("[SECRET=" + secret + ", conf=" + confSecretString + "]");
		try {
			if (!secret.equals(confSecretString)) {
				LOG.error("Versuch mit falschem secret [{}]", new String(secret));
				throw new EgladilAuthenticationException("Schade - Authentisierungsfehler!");
			}
		} finally {
			PrettyStringUtils.erase(secret);
			PrettyStringUtils.erase(configuredSecret);
			PrettyStringUtils.erase(confSecretString);
		}
	}
}
