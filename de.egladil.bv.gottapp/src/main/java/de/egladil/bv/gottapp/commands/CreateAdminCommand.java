//=====================================================
// Projekt: de.egladil.bv.gottapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.gottapp.commands;

import java.util.Base64;

import org.apache.shiro.crypto.hash.Hash;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.domain.LoginSecrets;
import de.egladil.bv.aas.domain.Salz;
import de.egladil.bv.gottapp.jpa.IAdminBenutzerDao;
import de.egladil.crypto.provider.IEgladilCryptoUtils;

@Singleton
public class CreateAdminCommand {
	private static final Logger LOG = LoggerFactory.getLogger(CreateAdminCommand.class);

	private IAdminBenutzerDao benutzerDao;

	private IEgladilCryptoUtils cryptoUtils;

	/**
	 * Erzeugt eine Instanz von CreateAdminCommand
	 */
	@Inject
	public CreateAdminCommand(IAdminBenutzerDao benutzerDao, IEgladilCryptoUtils cryptoUtils) {
		this.cryptoUtils = cryptoUtils;
		this.benutzerDao = benutzerDao;

	}

	private Benutzerkonto createAdmin(Benutzerkonto benutzer, String password) {
		Hash hash = cryptoUtils.hashPassword(password.toCharArray());

		LoginSecrets loginSecrets = new LoginSecrets();
		loginSecrets.setPasswordhash(Base64.getEncoder().encodeToString(hash.getBytes()));

		Salz salz = new Salz();
		salz.setAlgorithmName(hash.getAlgorithmName());
		salz.setIterations(hash.getIterations());
		salz.setWert(hash.getSalt().toBase64());
		loginSecrets.setSalz(salz);

		benutzer.setLoginSecrets(loginSecrets);
		Benutzerkonto result = this.benutzerDao.storeBenutzer(benutzer);
		LOG.debug("Benutzer {} angelegt", result.toString());
		return result;
	}

	/**
	 * Legt einen Admin für die gegebene Anwendung mit den gegebenen Credentials an.
	 *
	 * @param loginName
	 * @param email
	 * @param anwendung
	 * @param password
	 * @return
	 */
	public Benutzerkonto createAdmin(String loginName, String email, Anwendung anwendung, String password) {
		Benutzerkonto benutzer = benutzerDao.createBenutzer(loginName, anwendung.toString() + "_ADMIN", anwendung);
		benutzer.setAktiviert(true);
		benutzer.setEmail(email);
		return createAdmin(benutzer, password);
	}
}
