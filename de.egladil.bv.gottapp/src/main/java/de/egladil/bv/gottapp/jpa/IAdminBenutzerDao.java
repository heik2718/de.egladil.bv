package de.egladil.bv.gottapp.jpa;

import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;

public interface IAdminBenutzerDao {

	/**
	 *
	 * @param loginName
	 * @param roleName String name der EnsureRole. Muss zu Role passen!
	 * @param anwendung
	 * @return Benutzerkonto
	 */
	Benutzerkonto createBenutzer(String loginName, String roleName, Anwendung anwendung);

	/**
	 *
	 * @param loginName
	 * @param anwendung
	 * @return
	 */
	Benutzerkonto findBenutzerByLoginName(String loginName, Anwendung anwendung);

	/**
	 *
	 * @param benutzer
	 * @return
	 */
	Benutzerkonto storeBenutzer(Benutzerkonto benutzer);

}