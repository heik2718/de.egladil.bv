//=====================================================
// Projekt: de.egladil.bv.gottapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.gottapp.commands;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.crypto.hash.Hash;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.domain.LoginSecrets;
import de.egladil.bv.aas.domain.Salz;
import de.egladil.bv.gottapp.jpa.IAdminBenutzerDao;
import de.egladil.common.exception.EgladilAuthenticationException;
import de.egladil.crypto.provider.IEgladilCryptoUtils;

@Singleton
public class ResetAdminPasswordCommand {
	private static final Logger LOG = LoggerFactory.getLogger(ResetAdminPasswordCommand.class);

	private final IAdminBenutzerDao benutzerDao;

	private final IEgladilCryptoUtils cryptoUtils;

	/**
	 * Erzeugt eine Instanz von ResetAdminPasswordCommand
	 */
	@Inject
	public ResetAdminPasswordCommand(final IAdminBenutzerDao benutzerDao, final IEgladilCryptoUtils cryptoUtils) {
		this.benutzerDao = benutzerDao;
		this.cryptoUtils = cryptoUtils;
	}

	public char[] resetAdminPassword(final String loginName, final Anwendung mandant) {
		final Benutzerkonto benutzer = checkBenutzer(loginName, mandant);
		return doResetAdminPassword(benutzer);
	}

	Benutzerkonto checkBenutzer(final String loginName, final Anwendung mandant) {
		if (StringUtils.isBlank(loginName)) {
			throw new EgladilAuthenticationException("loginName ist erforderlich (steht im keepass :) )");
		}
		final Benutzerkonto benutzer = this.benutzerDao.findBenutzerByLoginName(loginName, mandant);
		if (benutzer == null) {
			throw new EgladilAuthenticationException(
				"Benutzer mit loginName [" + loginName + "] und Mandant [" + mandant + "] gibt es nicht");
		}

		LOG.debug("Benutzer gefunden: {}", benutzer.toString());

		return benutzer;
	}

	private char[] doResetAdminPassword(final Benutzerkonto benutzer) {
		final String password = cryptoUtils.generateRandomPasswort();
		final Hash hash = cryptoUtils.hashPassword(password.toCharArray());
		final LoginSecrets loginSecrets = benutzer.getLoginSecrets();
		loginSecrets.setPasswordhash(hash.toBase64());
		final Salz salz = loginSecrets.getSalz();
		salz.setWert(hash.getSalt().toBase64());
		salz.setAlgorithmName(hash.getAlgorithmName());
		salz.setIterations(hash.getIterations());
		benutzer.setLoginSecrets(loginSecrets);
		this.benutzerDao.storeBenutzer(benutzer);
		return password.toCharArray();
	}
}
