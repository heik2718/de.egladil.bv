//=====================================================
// Projekt: de.egladil.bv.gottapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.gottapp.validation;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

import de.egladil.bv.gottapp.commands.SupportedCommand;


public class CommandNameValidator implements IParameterValidator {

	@Override
	public void validate(String name, String value) throws ParameterException {
		if (!SupportedCommand.getCLIValues().contains(value)) {
			throw new ParameterException(
				"unsupported value for option -c! expect one of " + SupportedCommand.printSupportedCLIValues());
		}
	}
}
