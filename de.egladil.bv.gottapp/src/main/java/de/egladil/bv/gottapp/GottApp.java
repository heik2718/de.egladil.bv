//=====================================================
// Projekt: de.egladil.bv.gottapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.gottapp;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.google.inject.Guice;
import com.google.inject.Injector;

import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.gottapp.commands.CreateAdminCommand;
import de.egladil.bv.gottapp.commands.ResetAdminPasswordCommand;
import de.egladil.bv.gottapp.commands.SupportedCommand;
import de.egladil.bv.gottapp.di.GottAppInjector;
import de.egladil.bv.gottapp.validation.ArgsValidator;
import de.egladil.bv.gottapp.validation.AuthenticationValidator;
import de.egladil.bv.gottapp.validation.CommandNameValidator;
import de.egladil.common.exception.EgladilAuthenticationException;
import de.egladil.common.persistence.utils.PrettyStringUtils;

/**
 * Hello world!
 *
 */
public class GottApp {

	private static final Logger LOG = LoggerFactory.getLogger(GottApp.class);

	private static final Pattern WINDOWS_NEWLINE = Pattern.compile("\\r\\n?");

	private static final String NAME = "Egladil BV Gottapp";

	public static final String GOTT_SECRET = "gott.secret";

	public static final String GOTT_LOGIN = "gott.bqadmin_loginname";

	public static final String GOTT_DEVMODE = "gott.devmode";

	private CreateAdminCommand createAdminCommand;

	private ResetAdminPasswordCommand resetPasswordCommand;

	private AuthenticationValidator authenticationValidator;

	@Parameter(names = { "-r" }, description = "path to config dir", required = true)
	private String pathConfigRoot;

	@Parameter(names = {
		"-c" }, description = "commandname: [create|reset] (create an ADMIN user or reset pwd)", required = true, validateWith = CommandNameValidator.class)
	private String cmdName;

	@Parameter(names = { "-l" }, description = "loginname", required = true)
	private String loginname;

	@Parameter(names = { "-e" }, description = "email of the new ADMIN user - mandatory for create")
	private String email;

	@Parameter(names = { "-m" }, description = "mandant of the new ADMIN user default is BQ", required = true)
	private String mandant;

	@Parameter(names = { "-p1" }, description = "password of the new ADMIN user - mandatory for create", password = true)
	private String password;

	@Parameter(names = { "-p2" }, description = "password of the new ADMIN user - mandatory for create", password = true)
	private String passwordRepeted;

	@Parameter(names = { "-s" }, description = "secret for internal authentication", required = true, password = true)
	private String secret;

	@Parameter(names = { "--help" }, help = true)
	private boolean help;

	/**
	 *
	 */
	public GottApp() {
	}

	public static void main(final String[] args) {
		GottApp application = null;
		try {
			application = new GottApp();
			application.attachShutDownHook();
			application.printBanner();
			new JCommander(application, args);
			LOG.info(application.toString());
			application.configure();
			application.start();
			System.exit(0);
		} catch (final ParameterException e) {
			System.err.println(e.getMessage());
			if (application != null) {
				application.printUsage();
			}
			System.exit(1);
		} catch (final EgladilAuthenticationException e) {
			System.err.println(e.getMessage());
			System.exit(1);
		}
	}

	private void configure() {
		final Injector injector = Guice.createInjector(new GottAppInjector(pathConfigRoot));
		createAdminCommand = injector.getInstance(CreateAdminCommand.class);
		resetPasswordCommand = injector.getInstance(ResetAdminPasswordCommand.class);
		authenticationValidator = injector.getInstance(AuthenticationValidator.class);
	}

	void attachShutDownHook() {
		LOG.info("adding shutdownHook");
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				// Tja, haben nix mehr
			}
		});
		LOG.info("shutdownHook added");
	}

	protected void printBanner() {
		try {
			final String banner = WINDOWS_NEWLINE.matcher(loadBanner()).replaceAll("\n").replace("\n",
				String.format("%n", new Object[0]));
			LOG.info(String.format("Starting {}%n{}", new Object[0]), NAME, banner);
		} catch (IllegalArgumentException | IOException ignored) {
			LOG.info("Starting {}", NAME);
		}
	}

	private CharSequence loadBanner() throws IOException {
		InputStream in = null;
		try {
			in = this.getClass().getResourceAsStream("/banner.txt");
			final StringWriter stringWriter = new StringWriter();
			IOUtils.copy(in, stringWriter);
			return stringWriter.toString();
		} finally {
			IOUtils.closeQuietly(in);
		}
	}

	public void printUsage() {
		final StringBuffer sb = new StringBuffer();
		sb.append("Usage: <main class> [options]\n");
		sb.append("   Options:\n");
		sb.append("     * -r\n");
		sb.append("          path config dir\n");
		sb.append("     * -c\n");
		sb.append("          commandname: [create|reset] (create an ADMIN user or reset pwd)\n");
		sb.append("       -e\n");
		sb.append("          email of the new ADMIN user - mandatory for create\n");
		sb.append("     * -l\n");
		sb.append("          loginname\n");
		sb.append("       -p1\n");
		sb.append("          password of the new ADMIN user - mandatory for create\n");
		sb.append("       -p2\n");
		sb.append("          repeated password of the new ADMIN user - mandatory for create must coincide with value of pc\n");
		sb.append("     * -m\n");
		sb.append("          mandant of the new ADMIN user\n");
		sb.append("     * -s\n");
		sb.append("          secret for internal authentication\n");
		sb.append("\n");
		sb.append("create ADMIN: <main class> -r /home/xxx/config -c create -s -m <anwendung> -l <loginname> -e <email> -p1 -p2\n");
		sb.append("\n");
		sb.append("reset password: <main class> -r /home/xxx/config -c reset -s -l <loginname>\n");

		System.out.println(sb.toString());
	}

	private void start() {
		authenticationValidator.checkSecret(secret);

		final SupportedCommand supportedCommand = SupportedCommand.valueOfName(this.cmdName);
		switch (supportedCommand) {
		case CREATE_ADMIN:
			new ArgsValidator().validateCreate(this.email, this.password, this.passwordRepeted, this.mandant);
			doCreateNewAdminUser();
			break;
		case RESET_PASSWORD:
			new ArgsValidator().validateReset(this.mandant);
			doResetPassword();
			break;
		}
	}

	private void doCreateNewAdminUser() {
		try {
			final Benutzerkonto benutzer = this.createAdminCommand.createAdmin(this.loginname, this.email,
				Anwendung.valueOf(this.mandant), this.password);
			System.out.println("Benutzer angelegt: " + benutzer.toString());
		} finally {
			PrettyStringUtils.erase(password);
		}
	}

	private void doResetPassword() {
		char[] pwd = null;
		try {
			pwd = this.resetPasswordCommand.resetAdminPassword(this.loginname, Anwendung.valueOf(mandant));
			System.out.println(new String(pwd));
		} finally {
			PrettyStringUtils.erase(pwd);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "App [cmdName=" + cmdName + ", loginname=" + loginname + ", email=" + email + ", mandant=" + mandant + "]";
	}
}
