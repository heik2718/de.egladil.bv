//=====================================================
// Projekt: de.egladil.bv.gottapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.gottapp.jpa.impl;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.google.inject.Inject;
import com.google.inject.Provider;

import de.egladil.bv.aas.domain.Role;
import de.egladil.bv.aas.domain.Rolle;
import de.egladil.bv.gottapp.jpa.IBerechtigungenDao;

/**
 * @author heike
 *
 */
public class BerechtigungenDao implements Serializable, IBerechtigungenDao {

	private static final long serialVersionUID = 2L;

	private Provider<EntityManager> emp;

	/**
	 * @param em
	 */
	@Inject
	public BerechtigungenDao(Provider<EntityManager> emp) {
		this.emp = emp;
	}

	/**
	 * @see de.egladil.bv.gottapp.jpa.IBerechtigungenDao#loadRollen()
	 */
	@Override
	public List<Rolle> loadRollen() {
		TypedQuery<Rolle> query = emp.get().createQuery("from EnsureRole r", Rolle.class);

		List<Rolle> result = query.getResultList();
		return result;
	}

	/**
	 * @see de.egladil.bv.gottapp.jpa.IBerechtigungenDao#findRolleByRole(de.egladil.bv.aas.domain.Role)
	 */
	@Override
	public Rolle findRolleByRole(Role role) {
		TypedQuery<Rolle> query = emp.get().createQuery("from EnsureRole r where r.role = :role", Rolle.class);
		query.setParameter("role", role);
		List<Rolle> result = query.getResultList();
		return result == null || result.isEmpty() ? null : result.get(0);
	}
}
