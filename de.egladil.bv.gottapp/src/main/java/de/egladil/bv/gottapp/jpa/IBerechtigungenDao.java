package de.egladil.bv.gottapp.jpa;

import java.util.List;

import de.egladil.bv.aas.domain.Role;
import de.egladil.bv.aas.domain.Rolle;

public interface IBerechtigungenDao {

	/**
	 *
	 * @return
	 */
	List<Rolle> loadRollen();

	Rolle findRolleByRole(Role role);

}