//=====================================================
// Projekt: de.egladil.bv.gottapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.bv.gottapp.commands;

import java.util.ArrayList;
import java.util.List;

import com.beust.jcommander.ParameterException;

import de.egladil.common.persistence.utils.PrettyStringUtils;


public enum SupportedCommand {
	CREATE_ADMIN("create"),
	RESET_PASSWORD("reset");

	private final String cliValue;

	private SupportedCommand(String cliValue) {
		this.cliValue = cliValue;
	}

	public String getCliValue() {
		return this.cliValue;
	}

	public static SupportedCommand valueOfName(String name) {
		for (SupportedCommand supportedCommand : SupportedCommand.values()) {
			if (supportedCommand.getCliValue().equals(name)) {
				return supportedCommand;
			}
		}
		throw new ParameterException("unsupported value for option -c! expect one of " + printSupportedCLIValues());
	}

	public static String printSupportedCLIValues() {
		return PrettyStringUtils.collectionToDefaultString(getCLIValues());
	}

	public static List<String> getCLIValues() {
		List<String> alle = new ArrayList<>();
		for (SupportedCommand supportedCommand : values()) {
			alle.add(supportedCommand.getCliValue());
		}
		return alle;
	}
}
